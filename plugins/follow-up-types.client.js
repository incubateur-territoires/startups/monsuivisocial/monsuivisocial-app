import { useAuthStore } from '~/store/auth'

export default defineNuxtPlugin(() => {
  async function loadOrganisationFollowUpTypes() {
    const { $pinia, $getItems } = useNuxtApp()
    const authStore = useAuthStore($pinia)

    const filter = {
      organisations: {
        organisation_id: {
          _eq: authStore.userOrganisationId
        }
      }
    }
    const res = await $getItems('follow_up_types', {
      fields: ['id', 'name', 'type'],
      filter,
      sort: [{ field: 'name' }]
    })

    if (res.status === 200) return res.data.data
    throw new Error(res.data.errors[0].message)
  }

  return {
    provide: {
      loadOrganisationFollowUpTypes
    }
  }
})
