import { directusItemDiff, setEmptyFieldsToNull } from '~/utils'
import type {
  DirectusItem,
  DirectusSuccessResponse,
  DirectusQuery,
  DirectusResponse
} from '~/types/directus'
import type {
  Options,
  DirectusExtendedResponse,
  Query,
  ExtendedResponse
} from '~/types/request'
import { useAuthStore } from '~/store/auth'

export default defineNuxtPlugin(() => {
  async function apiRequest<T extends DirectusItem | DirectusItem[]>(
    uri: string,
    options: Options = {},
    query?: DirectusQuery
  ): Promise<DirectusExtendedResponse<T>> {
    const { $pinia } = useNuxtApp()
    const authStore = useAuthStore($pinia)
    const config = useRuntimeConfig()

    let url = `${config.public.apiUrl}${uri}`
    const _options = { ...options }

    if (authStore.isAuthenticated) {
      if (!_options.headers) _options.headers = new Headers()

      _options.headers.set('Authorization', `Bearer ${authStore.accessToken}`)
    }

    let params
    if (query) {
      const _query: Query = directusQueryToQuery(query)
      params = new URLSearchParams(_query)
    }
    if (params) url += `?${params}`

    return await request<DirectusSuccessResponse<T>>(url, _options)
  }

  async function createItem<T extends DirectusItem>(
    collection: string,
    item: T,
    query?: DirectusQuery,
    options: Options = {}
  ): Promise<DirectusExtendedResponse<T>> {
    return await apiRequest<T>(
      `/items/${collection}`,
      {
        ...options,
        method: 'post',
        data: setEmptyFieldsToNull(item)
      },
      query
    )
  }

  async function updateItem<T extends DirectusItem>(
    collection: string,
    originalItem: T,
    updatedItem: T,
    query?: DirectusQuery,
    options: Options = {}
  ): Promise<DirectusExtendedResponse<T>> {
    const itemChanges = directusItemDiff(originalItem, {
      ...originalItem,
      ...setEmptyFieldsToNull(updatedItem)
    })

    if (!itemChanges) {
      return createSuccessfulDirectusExtendedResponse<T>(originalItem)
    }

    return await apiRequest<T>(
      `/items/${collection}/${originalItem.id}`,
      {
        ...options,
        method: 'patch',
        data: itemChanges
      },
      query
    )
  }

  async function deleteItem<T extends DirectusItem>(
    collection: string,
    id: string
  ): Promise<DirectusExtendedResponse<T>> {
    return await apiRequest<T>(`/items/${collection}/${id}`, {
      method: 'delete'
    })
  }

  async function getItem<T extends DirectusItem>(
    collection: string,
    id: string,
    query?: DirectusQuery,
    options: Options = {}
  ): Promise<DirectusExtendedResponse<T>> {
    return await apiRequest<T>(`/items/${collection}/${id}`, options, query)
  }

  async function getItems<T extends DirectusItem>(
    collection: string,
    query?: DirectusQuery,
    options: Options = {}
  ): Promise<DirectusExtendedResponse<T[]>> {
    return await apiRequest<T[]>(`/items/${collection}`, options, query)
  }

  return {
    provide: {
      request,
      apiRequest,
      createItem,
      updateItem,
      deleteItem,
      getItem,
      getItems
    }
  }
})

async function request<T>(
  url: string,
  options: Options = {}
): Promise<ExtendedResponse<T>> {
  const params = { ...options, mode: 'cors' }

  if (params.data) {
    params.body = JSON.stringify(params.data)

    if (!params.headers) params.headers = new Headers()

    params.headers.set('Content-Type', 'application/json')
  }

  if (params.body && !params.method) params.method = 'post'

  params.method = params.method?.toUpperCase()

  const res = (await fetch(url, params as RequestInit)) as ExtendedResponse // Force returned type by fetch
  const contentType = res.headers.get('Content-Type')

  if (res.status !== 204) {
    if (contentType?.startsWith('application/json')) {
      res.data = (await res.json()) as DirectusResponse<T>
    } else {
      // For now, this case never occurs
      throw new Error(
        'Not implemented: Response with unknown content type is not handled'
      )
    }
  }

  return res as ExtendedResponse<T>
}

function directusQueryToQuery(directusQuery: DirectusQuery): Query {
  const {
    filter,
    aggregate,
    fields,
    sort,
    groupBy,
    search,
    limit,
    page,
    meta,
    deep
  } = directusQuery
  const query: Query = {}
  if (sort) {
    query.sort = sort
      .map(({ field, sort }) => `${sort || ''}${field}`)
      .join(',')
  }
  if (groupBy) query.groupBy = groupBy
  if (limit) query.limit = limit.toString()
  if (page) query.page = page.toString()
  if (meta) query.meta = meta
  if (search) query.search = search
  if (fields) query.fields = fields.toString()
  if (filter) query.filter = JSON.stringify(filter)
  if (aggregate) query.aggregate = JSON.stringify(aggregate)
  if (deep) query.deep = JSON.stringify(deep)
  return query
}

function createSuccessfulDirectusExtendedResponse<T extends DirectusItem>(
  data: T
) {
  const noOpResponse = new Response(null, { status: 200 })
  /**
   * Explicitly set `ok` to true to narrow inferred type on the received
   * response, as Response instantiation with status ~200 implicitly set
   * `ok` to true
   * WARNING: Spread operator does not work on Response class instance
   */
  const extendedResponse: DirectusExtendedResponse<T> = {
    status: noOpResponse.status,
    headers: noOpResponse.headers,
    redirected: noOpResponse.redirected,
    statusText: noOpResponse.statusText,
    type: noOpResponse.type,
    url: noOpResponse.url,
    clone: noOpResponse.clone,
    body: noOpResponse.body,
    bodyUsed: noOpResponse.bodyUsed,
    arrayBuffer: noOpResponse.arrayBuffer,
    blob: noOpResponse.blob,
    formData: noOpResponse.formData,
    json: noOpResponse.json,
    text: noOpResponse.text,
    ok: true,
    data: { data }
  }

  return extendedResponse
}
