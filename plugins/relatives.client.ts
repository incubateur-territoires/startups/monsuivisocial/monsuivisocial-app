import { errorResponseMessage } from '~/utils/errorResponse'
import type { Relative } from '~/types/relative'

export default defineNuxtPlugin(() => {
  async function loadBeneficiaryHouseholdRelatives(beneficiaryId: string) {
    const { $getItems } = useNuxtApp()

    const query = {
      fields: [
        '*',
        'beneficiary.id',
        'beneficiary.firstname',
        'beneficiary.usual_name',
        'beneficiary.birth_name',
        'beneficiary.phone_1',
        'beneficiary.phone_2',
        'beneficiary.email'
      ],
      filter: {
        relative_beneficiary: { id: { _eq: beneficiaryId } },
        hosted: { _eq: true }
      }
    }

    const res = await $getItems<Relative>('relatives', query)

    if (res.ok) return res.data.data
    throw new Error(errorResponseMessage(res))
  }

  async function loadBeneficiaryRelatives(beneficiaryId: string) {
    const { $getItems } = useNuxtApp()

    const query = {
      filter: {
        relative_beneficiary: { id: { _eq: beneficiaryId } },
        hosted: { _eq: false }
      },
      sort: [{ field: 'beneficiary' }]
    }

    const res = await $getItems<Relative>('relatives', query)

    if (res.ok) return res.data.data
    throw new Error(errorResponseMessage(res))
  }

  return {
    provide: {
      loadBeneficiaryHouseholdRelatives,
      loadBeneficiaryRelatives
    }
  }
})
