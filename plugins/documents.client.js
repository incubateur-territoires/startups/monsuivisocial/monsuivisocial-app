import { useDocumentsStore } from '~/store/documents'

export default defineNuxtPlugin(() => {
  async function loadBeneficiaryDocuments(beneficiaryId) {
    const { $pinia, $apiRequest } = useNuxtApp()
    const documentsStore = useDocumentsStore($pinia)

    const beneficiaryDocumentsFolderId =
      await documentsStore.loadBeneficiaryDocumentsFolderId()

    if (!beneficiaryDocumentsFolderId) {
      throw new Error('Beneficiary documents folder id could not be loaded.')
    }

    const filter = {
      beneficiary: { id: { _eq: beneficiaryId } },
      folder: { id: { _eq: beneficiaryDocumentsFolderId } }
    }
    const fields = [
      'id',
      'document_type',
      'filename_download',
      'type',
      'filesize',
      'labels',
      'confidential',
      'uploaded_by'
    ]
    const res = await $apiRequest(
      '/files',
      {},
      {
        fields,
        filter,
        sort: [{ field: 'uploaded_on', sort: '-' }]
      }
    )

    if (res.status === 200) return res.data.data
    throw new Error(res.data.errors[0].message)
  }

  return {
    provide: {
      loadBeneficiaryDocuments
    }
  }
})
