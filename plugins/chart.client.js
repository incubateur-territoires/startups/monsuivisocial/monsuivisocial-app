import { ArcElement, Chart, PieController, Tooltip } from 'chart.js'

export default defineNuxtPlugin(() => {
  Chart.register(ArcElement, PieController, Tooltip)

  return {
    provide: {
      buildChart: (item, config) => new Chart(item, config)
    }
  }
})
