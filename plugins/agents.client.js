export default defineNuxtPlugin(() => {
  async function loadAgents() {
    const { $apiRequest } = useNuxtApp()

    const { status, data } = await $apiRequest(
      '/users',
      {},
      {
        fields: ['id', 'first_name', 'last_name', 'status']
      }
    )

    if (status === 200) return data.data
    throw new Error(data.errors[0].message)
  }

  return {
    provide: {
      loadAgents
    }
  }
})
