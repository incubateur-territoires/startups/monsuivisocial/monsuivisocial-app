import { useAuthStore } from '~/store/auth'

export default defineNuxtPlugin(() => {
  async function loadPrescribingOrganisations() {
    const { $pinia, $getItems } = useNuxtApp()
    const authStore = useAuthStore($pinia)

    const filter = {
      organisation: {
        _eq: authStore.userOrganisationId
      }
    }
    const res = await $getItems('organismes_prescripteurs', {
      fields: ['id', 'name'],
      filter,
      sort: [{ field: 'name' }]
    })

    if (res.status === 200) return res.data.data
    throw new Error(res.data.errors[0].message)
  }

  return {
    provide: {
      loadPrescribingOrganisations
    }
  }
})
