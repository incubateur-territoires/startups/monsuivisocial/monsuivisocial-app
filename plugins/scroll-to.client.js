import VueScrollTo from 'vue-scrollto'

export default defineNuxtPlugin(nuxtApp => {
  nuxtApp.vueApp.use(VueScrollTo)

  function focusAndScrollTo(selector) {
    // Prevent scroll and then trigger it on purpose as the native focus scroll can cause a weird jump back to the top on page access
    document.querySelector(selector)?.focus({ preventScroll: true })
    VueScrollTo.scrollTo(selector, 300, {
      easing: 'linear',
      offset: -50
    })
  }

  return {
    provide: {
      scrollTo: VueScrollTo.scrollTo,
      focusAndScrollTo
    }
  }
})
