export default defineNuxtPlugin(() => {
  function openModal(
    title,
    contentComponent,
    contentComponentProps,
    contentComponentEvents
  ) {
    const { $globalEventEmitter } = useNuxtApp()

    $globalEventEmitter.emit('openModal', {
      title,
      contentComponent,
      contentComponentProps,
      contentComponentEvents
    })
  }

  function closeModal() {
    const { $globalEventEmitter } = useNuxtApp()

    $globalEventEmitter.emit('closeModal')
  }

  return {
    provide: {
      modal: {
        open: openModal,
        close: closeModal
      }
    }
  }
})
