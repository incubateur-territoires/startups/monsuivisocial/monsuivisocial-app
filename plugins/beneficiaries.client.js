import { sort } from '~/utils'
import { errorResponseMessage } from '~/utils/errorResponse'

function buildFields(complete, withHistory) {
  const fields = ['user_created']

  if (complete) {
    fields.push(
      '*',
      'referents.id',
      'referents.referent.id',
      'referents.referent.first_name',
      'referents.referent.last_name',
      'relatives.id',
      'relatives.firstname',
      'relatives.lastname',
      'relatives.type',
      'relatives.city',
      'relatives.email',
      'relatives.phone',
      'relatives.additional_information',
      'relatives.hosted',
      'relatives.caregiver',
      'relatives.beneficiary'
    )
  } else fields.push('id', 'firstname', 'usual_name')

  if (withHistory) {
    fields.push(
      'follow_ups.follow_up_types.follow_up_types_id.id',
      'follow_ups.follow_up_types.follow_up_types_id.type',
      'follow_ups.follow_up_types.follow_up_types_id.name',
      'help_requests.follow_up_type.id',
      'help_requests.follow_up_type.type',
      'help_requests.follow_up_type.name'
    )
  }

  return fields
}

function getBeneficiaryFollowUpTypes(beneficiary) {
  const uniqueFollowUpTypesTuples = {
    ...getUniqueFollowUpTypesTuplesFromInterviews(beneficiary.follow_ups),
    ...getUniqueFollowUpTypesTuplesFromHelpRequests(beneficiary.help_requests)
  }
  const followUpTypes = Object.values(uniqueFollowUpTypesTuples)
  return sort(followUpTypes, [{ field: 'name', sort: '' }])
}

function getUniqueFollowUpTypesTuplesFromInterviews(interviews) {
  return interviews.reduce((uniques, interview) => {
    if (!interview.follow_up_types.length) return uniques
    const interviewUniqueFollowUpTypes = getUniqueFollowUpTypesFormInterview(
      interview.follow_up_types
    )
    return {
      ...uniques,
      ...interviewUniqueFollowUpTypes
    }
  }, {})
}

function getUniqueFollowUpTypesFormInterview(followUpTypesArr) {
  return followUpTypesArr.reduce(
    (uniques, f) => ({
      ...uniques,
      [f.follow_up_types_id.id]: f.follow_up_types_id
    }),
    {}
  )
}

function getUniqueFollowUpTypesTuplesFromHelpRequests(helpRequests) {
  return helpRequests.reduce((uniques, helpRequest) => {
    if (!helpRequest.follow_up_type) return uniques
    return {
      ...uniques,
      [helpRequest.follow_up_type.id]: helpRequest.follow_up_type
    }
  }, {})
}

export default defineNuxtPlugin(() => {
  async function loadBeneficiary(
    beneficiaryId,
    complete = false,
    withHistory = false
  ) {
    const { $getItem } = useNuxtApp()

    const query = { fields: buildFields(complete, withHistory) }
    if (withHistory) {
      query.deep = {
        follow_ups: { comments: { _sort: 'date_created' } },
        help_requests: { comments: { _sort: 'date_created' } }
      }
    }
    const res = await $getItem('beneficiary', beneficiaryId, query)

    if (res.status === 200) return res.data.data
    throw new Error(errorResponseMessage(res))
  }

  function buildBeneficiarySearchFrom(search) {
    const searchWords = search.split(' ')

    return searchWords.reduce(
      (_or, word) => [
        ..._or,
        /**
         * Usual name and birth name use `_contains` and not `_icontains` as those fields are always uppercase.
         * It prevents search issues due to accents (éèà...).
         */
        { usual_name: { _contains: word.toUpperCase() } },
        { birth_name: { _contains: word.toUpperCase() } },
        { firstname: { _icontains: word } }
      ],
      []
    )
  }

  async function searchBeneficiaries(search, excludeId = undefined) {
    const { $getItems } = useNuxtApp()
    const query = {
      fields: ['id', 'usual_name', 'birth_name', 'firstname', 'birthdate'],
      filter: { _or: buildBeneficiarySearchFrom(search) },
      sort: [{ field: 'usual_name' }]
    }

    if (excludeId) query.filter.id = { _neq: excludeId }

    const { status, data } = await $getItems('beneficiary', query)

    if (status === 200) return data.data
    throw new Error(data.errors[0].message)
  }

  async function loadBeneficiariesCities() {
    const { $getItems } = useNuxtApp()

    const res = await $getItems('beneficiary', {
      groupBy: 'city',
      filter: { city: { _neq: null } }
    })

    if (res.status === 200) return res.data.data
    throw new Error(res.data.errors[0].message)
  }

  return {
    provide: {
      loadBeneficiary: beneficiaryId => loadBeneficiary(beneficiaryId, true),
      loadBeneficiaryWithHistory: beneficiaryId =>
        loadBeneficiary(beneficiaryId, true, true),
      loadMinimalBeneficiary: beneficiaryId => loadBeneficiary(beneficiaryId),
      searchBeneficiaries,
      getBeneficiaryFollowUpTypes,
      loadBeneficiariesCities
    }
  }
})
