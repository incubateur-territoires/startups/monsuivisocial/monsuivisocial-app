import { useAuthStore } from '~/store/auth'
import { Routes } from '~/utils/routing/routes'

export default defineNuxtRouteMiddleware(async to => {
  const { $pinia, $getItem } = useNuxtApp()
  const authStore = useAuthStore($pinia)
  if (!authStore.isReferent) return

  try {
    const res = await $getItem('beneficiary', to.params.id, {
      fields: ['referents.referent.id']
    })

    if (res.status !== 200) return navigateTo(Routes.Beneficiaries)

    let isBeneficiaryReferent = false
    res.data.data.referents.forEach(({ referent }) => {
      if (referent.id === authStore.userId) {
        isBeneficiaryReferent = true
      }
    })
    if (!isBeneficiaryReferent) return navigateTo(Routes.Beneficiaries)
  } catch {
    return navigateTo(Routes.Beneficiaries)
  }
})
