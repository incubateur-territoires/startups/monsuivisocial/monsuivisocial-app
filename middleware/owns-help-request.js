import { usePermissionsStore } from '~/store/permissions'
import { ownsItem } from '~/utils/owns-item'
import { Routes } from '~/utils/routing/routes'

export default defineNuxtRouteMiddleware(async to => {
  const app = useNuxtApp()
  const permissionsStore = usePermissionsStore(app.$pinia)
  if (permissionsStore.canEditAnyHistoryItem) {
    return
  }

  return await ownsItem(
    app,
    navigateTo,
    permissionsStore,
    'help_requests',
    to.params.hrid,
    Routes.History
  )
})
