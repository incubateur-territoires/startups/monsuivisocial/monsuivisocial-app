import { useAuthStore } from '~/store/auth'
import { Routes } from '~/utils/routing/routes'

export default defineNuxtRouteMiddleware(async to => {
  const { $pinia } = useNuxtApp()
  const authStore = useAuthStore($pinia)
  await authStore.refresh()
  if (authStore.isAuthenticated) {
    const redirectUrl = to.query.redirectUrl || Routes.Overview
    return navigateTo(redirectUrl)
  }
})
