import { usePermissionsStore } from '~/store/permissions'
import { Routes } from '~/utils/routing/routes'

export default defineNuxtRouteMiddleware(() => {
  const { $pinia } = useNuxtApp()
  const permissionsStore = usePermissionsStore($pinia)
  if (!permissionsStore.canEditOrganisation) return navigateTo(Routes.Overview)
})
