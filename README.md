# Mon Suivi Social - Application Client

[Mon Suivi Social](https://monsuivisocial.incubateur.anct.gouv.fr/) est une startup d'État portée par [beta.gouv.fr](beta.gouv.fr) et l'[Incubateur des Territoires](https://incubateur.anct.gouv.fr/).

Mon Suivi Social est un outil simple d'utilisation et complet pour gérer l'accompagnement social.

Cette partie concerne l'application Web de Mon Suivi Social développée avec Vue.js/Nuxt.

Le code source de l'API de Mon Suivi Social est disponible sur un [autre dépôt](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-api) dédié.

## Dépendances

Pour lancer le projet, vous avez besoin de :

- Node.js _([lien de téléchargement](https://nodejs.org/en/))_ - Après l'installation, vérifiez que Node.js est correctement installé en exécutant dans le terminal de votre choix la commande `node -v`. Celle-ci doit afficher la version de Node.js installée.
- l'API de Mon Suivi Social. Les instructions d'installation sont disponibles sur [son dépôt](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-api).

## Comment contribuer au projet

- Installez les dépendances Node.js
  ```bash
  npm install
  ```
- Démarrez le serveur de développement
  ```
  npm run dev
  ```
  Il est alors disponible à l'adresse [localhost:3000](http://localhost:3000/).

Pour démarrer l'API, voir le README.me sur [son dépôt](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-api).

Avant d'accéder à l'application, il est conseillé de lire la section du Wiki [🏁 Pour un démarrage en douceur](https://gitlab.com/groups/incubateur-territoires/startups/monsuivisocial/-/wikis/%F0%9F%8F%81-Pour-un-d%C3%A9marrage-en-douceur).

Pour plus d'explications détaillées, vous pouvez consulter la [documentation](https://nuxtjs.org) de Nuxt.

**À SAVOIR** : Si vous voulez contribuer au projet, merci d'exécuter la commande `npm exec simple-git-hooks` une fois les dépendances Node.js installées pour que votre code soit _linté_ lorsque vous le _commitez_.

## Comment construire le projet

- Préparez un _build_ pour la production
  ```bash
  npm run build
  ```
- Vous pouvez servir le _build_ préparé
  ```bash
  npm run start
  ```

## Tester l'image docker de production en local

Contruire l'image docker

```bash
docker build . -t monsuivisocial-app
```

Démarrer l'image docker

```bash
docker run -p 3000:3000 monsuivisocial-app
```

## Conventions de code

Au-delà des conventions définies grâce aux linters et formatters ESLint, Stylelint et Prettier, le projet suis les [conventions de code proposées par la communauté de Vue.js](https://vuejs.org/style-guide/).

En supplément, deux choix ont été faits sur le nommage des composants :

- Le préfixe `Atom` a été choisi pour les [composants de base](https://vuejs.org/style-guide/rules-strongly-recommended.html#base-component-names),
- Le préfixe `The` a été choisi pour les [composants à instance unique](https://vuejs.org/style-guide/rules-strongly-recommended.html#single-instance-component-names).

## Authentification

Pour l'authentification, il a été choisi de ne pas utiliser le paquet `@nuxtjs/auth-next`, et ce pour plusieurs raisons :

- Le paquet est trop [trop lourd](https://bundlephobia.com/package/@nuxtjs/auth-next@5.0.0-1648802546.c9880dc),
- Cela permet d'éviter une dépendance et donc de garder la main sur cette partie,
- Le paquet demande une dépendance supplémentaire : `@nuxtjs/axios`,
- Les besoins en authentification du projet sont pour le moment suffisamment restreints (authentification via JWT classique) pour être géré facilement avec du code écrit et maintenu par l'équipe.

L'authentification est implantée dans le projet par 4 fichiers :

- `store/auth.js` : Ce store définit toute la logique d'authentification et rend disponible l'état et les méthodes d'authentification dans le contexte Nuxt ;
- `middleware/auth.js` et `middleware/skip-auth.js` : Ils permettent de gérer l'accès ou l'évitement de certaines routes suivant l'état d'authentification de l'utilisateur.

## Tests

Le dossier `test` contient l'ensemble des tests unitaires du projet. Les tests sont exécutés grâce à l'utilitaire [Jest](https://jestjs.io/).

Exécutez l'ensemble des tests :

```bash
npm run test
```

En phase de développement, vous pouvez exécuter les tests avec un _watcher_ qui relance les tests à chaque modification :

```bash
npm run test -- --watch
```

Par défaut, la configuration de Jest génère la couverture de tests dans le dossier `coverage`. Vous pouvez afficher la couverture de tests générée en ouvrant dans votre navigateur le fichier `coverage/lcov-report/index.html`.

Vous pouvez exécuter les tests sans la couverture de tests :

```bash
npm run test -- --coverage=false
```

## Environnements

L'application de Mon Suivi Social existe sur plusieurs environnements :

- production :
  - Lien d'accès : https://monsuivisocial.incubateur.anct.gouv.fr/
  - Mode de déploiement : Création d'un Git tag
- développement :
  - Lien d'accès : https://monsuivisocial.dev.incubateur.anct.gouv.fr/
  - Mode de déploiement : Poussée sur la branche `main`
- review : pour chaque branche, un environnement de review est généré.
  - Lien d'accès : L'URL de chaque environnement de review est accessible dans le menu [Environnements](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/environments/folders/review) sur le dépôt GitLab
  - Mode de déploiement : Poussée sur une branche autre que la branche `main`
  - NOTE : Par soucis de praticité, il existe aussi une branche `review` qui peut être utilisé pour cet environnement. Elle n'est pas obligatoire et peut être supprimée au besoin.

## Dossiers spéciaux

Documentation en anglais générée par Nuxt :

> You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.
>
> ### `assets`
>
> The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).
>
> ### `components`
>
> The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).
>
> ### `layouts`
>
> Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).
>
> ### `pages`
>
> This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).
>
> ### `plugins`
>
> The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.ts`.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).
>
> ### `static`
>
> This directory contains your static files. Each file inside this directory is mapped to `/`.
>
> Example: `/static/robots.txt` is mapped as `/robots.txt`.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).
>
> ### `store`
>
> This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.
>
> More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
