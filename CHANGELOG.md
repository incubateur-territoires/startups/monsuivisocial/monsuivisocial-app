# Notes de version

## Prochaine version

### ✨ Ajout

### 🚀 Modification

### 🐛 Correction

### 🧽 Suppression

# 0.24.2 (15 mai 2023)

### 🐛 Correction

- Correction d'un bug qui empêche de revenir sur le tableau de bord depuis la fiche d'un bénéficiaire accédée à partir de la barre de recherche du tableau de bord. [#352](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/352)

# 0.24.1 (26 avril 2023)

### 🐛 Correction

- Correction d'un bug qui affiche le bloc des statistiques de zone d'hébergement aux structures qui ne sont pas de type **Ministère**.

# 0.24.0 (25 avril 2023)

### ✨ Ajout

- Fonctionnalité de suppression d'une demande d'aide ou d'une synthèse d'entretien pour les responsables de structure. [#120](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/120)
- Fonctionnalité de suppression d'un bénéficiaire pour les responsables de structure. [#155](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/155)
- Ajout du champ **Numéro Pégase** dans les informations générales de la fiche bénéficiaire pour les structures de type **Ministère**. [#339](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/339)
- Ajout du filtre **Référent** dans le module statistiques pour les structures de type **Ministère**. [#338](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/338)

### 🚀 Modification

- Les agents désactivés ne peuvent pas être sélectionnés comme référent d'un bénéficiaire. [#334](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/334)
- Mise au singulier des filtres et des intitulés "Objet de l'entretien" [#333](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/333)
- Champs "Situation familiale", "Nombre d'enfant(s) mineur(s)" et "Nombre d'enfant(s) majeur(s)" déplacés dans la section "Foyer fiscal" [#327](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/327)

### 🐛 Correction

- Correction d'un bug qui empêche de supprimer tous les membres du foyer fiscal et de l'entourage d'une fiche bénéficiaire. [#327](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/327#note_1361571551)
- Correction d'un bug qui empêche les agents d'accueil de modifier une fiche bénéficiaire.

### 🧽 Suppression

- Suppression des espaces vides dans la page Statistiques en agrandissant chaque bloc à la même hauteur. [#335](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/335)

# 0.23.1 (17 avril 2023)

### 🚀 Modification

- Mise à jour du lien vers le webinaire mensuel sur la page **S'enregistrer**.

# 0.23.0 (7 avril 2023)

### 🚀 Modification

- Amélioration du comportement des boutons "Retour" [#324](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/324)
- La recherche des bénéficiaires ne se fait plus que sur les noms, noms de naissances et prénoms. [#328](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/328)
- La date de naissance des bénéficiaires est affichée dans les résultats de la recherche pour pouvoir discriminer les homonymes. [#328](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/328)

# 0.22.0 (3 avril 2023)

### ✨ Ajout

- Plusieurs onglets pour les statistiques "Demandes d'aide" et "Entretiens" dans la page "Statistiques" [#313](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/313)

### 🚀 Modification

- Lors de l'enregistrement d'un bénéficiaire, son nom, son nom de naissance et la première lettre de son prénom sont automatiquement mis en majuscule. [#329](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/329)
- Les en-têtes des éléments de l'historique d'un bénéficiaire sont entièrement cliquable pour améliorer l'accessibilité. [#294](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/294)
- Le tri des éléments se fait désormais à travers toutes les pages d'un tableau. [#285](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/285)
- Nouvelle valeur "Pension alimentaire" dans le champ "Nature des ressources" de la fiche bénéficiaire. [#319](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/319)

# 0.21.2 (3 avril 2023)

### 🐛 Correction

- Il n'est plus possible de mettre à jour un champ de sélection multiple (**Natures des ressources** par exemple). [#319](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/319#note_1323078320)

# 0.21.1 (22 mars 2023)

### 🐛 Correction

- Les filtres ne s'appliquent plus automatiquement.
- Il n'est plus possible de choisir le nombre de résultats par page. [#331](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/331)

# 0.21.0 (20 mars 2023)

### ✨ Ajout

- Statistiques des "Ministres" dans la page des statistiques pour les ministères sociaux. [#134](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/134)
- Statistiques des "Modes d'hébergement" dans la page des statistiques. [#145](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/145)
- Statistiques des "Régions" dans la page statistiques. [#135](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/135)
- Statistiques des "Données mobilité" dans la page des statistiques. [#146](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/146)
- Statistiques des "Catégories socio-professionnelles" dans la page des statistiques. [#147](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/147)
- Filtre par "Ville" sur la page des statistiques. [#222](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/222)
- Champ de sélection / création avec recherche pour les organismes instructeurs [#302](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/302)
- Statistiques des "Organismes instructeurs" dans la page des statistiques. [#140](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/140)
- Au cours de la session, les filtres sont maintenus pour les accompagnements, l'historique d'un bénéficiaire et les bénéficiaires. [#242](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/242#note_1287849644)
- Ajout de la possibilité de lier une fiche bénéficiaire à un membre de l'entourage. Lorsqu'une relation de ce type est établie, un lien pour accéder à cette autre fiche bénéficiaire est affiché dans les informations générales de la fiche bénéficiaire. [#63](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/63)
- Statistiques des "Structures de rattachement" dans la page des statistiques. [#312](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/312)
- Statistiques des "Départements ou services de l'administration centrale" dans la page des statistiques. [#311](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/311)
- Statistiques des "Catégories" dans la page des statistiques. [#310](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/310)
- Ajout du champ "Zone d'hébergement" dans la fiche bénéficiaire [#307](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/307)
- Statistiques des "Zones d'hébergement" dans la page des statistiques. [#308](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/308)
- Actions rapides dans le tableau des Accompagnements [#315](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/315)

### 🚀 Modification

- "Types d'accompagnement" est renommé en "Objets de l'entretien" dans les synthèses d'entretien, et les statistiques relatives. [#300](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/300)
- "Types d'accompagnement" est renommé en "Objets de la demande" dans les instructions de demande d'aide, et les statistiques relatives. [#300](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/300)
- Modifications dans la page statistiques : nombres en gras et pourcentages normaux et couleurs spécifiques pour les demandes d'aide ou entretiens [#296](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/296)
- Modification de la position des statistiques bénéficiaires sur écran réduit pour une meilleure lisibilité. [#304](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/304)
- Nouveau design pour la page d'accueil. [#286](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/286)
- Dans la fiche bénéficiaire, les membres du foyer fiscal sont séparés du reste des membres de l'entourage. [#318](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/318)
- Modification de l'éditeur de Mon Suivi Social et du directeur de publication dans les Mentions Légales. [#286](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/286)
- Les utilisateurs des structures de type **Ministère** n'ont pas accès à la section "Santé" de la fiche bénéficiaire. [#309](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/309)

### 🐛 Correction

- Impossible de sélectionner des filtres d'accompagnements [#314](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/314)
- Suppression des champs **Ministre** et **Administration** de la fiche bénéficiaire destinés aux utilisateurs des structures de type **Ministère**. [#309](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/309)

# 0.20.2 (28 février 2023)

- Correction d'une régression qui affiche la valeur brute des filtres sélectionnés. [#314](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/314)

# 0.20.1 (21 février 2023)

### 🐛 Correction

- Correction d'une régression qui rend impossible la sélection des filtres d'accompagnements. [#314](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/314)

# 0.20.0 (20 février 2023)

### ✨ Ajout

- Ajout des filtres de période et par types d'accompagnement dans la page des statistiques. [#136](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/136)
- Dans la fiche bénéficiaire, si un membre de l'entourage a une fiche bénéficiaire rattachée, un lien vers cette fiche est affiché. [#63](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/63)
- Statistiques des "Organismes prescripteurs" dans la page des statistiques. [#149](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/149)
- Affichage des totaux pour les bénéficiaires, demandes d'aide, et entretiens dans la page des statistiques [#252](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/252)
- Statistiques des "Statuts" dans la page des statistiques. [#290](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/290)
- Statistiques des "Types d'accompagnement" dans la page des statistiques. [#247](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/247)
- Dans la tableau de bord, différentes notifications sont désormais affichées :

  - L'ajout d'un document par un autre référent sur une fiche bénéficiaire dont on est référent ;
  - L'ajout d'un commentaire sur une synthèse d'entretien ou une instruction de demande d'aide que l'on a créé ;
  - Lorsqu'une synthèse d'entretien ou d'une instruction de demande d'aide que l'on a créé arrive à échéance le jour même ;
  - Lorsqu'une instruction de demande d'aide que l'on a créé arrive à sa fin de prise en charge le jour même ;

    Les notifications sont générées suivant les accès permis en fonction du rôle des destinataires. [#23](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/23)

- Les éléments de l'historique présentant une notification non lue sont signalés par une icône "🔔" dans leur en-tête. [#277](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/277#note_1272749546)
- Les documents présentant une notification non lue sont signalés par une icône "🔔" dans l'espace document de la fiche bénéficiaire. [#277](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/277#note_1272749546)
- Statistiques des "Aides financières" dans la page des statistiques. [#214](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/214)
- Statistiques des "Actions entreprises" dans la page des statistiques. [#141](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/141)
- Statistiques des "Types d'entretiens" dans la page des statistiques. [#295](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/295)

### 🚀 Modification

- Le nom de naissance du bénéficiaire est affiché entre parenthèse si aucun nom usuel n'est renseigné. [#269](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/269)
- Réorganisation de la page des statistiques. Les statistiques qui concernent l'historique des bénéficiaires est déplacée dans un onglet séparé des statistiques relatives aux bénéficiaires. [#263](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/263)
- Le design de l'onglet **Historique** de la fiche bénéficiaire est repensé pour faciliter son utilisation. [#280](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/280)
- Affichage de certains pourcentages sans barre de chargement dans la page des statistiques [#289](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/289)

### 🐛 Correction

- Correction d'un bug qui empêche de décocher la case **1er entretien** lors de la modification d'une synthèse d'entretien existante. [#288](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/288)
- Un instructeur peut marquer un document qu'il n'a pas déposé lui-même comme confidentiel. [#298](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/298)
- Certaines informations dans les tableaux des accompagnements étaient affichées indifféremment des permissions pour les rôles Instructeur et Agent d'accueil. [#280](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/280#note_1278118467)

### 🧽 Suppression

- Suppression de certaines fonctionnalités listées dans le tableau des rôles alors qu'elles ne sont pas encore implémentées. [#301](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/301)

# 0.19.2 (7 février 2023)

### 🐛 Correction

- Correction du lien InclusionConnect **En savoir plus** sur la page de connexion.

# 0.19.1 (6 février 2023)

### 🐛 Correction

- Correction d'un bug qui empêche de filtrer l'historique de la fiche bénéficiaire.

# 0.19.0 (30 janvier 2023)

### ✨ Ajout

- Une instruction de demande d'aide peut être liée à un organisme prescripteur. Un nouvel organisme prescripteur peut être créé directement depuis le formulaire d'instruction de demande d'aide. [#202](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/202)
- Dans la fiche bénéficiaire, ajout d'un champ **Région**, avec autocomplétion, pour les structures de type ministère. [#256](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/256)
- Ajout d'un lien vers l'aide du logiciel. [#266](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/266)

### 🐛 Correction

- Correction d'un bug qui fermait la modale de modification de document en plus du sélecteur multiple de thèmes, lors de l'utilisation de la touche _Échap_.

# 0.18.1 (26 janvier 2023)

### 🐛 Correction

- Correction d'un bug empêchant de créer une synthèse d'entretien qui n'est pas liée à un organisme prescripteur.

# 0.18.0 (26 janvier 2023)

### ✨ Ajout

- Les informations d'un document peuvent être modifiées. [#250](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/250)
- Ajout des permissions de modification dans le tableau des rôles. [#250](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/250)
- Ajout de la colonne **Date d'échéance** dans les tableaux des accompagnements, avec tri. [#267](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/267)
- Une synthèse d'entretien peut être liée à un organisme prescripteur. Un nouvel organisme prescripteur peut être créé directement depuis le formulaire de synthèse d'entretien. [#199](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/199)

### 🚀 Modification

- La recherche dans les sélecteurs à choix multiples ne considère plus uniquement le début des options mais tout leur contenu. Par exemple, "num" aura comme résultat "Inclusion numérique". [#186](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/186)
- Le numéro de dossier est relégué en dernière colonne dans les tableaux des accompagnements. [#267](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/267)

### 🐛 Correction

- Après le changement de page au sein du tableau des bénéficiaires, le défilement est remis en haut du tableau. [#273](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/273#note_1253366593)

# 0.17.2 (24 janvier 2023)

### 🐛 Correction

- Correction d'un bug empêchant de filtrer correctement les bénéficiaires en fonction de leurs référents.
- Correction d'un bug qui affiche un nombre d'éléments affichés incorrect. [#272](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/272) [#273](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/273)
- Correction d'un bug qui affiche des statistiques incorrectes. [#272](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/272)

# 0.17.1 (19 janvier 2023)

### 🐛 Correction

- Les agents d'accueil ne peuvent pas accéder au détail des fiches bénéficiaires. [#264](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/264)

# 0.17.0 (18 janvier 2023)

### ✨ Ajout

- Les responsables de structure peuvent consulter la liste des utilisateurs de leur structure. [#38](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/38)
- Les responsables de structure peuvent modifier le rôle d'un utilisateur de leur structure. [#38](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/38)
- Les responsables de structures peuvent désactiver l'accès à MonSuiviSocial à un utilisateur de leur structure. [#38](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/38)
- Nouveau filtre "Type de suivi" dans l'historique d'un bénéficiaire ("Synthèse d'entretien" ou "Demande d'aide"). [#240](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/240)
- Texte d'aide concernant les données sensibles sur plusieurs formulaires (fiche bénéficiaire, synthèses d'entretien, demandes d'aide). [#243](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/243)
- Bouton "Imprimer" pour exporter une fiche d'informations d'un bénéficiaire au format PDF. [#10](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/10)
- Les synthèses d'entretien et les instructions de demande d'aide peuvent être liées à des documents de la fiche bénéficiaire ou à de nouveaux documents qui sont alors ajoutés à la fiche bénéficiaire. [#184](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/184)
- Nouvelle option "À traiter" pour le statut des synthèses d'entretien. [#245](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/245)
- Bouton "Voir plus"/"Masquer" sur la liste des statistiques des "Types d'accompagnement". [#247](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/247)
- Dans la page Statistiques, ajout d'une section qui détaille la répartition des situations familiales par rapport au nombre de bénéficiaires au sein de la structure. [#144](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/144)
- Dans la page Statistiques, ajout d'une section qui détaille la répartition des tranches d'âge par rapport au nombre de bénéficiaires au sein de la structure, triées par ordre décroissant. [#137](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/137)
- Ajout d'une pagination sur la page bénéficiaires. [#112](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/112)
- Ajout d'une pagination sur la page accompagnement (synthèses d'entretien et demandes d'aide). [#112](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/112)
- Nouveau formulaire de téléchargement de documents, avec l'ajout d'un choix de thèmes. [#98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/98)
- Affichage des thèmes dans l'espace document. [#98](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/98)
- Possibilité de marquer un document comme "confidentiel" dans le formulaire d'ajout de documents. [#249](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/249)
- Nouveau champ "1er entretien" dans le formulaire de synthèse d'entretien. [#255](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/255)
- Dans l'onglet **Historique** de la fiche bénéficiaire, des commentaires peuvent être ajoutés sur les synthèses des éléments de l'historique (synthèses d'entretien ou instructions de demande d'aide). [#105](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/105)
- Ajout d'un motif "Autre" dans le formulaire de demande d'aide [#261](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/261)

### 🚀 Modification

- Il n'est plus possible de désélectionner un type d'accompagnement si celui-ci est déjà associé à une synthèse d'entretien ou une instruction de demande d'aide. [#130](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/130)
- Les documents présentent un lien de prévisualisation et un lien de téléchargement. [#115](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/115)
- Amélioration du design de l'espace document des bénéficiaires. [#115](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/115)
- Modification de l'ordre d'affichage des statistiques de genres. (ordre décroissant) [#144](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/144#note_1231796351)
- Mise à jour du design du bouton de connexion InclusionConnect. [#257](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/257)
- Modification de l'ordre des champs d'adresse dans le formulaire de la fiche bénéficiaire et de la structure. Amélioration de la suggestion d'adresses. [#133](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/133)
- Modification du tableau des permissions pour le rôle "Instructeur" et la consultation des documents. [#249](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/249)

### 🐛 Correction

- Suite à certaines interactions (fermeture d'alerte, navigation vers un élément de l'historique du bénéficiaire), l'élément ciblé n'est pas mis en évidence.

# 0.16.2 (16 janvier 2023)

### 🚀 Modification

- Mise à jour technique sous le capot.

# 0.16.1 (19 décembre 2022)

### 🐛 Correction

- Correction d'un bug qui affiche en double les types d'accompagnements des synthèses d'entretien dans la page **Accompagnements**. [#248](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/248)

# 0.16.0 (19 décembre 2022)

### ✨ Ajout

- Ajout des rôles Référent, Instructeur et Agent d'accueil. Les permissions de chaque rôle permettre une meilleure finesse dans la gestion des accès. [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/123)
- Ajout d'un lien de retour sur la fiche bénéficiaire qui ramène sur la liste des bénéficiaires. [#241](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/241)

### 🚀 Modification

- Lors de la création d'une synthèse d'entretien, le statut sélectionné par défaut est **Terminé**. [#187](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/187)
- Il n'est plus nécessaire de faire défiler une page qui contient un tableau jusqu'à son pied pour accéder à la barre de défilement du tableau. [#156](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/tree/156-barre-de-defilement-accompagnements-et-benefiaires)
- Le rôle Agent devient Travailleur social. [#123](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/123)

### 🐛 Correction

### 🧽 Suppression

# 0.15.0 (12 décembre 2022)

### ✨ Ajout

- Ajout d'un texte d'aide pour le champ **Complément d'adresse** de la fiche bénéficiaire.
- L'accès sécurisé à l'application s'effectue désormais par le biais du service d'authentification Inclusion Connect. Cet outil permet d'accéder avec un unique compte à plusieurs services numériques de l'inclusion (Mon Suivi Social, RDV-Solidarités, et d'autres à venir). [#172](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/172)
- Lorsqu'un responsable de structure crée un nouvel utilisateur, un email avec un lien vers l'inscription Inclusion Connect est envoyé à l'adresse mail du nouvel utilisateur. [#106](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/106)

### 🚀 Modification

- L'alerte de réussite suite à la création d'un utilisateur est complété par la phrase "Pensez à l'informer de la réception de ce mail et de l'inviter si nécessaire à vérifier ses spams.".
- Amélioration de la visibilité des filtres. [#232](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/232)
- Les statistiques des types d'accompagnement sont triées du plus fréquent au moins fréquent. [#233](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/233)
- Le bouton retour sur les pages de création de synthèse d'entretien et de création d'instruction de demande d'aide ramènent sur l'onglet **Historique** de la fiche bénéficiaire. [#234](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/234)
- Dans les sélecteurs multiples et les sélecteurs multiples avec recherche, les options sélectionnées apparaissent en premier. [#213](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/213#note_1183982467)

### 🐛 Correction

- Correction d'un bug qui affiche en double les documents d'un bénéficiaire. [#225](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/225)
- Les structures de type association ou ministère n'ont plus accès au champ **N° de Sécurité Sociale** dans la fiche bénéficiaire. [#205](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/205)

### 🧽 Suppression

- Retrait du champ **Type** dans le formulaire de la structure car une structure n'est pas censée changer de typologie. [#235](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/235)

# 0.14.9 (7 décembre 2022)

### 🐛 Correction

- Correction d'un bug qui empêche l'application des filtres sur la liste des bénéficiaires. [#231](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/231)

# 0.14.8 (7 décembre 2022)

### 🐛 Correction

- Une erreur survient lorsque l'on accède au formulaire d'édition d'une fiche bénéficiaire et que l'on soumet le formulaire sans avoir effectué de changement.

# 0.14.7 (1er décembre 2022)

### 🐛 Correction

- Correction d'un bug qui affiche en double les documents d'un bénéficiaire. [#225](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/225)

# 0.14.6 (1er décembre 2022)

### 🐛 Correction

- Correction d'un bug qui empêche de voir les documents de la fiche bénéficiaire. [#226](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/226)
- Correction d'un bug qui empêche les agents de voir ou modifier les types d'accompagnement d'une synthèse d'entretien. [#226](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/226)

# 0.14.5 (25 novembre 2022)

### 🐛 Correction

- Dans la page **Accompagnements**, correction d'un bug qui affiche en double les synthèses d'entretien et les instructions de demande d'aide. [#223](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/223)

# 0.14.4 (25 novembre 2022)

### 🐛 Correction

- Correction d'un bug qui affiche l'historique du bénéficiaire en double si le bénéficiaire est affecté à plusieurs référents. [#219](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/219)

# 0.14.3 (24 novembre 2022)

### 🐛 Correction

- Correction d'un bug qui affichait toujours la même fiche bénéficiaire.

# 0.14.2 (24 novembre 2022)

### 🐛 Correction

- Correction d'un bug empêchant une synthèse d'entretien ou une instruction de demande d'aide nouvellement créée d'apparaître dans l'historique du bénéficiaire.

# 0.14.1 (24 novembre 2022)

### 🐛 Correction

- Correction d'un bug empêchant la création d'une synthèse d'entretien.

# 0.14.0 (24 novembre 2022)

### ✨ Ajout

- Ajout de la colonne **Montant demandé** dans le tableau des demandes d'instruction de la structure. [#211](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/211)

### 🚀 Modification

- Un bénéficiaire peut désormais avoir plusieurs référents. [#152](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/152)

# 0.13.1 (22 novembre 2022)

### 🐛 Correction

- Dans le formulaire de synthèse d'entretien, il n'est pas possible de sélectionner un type d'accompagnement en utilisant la souris si une recherche est saisie. [#213](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/213)
- Dans le formulaire de synthèse d'entretien, lorsque le champ Type d'accompagnement est vidé, les options sélectionnées le restent. [#213](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/213)

# 0.13.0 (18 novembre 2022)

### ✨ Ajout

- Pour les agents travaillant au sein d'un ministère social, ajout des champs **Structure**, **Département ou service de l'Administration Centrale** et **Catégorie** dans le formulaire de la fiche bénéficiaire. [#173](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/173)
- Ajout de séparateurs horizontaux autour des synthèses dans l'historique d'un bénéficiaire. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)
- Ajout de l’icône cadenas aux synthèses privées. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)

### 🚀 Modification

- Dans les formulaires de synthèse d'entretien et d'instruction de demande d'aide, le champ des types d'accompagnement utilise un champ de sélection avec recherche. [#186](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/186)
- Dans l'historique du bénéficiaire et la page **Accompagnements**, les synthèses d'entretien et les instructions de demande d'aide peuvent être différencié par un code couleur. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)
- Les détails d'une instruction de demande d'aide sont présentés sur 2 colonnes avec un séparateur vertical. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)
- Amélioration des alignements des champs filtres. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)

### 🐛 Correction

- Les informations des éléments de l'historique d'un bénéficiaire ne sont plus en lettres capitales. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)
- Les sauts de ligne à l'intérieur des synthèses s'affichent correctement. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)
- Correction du bug d'affichage quand les informations d'un élément de l'historique d'un bénéficiaire prennent toute la ligne (le nom du créateur est décalé). [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)

# 0.12.1 (15 novembre 2022)

### 🐛 Correction

- Les sections **Activité/Revenu** et **Autres informations** de la fiche bénéficiaire n'apparaissent pas alors qu'elles contiennent de l'information.

# 0.12.0 (10 novembre 2022)

### 🚀 Modification

- Mise à jour de technologie importante sous le capot. À l'avenir, l'application gagnera en performance et en stabilité. [#13](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/13)
- Dans la fiche bénéficiaire, le champ de texte libre **Organisme(s) de retraite** est remplacé par un champ à sélection multiple **Organismes de retraite** accompagné d'un champ de texte libre **Autres organismes de retraite** dans le cas où le choix **Autre(s)** est sélectionné. [#122](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/122)
- Les filtres s'appliquent désormais à la volée, sans passer par un bouton pour les appliquer. [#131](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/131)
- Les filtres peuvent être replié ou déplié pour gagner de l'espace. Lorsqu'ils sont repliés, les valeurs de filtres sélectionnées sont affichées. [#131](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/131)
- Pour les agents travaillant au sein d'un ministère social, ajout des champs **Numéro Pégase**, **Ministre** dans le formulaire d'instruction de demande d'aide. [#158](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/158)
- Pour les agents travaillant au sein d'un ministère social, ajout des champs **Numéro Pégase**, **Ministre** dans le formulaire de synthèse d'entretien. [#158](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/158)
- Amélioration de l'accessibilité des champs de sélection multiple. [#173](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/173)
- Amélioration de l'accessibilité des champs de recherche. [#173](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/173)
- Amélioration de l'accessibilité des champs avec autocomplétion. [#173](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/173)

### 🐛 Correction

- Les champs de texte pour la saisie d'une adresse ne donnent des résultats qu'à partir de 3 lettres, en accord avec l'API BAN.

## 0.11.0 (2 novembre 2022)

### ✨ Ajout

- Ajout du statut **Clôturé par le bénéficiaire** dans les instructions de demande d'aide. [#176](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/176)
- Ajout du statut **Classé sans suite** dans les instructions de demande d'aide. [#176](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/176)
- Ajout de l'historique des changements dans le formulaire de modification de la structure, dans la liste des bénéficiaires et dans la fiche bénéficiaire. [#104](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/104)

### 🚀 Modification

- Sur les tableaux ou les listes comportant des filtres, l'indicateur du nombre de résultats affichés est toujours au-dessus du tableau ou de la liste. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129#note_1122398289)
- Dans le formulaire d'instruction de demande d'aide, le champ **Date d'envoi du dossier** n'est plus affiché lorsque l'instruction est réalisée en interne. [#178](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/178)
- Lors de la modification d'une instruction de demande d'aide, le formulaire est verrouillé lorsque les statuts **Clôturé par le bénéficiaire** ou **Classé sans suite** sont sélectionnés. [#176](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/176#note_1126636367)
- Plusieurs types d'accompagnement peuvent être rattaché à une synthèse d'entretien. [#174](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/174)
- Dans l'historique des bénéficiaires, chaque type d'accompagnement est affiché dans son propre badge. [#174](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/174)
- Agrandissement des champs de synthèse dans la saisie d'une synthèse d'entretien ou d'une instruction de demande d'aide. [#174](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/174)

### 🐛 Correction

- Restructuration importante sous le capot pour que l'historisation des modifications et des ajouts soit plus lisible. [#104](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/104#note_1102000277)

## 0.10.1 (11 octobre 2022)

### 🐛 Correction

- Lors de la demande de réinitialisation de mot de passe, un message d'erreur est affiché alors que la demande a été correctement transmise. [#188](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/188)

## 0.10.0 (29 septembre 2022)

### ✨ Ajout

- Ajout du type d'accompagnement par défaut "Autre". Il est désormais sélectionné pour toutes les structures existantes. [#132](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/132)
- Dans la fiche bénéficiaire, ajout d'un champ **Informations complémentaires** pour permettre aux agents de renseigner des informations qui n'ont pas de champ prévu à cet effet. [#126](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/126)
- Dans la fiche bénéficiaire, ajout d'un champ **Précisions hébergement**. [#127](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/127)
- Lorsque le nom ou le prénom d'un bénéficiaire n'est pas présent dans sa fiche, celui-ci est affiché comme "(non renseigné)" lorsqu'il doit apparaître.
- Dans la fiche bénéficiaire, ajout de l'option **APL** à tous les champs en lien avec la nature des ressources. [#163](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/163)
- Dans le formulaire d'instruction de demande d'aide, ajout des champs **Dossier complet**, **Date d'échéance** et **Motif**. [#161](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/161#note_1101597442)
- Dans le formulaire de synthèse d'entretien, ajout des types d'entretien **Visioconférence** et **Entretien avec un tiers**. Dans le cas du choix **Entretien avec un tiers**, un champ **Nom du tiers** apparaît. [#48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/48#note_1092882626)
- Dans le formulaire de synthèse d'entretien, ajout d'un champ **Date d'échéance**. [#48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/48#note_1092882626)
- Dans la fiche bénéficiaire, ajout d'un champ **N'a pas de téléphone**. Lorsque ce champ est coché, les champs **Téléphone 1** et **Téléphone 2** sont masqués. [#128](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/128)
- Sur les tableaux ou les listes comportant des filtres, ajout d'un indicateur du nombre de résultats affichés. [#129](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/129)

### 🚀 Modification

- Met à jour le logo de l'Agence Nationale de la Cohésion des Territoires dans le pied de page de la page d'accueil.
- Dans la page **Accompagnements**, l'onglet **Instructions** est renommé en **Demandes d'aide**, par soucis de consistance.
- La validation des numéros de Sécurité Sociale a été améliorée.
- Réorganisation des champs du formulaire d'instruction de demande d'aide. [#161](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/161#note_1101597442)
- Réorganisation des champs du formulaire de synthèse d'entretien. [#48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/48#note_1092882626)
- Les options dans les champs à sélection sont désormais classés par ordre alphabétique. [#163](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/163#note_1113992188)

### 🐛 Correction

- Correction d'un espacement entre les badges d'une synthèse d'entretien.
- L'historique d'un bénéficiaire classe dans l'ordre chronologique les synthèses d'entretien et les demandes d'aide.
- Les erreurs s'affichent sur les pages de connexion et de réinitialisation de mot de passe.
- Lors de l'édition d'une synthèse d'entretien, son statut n'est plus systématiquement initialisé à **Terminé**. [#157](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/157)
- Lors de l'édition d'une synthèse d'entretien, il est désormais possible de désélectionner **Réorienté après l'entretien** et **Instruire une demande**.
- Le numéro de téléphone des personnes de l'entourage est correctement formaté. [#125](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/125)
- Les numéros de téléphone et de sécurité sociale sont correctement formatés lorsque l'utilisateur efface le dernier espace inséré. [#125](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/125)
- Dans la fiche bénéficiaire, les menus déroulants des filtres de l'onglet Historique s'affichent en-dessous des encadrés des synthèses d'entretien et des instructions de demande d'aide de l'historique.
- La nationalité s'affiche correctement dans la fiche bénéficiaire.
- Certaines fiches bénéficiaires ne sont plus accessibles lors qu'un des éléments de son historique (synthèse d'entretien ou instruction de demande d'aide) est associé à un type de suivi désélectionné.

## 0.9.1 (19 juillet 2022)

### 🚀 Modification

- Dans la fiche bénéficiaire, renommage du champs **Statut** en **Statut du dossier** pour limiter la confusion avec le statut professionnel du bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_1030557692)

## 0.9.0 (19 juillet 2022)

### ✨ Ajout

- Dans la page Statistiques, ajout d'une section qui détaille la répartition des types de suivi par rapport au nombre de bénéficiaires au sein de la structure. [#52](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/52)
- Ajout de textes d'état vide aux blocs de statistiques.

### 🚀 Modification

- La liste des types d'accompagnement d'un bénéficiaire n'est plus modifiable dans le formulaire de la fiche bénéficiaire. Elle est maintenant automatiquement générée à partir des types d'accompagnement utilisés pour les synthèses d'entretien et les instructions de demande d'aide du bénéficiaire. [#100](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/100)

## 0.8.2 (12 juillet 2022)

### ✨ Ajout

- La page **Bénéficiaires** dispose de la barre de recherche de bénéficiaire.
- Dans le formulaire d'instruction de demande d'aide, ajout de l'option **Carte bancaire** aux modes de paiement.
- Dans les formulaires de synthèse d'entretien et d'instruction de demande d'aide, ajout d'un texte d'aide sur le champ des types d'accompagnement pour indiquer aux agents que les responsables de leur structure peuvent personnaliser les types d'accompagnement.
- Dans les statiques de répartition de genre, ajout du segment **Non renseigné**.

### 🚀 Modification

- Le lien vers la simulation de droits sociaux Mes Droits Sociaux est renommé pour être plus explicite. [#68](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/68#note_1023507624)

### 🐛 Correction

- Lorsqu'une instruction de demande d'aide passe du statut **Accepté** à **Refusé** (correction d'une erreur de saisie), les informations relatives au statut **Accepté** ne sont plus affichées. La même correction est appliquée au passage du statut **Refusé** à **Accepté**.

## 0.8.1 (12 juillet 2022)

### 🐛 Correction

- Dans le formulaire d'instruction de demande d'aide, les champs correspondant à l'étape **En cours d'instruction** ne sont pas affichés.

## 0.8.0 (12 juillet 2022)

### ✨ Ajout

- Dans le formulaire de la fiche bénéficiaire, dans les champs **Nature des ressources**, ajout de l'option **AAH**.
- Ajout d'un message indiquant lorsque les filtres ne donnent aucun résultat dans l'onglet Historique de la fiche bénéficiaire.
- Ajout d'un lien pour revenir au tableau de bord sur le logo Mon Suivi Social du menu latéral de l'application.
- Ajout du formulaire pour enregistrer et modifier une instruction de demande d'aide pour un bénéficiaire. Le formulaire est accessible directement depuis la fiche bénéficiaire. [#49](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/49)
- Dans la page Accompagnements, ajout d'un tableau qui rassemble toutes les instructions de demande d'aide enregistrées au sein de la structure. [#96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/96)
- Ajout de filtres spécifiques aux instructions de demande d'aide.
- Dans la fiche bénéficiaire, dans l'onglet Historique, ajout des instructions de demande d'aide enregistrées pour le bénéficiaire concerné. [#50](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/50)
- Dans la fiche bénéficiaire, ajout d'un lien vers la simulation d'accès aux droits Mes Droits Sociaux. [#68](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/68)
- Ajout de la page Statistiques qui détaille la répartition des genres au sein de la structure. [#51](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/51)
- Dans le formulaire de la fiche bénéficiaire, ajout de l'option **Suivi cabinet** aux modes d'orientation.
- Dans les formulaires de synthèse d'entretien et d'instruction de demande d'aide, ajout d'un texte d'aide sur le champ des types d'accompagnement pour rediriger vers la personnalisation de la structure. [#113](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/113)
- Dans la fiche bénéficiaire, dans la section Informations générales, ajout des champs **Administration** et **Ministre** dans le cas où l'agent est un un agent ministériel. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_1022407836)

### 🚀 Modification

- Le tableau des bénéficiaires prend l'intégralité de l'espace disponible.
- Dans la page Accompagnements, le tableau des synthèses d'entretien est dans un onglet Entretiens.
- Dans la page Accompagnements, les colonnes **Statut** des tableaux de synthèses et d'instructions se présentent sous la forme de badges visuellement identifiables.
- La présentation des alertes en cas de succès ou d'échec est repensée afin d'être plus visible.

### 🐛 Correction

- Lors de l'édition d'une synthèse d'entretien marquée comme "Réorienté après l'entretien" lors de sa création, la case à cocher est initialement cochée.
- Pour des raisons de sécurité, l'autocomplétion est désactivée sur tous les champs de formulaire.
- Lorsque l'on enregistre une seconde fois le formulaire d'édition de sa structure, les nouveaux types d'accompagnement sont enregistrés deux fois. [#107](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/107)

## 0.7.0 (29 juin 2022)

### ✨ Ajout

- Ajout des champs **Nature de la ressource principale du conjoint** et **Montant de la ressource principale du conjoint** à la section Activité/Revenu dans le formulaire de la fiche bénéficiaire, lorsque la situation familiale sélectionnée implique un conjoint. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_994295949)
- Ajout du formulaire pour enregistrer et modifier une synthèse suite à un entretien avec un bénéficiaire. Le formulaire est accessible directement depuis la fiche bénéficiaire. [#48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/48)
- Ajout de la page Accompagnements qui rassemble toutes les synthèses enregistrées au sein de la structure. [#96](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/96)
- Dans la fiche bénéficiaire, ajout d'un onglet Historique qui rassemble toutes les synthèses enregistrées pour le bénéficiaire concerné. [#50](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/50)
- Ajout d'un lien de retour sur les formulaires pour revenir à la page précédente. [#48](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/48#note_1002536112)
- Ajout du choix **Ministère** aux types de structure possibles.
- Dans le formulaire de la fiche bénéficiaire, dans la section Activité/Revenu, ajout des champs **Nature des ressources des enfants majeurs** et **Montant des ressources des enfants majeurs** lorsque le bénéficiaire a au moins un enfant majeur. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_1001086757)
- Ajout d'un message d'erreur explicite lorsque l'on tente de créer un utilisateur avec un email déjà utilisé.

### 🚀 Modification

- Les champs monétaires affichent maintenant l'unité (€) et le format attendu. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_994295949)
- Le design des champs de choix multiples a été revu pour un usage simplifié.
- Les types de suivi ont été renommé en types d'accompagnement.
- Dans la section Activité/Revenu dans le formulaire de la fiche bénéficiaire, les champs **Nature de la ressource principale** et **Nature de la ressource principale du conjoint** deviennent des choix multiples. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_995613742)
- La barre de recherche du tableau de bord occupe tout l'espace disponible.
- Dans toute l'application, les types d'accompagnement sont classés par ordre alphabétique.
- Dans l'espace document de la fiche bénéficiaire, les documents sont classés par ordre chronologique. [#18](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/18#note_1001092172)
- Certains filtres de la liste des bénéficiaires sont à choix multiples.
- L'accessibilité des champs à sélection multiple et des menus déroulants a été améliorée.
- Le nom et le prénom ne sont plus requis pour la création d'une fiche bénéficiaire.
- L'illustration de la page d'accueil représente plus de diversité.

### 🐛 Correction

- Les champs avec autocomplétion qui sont requis sont indiqués comme tel et vérifié dans la validation du formulaire.
- Les documents avec un nom de fichier trop longs sont correctement coupés.
- Les tris par ordre alphabétique dans le tableau des bénéficiaires ne différencie plus les majuscules des minuscules.
- Le nombre d'enfants majeurs ou mineurs renseigné ne peut plus être négatif.

## 0.6.2 (14 juin 2022)

### ✨ Ajout

- Ajout de l'option **En couple avec enfant(s)** aux situations familiales dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_984365756)

### 🐛 Correction

- Corrige un bug empêchant l'enregistrement d'une fiche bénéficiaire car le bouton d'enregistrement est bloqué en état de chargement.

## 0.6.0 (10 juin 2022)

### ✨ Ajout

- Il est désormais possible de filtrer le tableau des bénéficiaires en fonction d'un type de suivi, d'un référent, d'une commune, d'un statut et d'une tranche d'âge. [#72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/72)
- Il est désormais possible de trier le tableau des bénéficiaires en fonction des noms et des prénoms. [#72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/72)
- Ajout des colonnes **Âge** et **Statut** au tableau des bénéficiaires. [#90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/90)
- Ajout de l'option **Séparé·e** aux situations familiales dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout des champs **Vit à domicile** et **Aidant familial** aux proches dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout du champ **Catégorie socio-professionnelle** à la section Activité/Revenu dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout du champ **SIRET de l'employeur** à la section Activité/Revenu dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout du champ **Nature de la ressource principale** à la section Activité/Revenu dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout du champ **Montant de la ressource principale** à la section Activité/Revenu dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout du champ **Contrat obsèques** à la section Activité/Revenu dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Ajout d'indicateurs de chargements lorsque de la donnée est attendue ou envoyée. [#95](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/95)

### 🚀 Modification

- L'ordre des colonnes du tableau des bénéficiaires a été mis à jour pour mettre en avant les informations les plus importantes. [#90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/90)
- La barre de navigation latéral occupe moins d'espace. [#90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/90)
- La largeur de la colonne **Adresse** est limitée dans le tableau des bénéficiaires. [#90](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/90)
- Remplacement du terme **Ville de naissance** par "Lieu de naissance". [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- La catégorisation d'un bénéficiaire en aidant familial est déplacé de la liste des situations familiales vers une option à cocher dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Le format du champs **Téléphone** des proches d'un bénéficiaire n'est pas imposé pour permettre d'ajouter des numéros de téléphones internationaux. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Aucun champs est requis pour ajouter un proche à un bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Le formatage des champs de numéro de Sécurité Sociale ne comprend plus la clé de sécurité. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Séparation du champ **Profession(s) / Employeur** en deux champs distincts dans la section Activité/Revenu dans le formulaire de la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_970379719)
- Les aides légales et les aides facultatives sont séparées dans la fiche bénéficiaire. [#93](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/93)
- Les aides légales et les aides facultatives sont séparées dans le formulaire de la fiche bénéficiaire. [#93](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/93)

## 0.5.1 (21 mai 2022)

### 🚀 Modification

- Dans le formulaire de la fiche bénéficiaire, l'accordéon qui contient le premier champ invalide s'ouvre pour que l'utilisateur puisse trouver le champ invalide. [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/67#note_954715371)

## 0.5.0 (20 mai 2022)

### ✨ Ajout

- Ajout de la réinitialisation de mot de passe à la page de connexion.
- Ajout de la tranche d'âge à côté de la date de naissance dans la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_954364250)
- Ajout des types de suivi dans le tableau des bénéficiaires. [#72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/72)
- Ajout des agents référents dans le tableau des bénéficiaires. [#72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/72)
- Le tableau des bénéficiaires est défilable pour afficher plus d'informations. [#72](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/72)

## 0.4.0 (19 mai 2022)

### ✨ Ajout

- Ajout de l'espace document dans la fiche bénéficiaire. [#18](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/18)
- Ajout d'un champ "Mandat Aidant Connect" dans la fiche bénéficiaire. [#76](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/76)
- Ajout du statut "Décédé" dans la fiche bénéficiaire. Ajout d'un champ "Date de décès" le cas échéant. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_949144710)
- Ajout des situations familiales "Pacsé" et "Divorcé" dans la fiche bénéficiaire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_949144710)
- Ajout de la mention obligatoire de conformité relative à l'accessibilité. [#80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/80)
- Ajout de la déclaration d'accessibilité. [#80](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/80)

### 🚀 Modification

- Les champs de texte correspondant à un numéro de téléphone fournissent un format à respecter. [#62](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/62)
- Les champs de texte correspondant à un numéro de Sécurité Social fournissent un format à respecter. [#62](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/62)
- Augmentation de l'ombre portée des menus interactifs pour les rendre plus visible.

## 0.3.0 (13 mai 2022)

### ✨ Ajout

- Ajout des crédits pour les illustrations dans le pied de page de la page d'accueil.
- Ajout d'un bouton pour retirer un proche de l'entourage d'un bénéficiaire. [#65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/65)
- Une structure peut choisir les types de suivi qu'elle propose. Elle peut également ajouter des types de suivi personnalisés. [#39](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/39)

### 🚀 Modification

- Le bouton pour modifier la fiche d'un bénéficiaire est plus visible et explicite. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75)
- Le champ "Civilité" du formulaire de la fiche bénéficiaire n'est plus obligatoire. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75)
- Le bouton de confirmation des formulaires n'est plus désactivé quand le formulaire est invalide. [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/67)
- La validation des champs d'un formulaire se fait à la confirmation du formulaire et de manière plus explicite. [#67](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/67)
- Le bouton de la page d'accueil "Enregistrer ma structure" devient "Contactez-nous" et redirige vers l'email de Vanessa.
- Amélioration du design de la section "Entourage" du formulaire de la fiche du bénéficiaire pour plus de clarté. [#65](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/65)
- Amélioration du design de la page d'accueil.
- Amélioration du design de la fiche bénéficiaire pour plus de clarté. [#75](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/75#note_943940231)

### 🐛 Correction

- Correction d'un bug dans la validation des caractères spéciaux d'un mot de passe.

## 0.2.0 (9 mai 2022)

### ✨ Ajout

- Des suggestions d'adresses postales sont proposées lorsque l'utilisateur saisit une adresse dans la fiche bénéficiaire ou dans les informations de sa structure. [#61](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/61)
- Ajout de l'option "Autre membre de la famille" aux types de relation pour l'entourage d'un bénéficiaire. [#4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/4#note_933000814)
- Ajout de l'option "EHPAD, résidence senior" aux mode d'hébergement dans le formulaire de la fiche bénéficiaire. [#44](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/44#note_932993742)

### 🚀 Modification

- La session d'un utilisateur ne persiste plus lorsque l'onglet ou le navigateur est fermé. Il lui faudra de nouveau s'authentifier pour accéder à l'application. [#12](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/12#note_930879457)
- La date de naissance d'un bénéficiaire s'affiche au format français dans sa fiche et dans la liste des bénéficiaires. [#8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/8#note_933022409)
- Le champ "Téléphone 1" n'est plus obligatoire dans le formulaire de la fiche bénéficiaire. [#4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/4#note_932998147)
- Le champ "Nationalité" dans le formulaire de la fiche bénéficiaire devient un champ sélectionnable et non plus un champ libre. [#69](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/69)
- Réduction du poids des illustrations de la page d'accueil de 60%.
- La fiche du bénéficiaire et les formulaires de création et de modification de bénéficiaire utilisent les types de suivi associés à la structure de l'agent. [#56](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/56), [#57](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/57)

### 🐛 Correction

- Correction d'un bug empêchant de vider une valeur de la fiche bénéficiaire.
- Correction d'un bug empêchant l'ajout d'une relation à l'entourage d'un bénéficiaire existant. [#4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/4#note_933007098)

### 🧽 Suppression

- Retrait temporaire du lien "Mot de passe oublié ?" sur l'écran de connexion.

## 0.1.0-0 (29 avril 2022)

### ✨ Ajout

- Ajout de la page d'accueil. [#11](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/11), [#35](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/35)
- Un utilisateur peut s'authentifier pour accéder à l'application. [#12](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/12)
- Un utilisateur peut créé une fiche bénéficiaire au sein de sa structure. [#4](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/4)
- Un utilisateur peut accéder à la liste des bénéficiaires de sa structure. [#8](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/8)
- Un utilisateur peut consulter la fiche d'un bénéficiaire de sa structure. [#14](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/14)
- Un utilisateur peut modifier une fiche bénéficiaire. [#34](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/34)
- Un utilisateur peut rechercher une fiche bénéficiaire depuis le tableau de bord. [#15](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/15)
- Un utilisateur authentifié peut mettre à jour son mot de passe. [#36](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/36)
- Un responsable de structure peut inviter des utilisateurs au sein de sa structure. [#3](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/3)
- Un responsable de structure peut modifier les informations de sa structure. [#60](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/60)
- Le référentiel de Carnet de Bord est utilisés pour les champs de la fiche bénéficiaire. [#44](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-app/-/issues/44)
