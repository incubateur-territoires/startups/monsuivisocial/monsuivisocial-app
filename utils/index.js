import dayjs from 'dayjs'
import { DOCUMENT_TYPE_OPTIONS } from './constants/document'
import { AgeGroup } from './constants/age-groups'
import { RELATIVE_TYPE_OPTIONS } from './constants/relative'
import { NEW_OPTION_PREFIX } from './constants'
import { beneficiaryName } from './beneficiary'

export function generateNewEmptyDocumentList() {
  return [...DOCUMENT_TYPE_OPTIONS.keys()].reduce(
    (documents, type) => ({ ...documents, [type]: [] }),
    {}
  )
}

export function dateIsSameOrBefore(date, upper) {
  return date.isSameOrBefore(upper, 'day')
}

export function dateIsSameOrAfter(date, lower) {
  return date.isSameOrAfter(lower, 'day')
}

export function dateIsBetween(date, lower, upper) {
  return date.isBetween(lower, upper, 'day', '[]')
}

export function ageGroupToDateRange(ageGroup) {
  switch (ageGroup) {
    case AgeGroup.Less25:
      return { lower: dayjs().subtract(26, 'year').add(1, 'day') }
    case AgeGroup.Group26_34:
      return {
        lower: dayjs().subtract(35, 'year').add(1, 'day'),
        upper: dayjs().subtract(26, 'year')
      }
    case AgeGroup.Group35_44:
      return {
        lower: dayjs().subtract(45, 'year').add(1, 'day'),
        upper: dayjs().subtract(35, 'year')
      }
    case AgeGroup.Group45_54:
      return {
        lower: dayjs().subtract(55, 'year').add(1, 'day'),
        upper: dayjs().subtract(45, 'year')
      }
    case AgeGroup.Group55_64:
      return {
        lower: dayjs().subtract(65, 'year').add(1, 'day'),
        upper: dayjs().subtract(55, 'year')
      }
    case AgeGroup.More65:
      return { upper: dayjs().subtract(65, 'year') }
  }
}

export function agentToAgentOption(agent) {
  return [agent.id, `${agent.first_name} ${agent.last_name}`]
}

export function toOptions(arr, valueKey, labelKey) {
  return new Map(arr.map(e => [e[valueKey], e[labelKey]]))
}

export function withEmptyValue(a, emptyLabel = '') {
  return new Map([['', emptyLabel], ...a.entries()])
}

/**
 * Sort an array of objects
 * @param array An array of objects
 * @param sorts An array of sort objects. It has a field key that corresponds to the field to sort and a sort key that indicates if sorting is applied ascending or descending
 */
export function sort(array, sorts) {
  return array.sort((a, b) => {
    return sorts.reduce((delta, sort) => {
      if (delta) return delta

      const valueA = getNestedValue(a, sort.field)
      const valueB = getNestedValue(b, sort.field)

      if (!valueA && !valueB) return 0
      if (!valueA) return -parseInt(`${sort.sort}1`)
      if (!valueB) return parseInt(`${sort.sort}1`)

      if (typeof valueA !== 'string' || typeof valueB !== 'string') {
        throw new TypeError(
          'Not Implemented : Nested sort only handles string for now.'
        )
      }

      const insensitiveA = valueA.toLowerCase()
      const insensitiveB = valueB.toLowerCase()

      if (insensitiveA < insensitiveB) return -parseInt(`${sort.sort}1`)
      if (insensitiveA > insensitiveB) return parseInt(`${sort.sort}1`)
      return 0
    }, 0)
  })
}

/**
 * Get a value within an object from a field path
 * @param obj The object
 * @param field The field path in dot notation (i.e. : 'foo.bar.baz')
 */
export function getNestedValue(obj, field) {
  const [firstNestedField, ...followingNestedFields] = field.split('.')
  if (!(firstNestedField in obj)) return undefined
  const nestedValue = obj[firstNestedField]
  if (followingNestedFields.length) {
    if (typeof nestedValue === 'object' && nestedValue) {
      return getNestedValue(nestedValue, followingNestedFields.join('.'))
    }
    return undefined
  }
  return nestedValue
}

export function parseKindOfBoolean(value) {
  return value === 'true'
}

/**
 * Get an object containing the differences between an original and an updated
 * Directus item.
 * It ignores Directus readonly fields.
 * It cast digit strings to number for the comparison.
 * For array fields, it ignores sort and adds the full array to the diff object
 * if any difference exists.
 * For array fields containing nested relations, it adds to the diff object the
 * diff for existing nested relations, it adds to the diff object the full
 * nested relation for new ones, it adds to the diff object ids of unchanged
 * nested relations.
 * /!\ This diff should not be used for other diff purpose than Directus items.
 * @param originalItem The original Directus item
 * @param updatedItem The updated Directus item
 */
export function directusItemDiff(originalItem, updatedItem) {
  const diff = Object.entries(updatedItem).reduce(
    (_diff, [key, updatedValue]) => {
      if (READONLY_FIELDS.includes(key)) return _diff

      const originalValue = originalItem[key]
      if (Array.isArray(updatedValue)) {
        if (!originalValue || !Array.isArray(originalValue)) {
          return addChange(_diff, key, updatedValue)
        }

        /**
         * To determine if the considered array is containing objects, it checks
         * if original array is containing objects, or, in case original array
         * was empty, it checks if updated array is containing objects.
         */
        const isObjectArray =
          (originalValue.length &&
            originalValue[0] &&
            typeof originalValue[0] === 'object') ||
          (updatedValue.length &&
            updatedValue[0] &&
            typeof updatedValue[0] === 'object')

        if (isObjectArray) {
          if (!updatedValue.every(v => typeof v === 'object')) {
            throw new Error(
              `Nested arrays should only contain objects if it has any (key: ${key}).`
            )
          }

          const nestedArrayDiff = updatedValue.map(updatedArrayItem => {
            const itemId = updatedArrayItem.id
            if (!itemId) return updatedArrayItem
            const originalArrayItem = originalValue.find(
              originalItem => originalItem.id === itemId
            )
            const arrayItemDiff = directusItemDiff(
              originalArrayItem,
              updatedArrayItem
            )
            return arrayItemDiff ? { id: itemId, ...arrayItemDiff } : itemId
          })

          /**
           * If the diff of the nested array does not contain any object and the
           * length has not changed, then only already existing items remains so
           * the whole nested array is unchanged and can be ignore in the item
           * diff.
           */
          if (
            updatedValue.length !== originalValue.length ||
            nestedArrayDiff.some(change => typeof change === 'object')
          ) {
            return addChange(_diff, key, nestedArrayDiff)
          }
          return _diff
        }

        if (
          updatedValue.length !== originalValue.length ||
          updatedValue.some(v => !originalValue.includes(v)) ||
          originalValue.some(v => !updatedValue.includes(v))
        ) {
          return addChange(_diff, key, updatedValue)
        }
        return _diff
      }

      if (typeof updatedValue === 'object' && updatedValue !== null) {
        const nestedObjectDiff = directusItemDiff(originalValue, updatedValue)
        return nestedObjectDiff
          ? addChange(_diff, key, nestedObjectDiff)
          : _diff
      }

      /**
       * For number fields, it compares using a stringified value as these
       * fields are returned as numbers by Directus but parsed as a string from
       * the form.
       */
      if (typeof originalValue === 'number') {
        return updatedValue === originalValue ||
          updatedValue === `${originalValue}`
          ? _diff
          : addChange(_diff, key, updatedValue)
      }

      /**
       * Directus returns the null value for a boolean value that has never been
       * set or updated.
       */
      if (
        typeof updatedValue === 'boolean' &&
        updatedValue === false &&
        originalValue === null
      ) {
        return _diff
      }

      return updatedValue === originalValue
        ? _diff
        : addChange(_diff, key, updatedValue)
    },
    null
  )
  return diff
}

function addChange(diff, key, value) {
  return diff ? { ...diff, [key]: value } : { [key]: value }
}

export const READONLY_FIELDS = [
  'user_updated',
  'date_updated',
  'user_created',
  'date_created'
]

/**
 * Return the item with all empty fields (even nested) set to null
 * @param item The item
 */
export function setEmptyFieldsToNull(item) {
  return Object.entries(item).reduce(
    (_item, [key, value]) => {
      if (Array.isArray(value)) {
        if (value[0] && typeof value[0] === 'object') {
          return { ..._item, [key]: value.map(v => setEmptyFieldsToNull(v)) }
        }
        return { ..._item, [key]: value }
      }

      if (typeof value === 'object' && value !== null) {
        return {
          ..._item,
          [key]: setEmptyFieldsToNull(value)
        }
      }

      return value === '' ? { ..._item, [key]: null } : { ..._item }
    },
    { ...item }
  )
}

export function checkboxValuesToBoolean(item, checkboxKeys) {
  const _item = { ...item }
  checkboxKeys.forEach(key => {
    _item[key] = _item[key] === 'on'
  })
  return _item
}

/**
 * Prepare a list of M2M relations from an initial list and a list of selected foreign ids
 * @param initialRelations The initial list of M2M relations
 * @param selectedForeignIds The new list of foreign ids
 * @param foreignIdKey The key of the foreign id in an M2M relation
 * @returns The updated list of M2M relations
 */
export function prepareM2MRelation(
  initialRelations,
  selectedForeignIds,
  foreignIdKey
) {
  return selectedForeignIds.map(foreignId => {
    const existingRelation = getExistingRelation(
      initialRelations,
      foreignId,
      foreignIdKey
    )
    return existingRelation || { [foreignIdKey]: foreignId }
  })
}

/**
 * Get an M2M relation from a list matching a foreign id
 * @param initialRelations The initial list of M2M relations
 * @param foreignId The foreign id to find in the list of M2M relations
 * @param foreignIdKey The key of the foreign id in an M2M relation
 * @returns The M2M relation if it exists
 */
function getExistingRelation(initialRelations, foreignId, foreignIdKey) {
  if (!initialRelations) return null
  return initialRelations.find(r => {
    const rForeignId = r[foreignIdKey]
    if (typeof rForeignId === 'string') return rForeignId === foreignId
    return rForeignId.id === foreignId
  })
}

export function agentName(agent) {
  return `${agent.first_name} ${agent.last_name.toUpperCase()}`
}

export function age(birthdate) {
  return dayjs().diff(dayjs(birthdate), 'year')
}

export function formatDate(date, format = 'DD/MM/YYYY') {
  if (!date) return ''
  return dayjs(date).format(format)
}

export function readableFileNumber(beneficiaryId) {
  return beneficiaryId.slice(0, 8).toUpperCase()
}

const KILO_OCTET = 1_000
const MEGA_OCTET = 1_000_000
const GIGA_OCTET = 1_000_000_000

export function readableFileSize(size) {
  /* eslint-disable no-irregular-whitespace */
  if (size < KILO_OCTET) return `${size} o`

  let dotSize = ''

  if (size < MEGA_OCTET) dotSize = `${(size / KILO_OCTET).toFixed(2)} ko`
  else if (size < GIGA_OCTET) dotSize = `${(size / MEGA_OCTET).toFixed(2)} Mo`
  else dotSize = `${(size / GIGA_OCTET).toFixed(2)} Go`
  /* eslint-enable no-irregular-whitespace */

  return dotSize.replace('.', ',')
}

export function readableFileType(mimeType) {
  if (mimeType !== 'image/svg+xml' && mimeType.match(/^image\//)) {
    return mimeType.replace('image/', '').toUpperCase()
  }

  switch (mimeType) {
    case 'application/pdf':
      return 'PDF'
    case 'image/svg+xml':
      return 'SVG'
    case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
      return 'XLSX'
    case 'application/vnd.ms-excel':
      return 'XLS'
    case 'application/vnd.oasis.opendocument.spreadsheet':
      return 'ODS'
    case 'application/msword':
      return 'DOC'
    case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
      return 'DOCX'
    case 'application/vnd.oasis.opendocument.text':
      return 'ODT'
    default:
      return mimeType
  }
}

export function euros(price) {
  const [integerPart, decimalPart] = price.toString().split('.')
  const firstDigit = decimalPart?.length > 0 ? decimalPart[0] : '0'
  const secondDigit = decimalPart?.length > 1 ? decimalPart[1] : '0'

  return `${integerPart},${firstDigit}${secondDigit} €`
}

export function girLabel(girValue) {
  return `Niveau ${girValue.replace('gir_', '')}`
}

export function relativeGeneralInformation(relative) {
  const { firstname, lastname, type, beneficiary } = relative
  let generalInformation = ''

  if (beneficiary) {
    generalInformation = beneficiaryName(beneficiary)
  } else {
    generalInformation = beneficiaryName({ firstname, usual_name: lastname })
  }

  if (type) {
    const typeLabel = RELATIVE_TYPE_OPTIONS.get(type) || type
    generalInformation += ` - ${typeLabel}`
  }

  return generalInformation
}

export function relativeContact(relative) {
  const { city, phone, email, beneficiary } = relative
  let contact = ''
  const relativePhone =
    (beneficiary && (beneficiary.phone_1 || beneficiary.phone_2)) ||
    (!beneficiary && phone)
  const relativeEmail =
    (beneficiary && beneficiary.email) || (!beneficiary && email)

  if (!beneficiary && city) {
    contact += city
    if (relativePhone || relativeEmail) contact += ' / '
  }
  if (relativePhone) {
    contact += relativePhone
    if (relativeEmail) contact += ' / '
  }
  if (relativeEmail) contact += relativeEmail

  return contact
}

/**
 * Map a created option from a searchable select to the input label
 */
export function newOptionToLabel(newOption) {
  return newOption.replace(NEW_OPTION_PREFIX, '').replace('_', ' ')
}
