/**
 * Define the access to a route based on whether the authenticated user has created a given item
 * @param app The Nuxt context
 * @param navigateTo The Vue Router redirection function
 * @param permissionsStore The permissions Pinia store
 * @param collection The item collection
 * @param id The item id
 * @param redirectPath The path of the route to redirect to on access not allowed
 * @returns Redirects to the specified route if the access is refused
 */
export async function ownsItem(
  app,
  navigateTo,
  permissionsStore,
  collection,
  id,
  redirectPath
) {
  let allowed = false

  try {
    const res = await app.$getItem(collection, id, {
      fields: ['user_created.id']
    })

    if (res.status === 200 && permissionsStore.ownsItem(res.data.data)) {
      allowed = true
    }
  } catch {
    allowed = false
  }

  if (!allowed) return navigateTo(redirectPath)
}
