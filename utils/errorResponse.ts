import type { DirectusErrorResponse } from '~/types/directus'
import { UNKNOWN_ERROR_MESSAGE } from '~/utils/constants'

export const errorResponseMessage = ({
  data
}: {
  data?: DirectusErrorResponse
}) => data?.errors[0]?.message || UNKNOWN_ERROR_MESSAGE
