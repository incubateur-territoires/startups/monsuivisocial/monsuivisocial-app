const AppRoute = '/app'
const BeneficiariesRoute = `${AppRoute}/beneficiaries`
const BeneficiaryRoute = (beneficiaryId: string) =>
  `${BeneficiariesRoute}/${beneficiaryId}`
const BeneficiaryHelpRequestsRoute = (beneficiaryId: string) =>
  `${BeneficiaryRoute(beneficiaryId)}/help-requests`
const BeneficiaryInterviewsRoute = (beneficiaryId: string) =>
  `${BeneficiaryRoute(beneficiaryId)}/interviews`

export const Routes = {
  Index: '/',
  Register: '/register',
  DeclarationAccessibilite: `/declaration-accessibilite`,
  MentionsLegales: `/mentions-legales`,

  Login: '/login',
  Logout: `${AppRoute}/logout`,
  Registration: '/registration',
  SsoCallback: '/sso-callback',

  App: `${AppRoute}`,
  Overview: `${AppRoute}/overview`,

  Beneficiaries: BeneficiariesRoute,
  BeneficiariesAdd: `${BeneficiariesRoute}/add`,

  Beneficiary: BeneficiaryRoute,
  BeneficiaryPrint: (beneficiaryId: string) =>
    `${BeneficiaryRoute(beneficiaryId)}/print`,
  BeneficiaryEdit: (beneficiaryId: string) =>
    `${BeneficiaryRoute(beneficiaryId)}/edit`,

  BeneficiaryHelpRequestsAdd: (beneficiaryId: string) =>
    `${BeneficiaryHelpRequestsRoute(beneficiaryId)}/add`,
  BeneficiaryHelpRequestsEdit: (beneficiaryId: string, helpRequestId: string) =>
    `${BeneficiaryHelpRequestsRoute(beneficiaryId)}/${helpRequestId}/edit`,

  BeneficiaryInterviewsAdd: (beneficiaryId: string) =>
    `${BeneficiaryInterviewsRoute(beneficiaryId)}/add`,
  BeneficiaryInterviewsEdit: (beneficiaryId: string, interviewId: string) =>
    `${BeneficiaryInterviewsRoute(beneficiaryId)}/${interviewId}/edit`,

  History: `${AppRoute}/history`,

  OrganisationEdit: `${AppRoute}/organisation/edit`,

  Users: `${AppRoute}/users`,

  Stats: `${AppRoute}/stats`,

  Account: `${AppRoute}/account`
}
