export function prepareFollowUpTypes(
  initialFollowUpTypes,
  selectedFollowUpTypeIds,
  createdFollowUpTypes
) {
  return selectedFollowUpTypeIds.map(followUpTypeId => {
    const existingRelation = getExistingRelation(
      initialFollowUpTypes,
      followUpTypeId
    )
    if (!existingRelation) {
      const createdFollowUpType = createdFollowUpTypes.find(
        f => f.id === followUpTypeId
      )
      if (createdFollowUpType) {
        const { name, type } = createdFollowUpType
        return { follow_up_types_id: { name, type } }
      }
      return { follow_up_types_id: followUpTypeId }
    }
    return existingRelation
  })
}

function getExistingRelation(initialFollowUpTypes, followUpTypeId) {
  return initialFollowUpTypes.find(f => f.follow_up_types_id === followUpTypeId)
}
