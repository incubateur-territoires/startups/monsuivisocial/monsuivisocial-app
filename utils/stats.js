function prepareDateFilters(filters) {
  const dateFilters = {}
  const MIDNIGHT_SUFFIX = 'T23:59:59+01:00'

  let toDateFilter = null
  if (filters.toDate) toDateFilter = `${filters.toDate}${MIDNIGHT_SUFFIX}`

  if (filters.fromDate && toDateFilter) {
    dateFilters._between = filters.fromDate + ',' + toDateFilter
  } else if (filters.fromDate) {
    dateFilters._gte = filters.fromDate
  } else if (toDateFilter) {
    dateFilters._lte = toDateFilter
  }

  return dateFilters
}

export function prepareBeneficiaryFilters(filters) {
  const interviewFilters = {}
  const helpRequestFilters = {}

  if (filters.fromDate || filters.toDate) {
    interviewFilters.date_created = helpRequestFilters.date_created =
      prepareDateFilters(filters)
  }

  if (filters.followUpType) {
    interviewFilters.follow_up_types = {
      follow_up_types_id: { _in: filters.followUpType }
    }
    helpRequestFilters.follow_up_type = { _in: filters.followUpType }
  }

  const formattedFilters = { _or: [] }

  if (Object.keys(interviewFilters).length) {
    formattedFilters._or.push({ follow_ups: interviewFilters })
  }
  if (Object.keys(helpRequestFilters).length) {
    formattedFilters._or.push({ help_requests: helpRequestFilters })
  }

  if (filters.beneficiariesCities) {
    formattedFilters.city = { _in: filters.beneficiariesCities }
  }

  if (filters.referent) {
    formattedFilters.referents = { referent: { _in: filters.referent } }
  }

  return formattedFilters
}

export function prepareHelpRequestFilters(filters) {
  const formattedFilters = {}

  if (filters.fromDate || filters.toDate) {
    formattedFilters.date_created = prepareDateFilters(filters)
  }

  if (filters.followUpType) {
    formattedFilters.follow_up_type = { _in: filters.followUpType }
  }

  if (filters.beneficiariesCities) {
    formattedFilters.beneficiary = {
      city: { _in: filters.beneficiariesCities }
    }
  }

  if (filters.referent) {
    formattedFilters.user_created = { _eq: filters.referent }
  }

  return formattedFilters
}

export function prepareInterviewFilters(filters) {
  const formattedFilters = {}

  if (filters.fromDate || filters.toDate) {
    formattedFilters.date_created = prepareDateFilters(filters)
  }

  if (filters.followUpType) {
    formattedFilters.follow_up_types = {
      follow_up_types_id: { _in: filters.followUpType }
    }
  }

  if (filters.beneficiariesCities) {
    formattedFilters.beneficiary = {
      city: { _in: filters.beneficiariesCities }
    }
  }

  if (filters.referent) {
    formattedFilters.user_created = { _eq: filters.referent }
  }

  return formattedFilters
}

export function labelOrUnprovided(value, options) {
  if (!value) return 'Non renseigné'
  return options.get(value)
}
