import type { Beneficiary } from '~/types/beneficiary'
import { formatDate } from '~/utils'

export function beneficiaryName(
  beneficiary: Pick<Beneficiary, 'firstname' | 'usual_name' | 'birth_name'>
): string {
  let lastname = ''
  if (beneficiary.usual_name) lastname = beneficiary.usual_name
  else if (beneficiary.birth_name) lastname = `(${beneficiary.birth_name})`
  return `${nameOrEmpty(beneficiary.firstname)} ${nameOrEmpty(
    lastname.toUpperCase()
  )}`
}

export function nameOrEmpty(name: string): string {
  return name || '(non renseigné)'
}

export function beneficiaryToSearchResult(
  beneficiary: Pick<
    Beneficiary,
    'firstname' | 'usual_name' | 'birth_name' | 'birthdate'
  >
): string {
  const name = beneficiaryName(beneficiary)
  const birthDate = beneficiary.birthdate
    ? formatDate(beneficiary.birthdate)
    : ''
  return birthDate ? `${name} (${birthDate})` : name
}
