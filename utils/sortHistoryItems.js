/**
 * Sort a list of interviews and help requests by date
 * @param a An interview or a help request
 * @param b An interview or a help request
 * @returns A list of interviews and help requests sorted by date
 */
export function sortHistoryItems(a, b) {
  const dateA = a.date || a.opening_date
  const dateB = b.date || b.opening_date
  return dateA >= dateB ? -1 : 1
}
