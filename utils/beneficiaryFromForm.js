import { prepareM2MRelation, checkboxValuesToBoolean } from '~/utils'
import {
  CREATED_RELATIVE_ID_PREFIX,
  RELATIVE_INPUT_PREFIX,
  HOUSEHOLD_RELATIVE_INPUT_PREFIX,
  CREATED_HOUSEHOLD_RELATIVE_ID_PREFIX
} from '~/utils/constants/relative'

const relativePrefixRegex = new RegExp(
  `^(${RELATIVE_INPUT_PREFIX}|${HOUSEHOLD_RELATIVE_INPUT_PREFIX})(.+)(\\d)$`
)

export function beneficiaryFromForm(
  form,
  initialBeneficiary,
  formHouseholdRelatives,
  formRelatives,
  canSetRelatives
) {
  const CHECKBOX_KEYS = [
    'aidant_connect_authorisation',
    'caregiver',
    'no_phone'
  ]
  const beneficiary = checkboxValuesToBoolean(
    Object.fromEntries(new FormData(form)),
    CHECKBOX_KEYS
  )

  beneficiary.usual_name = beneficiary.usual_name.toUpperCase()
  beneficiary.birth_name = beneficiary.birth_name.toUpperCase()

  if (beneficiary.firstname?.length) {
    const [firstnameFirstLetter, ...firstnameRest] = beneficiary.firstname
    beneficiary.firstname = `${firstnameFirstLetter.toUpperCase()}${firstnameRest.join(
      ''
    )}`
  }

  beneficiary.referents = referentsFromFormBeneficiary(
    initialBeneficiary?.referents,
    beneficiary.referents
  )

  if (formHouseholdRelatives.length || formRelatives.length) {
    beneficiary.relatives = relativesFromFormBeneficiary(
      beneficiary,
      formHouseholdRelatives,
      formRelatives
    )
  } else if (canSetRelatives) {
    beneficiary.relatives = []
  }

  if (beneficiary.main_income_type) {
    beneficiary.main_income_type = beneficiary.main_income_type.split(',')
  }
  if (beneficiary.partner_main_income_type) {
    beneficiary.partner_main_income_type =
      beneficiary.partner_main_income_type.split(',')
  }
  if (beneficiary.major_children_main_income_type) {
    beneficiary.major_children_main_income_type =
      beneficiary.major_children_main_income_type.split(',')
  }
  if (beneficiary.pension_organisations) {
    beneficiary.pension_organisations =
      beneficiary.pension_organisations.split(',')
  }

  if (beneficiary.other_pension_organisations) {
    beneficiary.pension_organisations = [
      ...(beneficiary.pension_organisations || []),
      beneficiary.other_pension_organisations
    ]
    delete beneficiary.other_pension_organisations
  }

  // Remove unwanted fields related to relatives
  Object.keys(beneficiary).forEach(key => {
    if (key.match(relativePrefixRegex)) delete beneficiary[key]
  })

  return beneficiary
}

function referentsFromFormBeneficiary(initialReferents, formReferents) {
  return prepareM2MRelation(
    initialReferents || [],
    formReferents.split(','),
    'referent'
  )
}

function relativesFromFormBeneficiary(
  beneficiary,
  formHouseholdRelatives,
  formRelatives
) {
  const relativeIds = [...formHouseholdRelatives, ...formRelatives].map(r => ({
    id: r.id
  }))

  let relatives = Object.entries(beneficiary)
    .reduce((_relatives, [key, value]) => {
      const match = key.match(relativePrefixRegex)

      if (match) {
        const isHousehold = match[1] === HOUSEHOLD_RELATIVE_INPUT_PREFIX
        const field = match[2]
        const indexShift = isHousehold ? 0 : formHouseholdRelatives.length
        const index = parseInt(match[3]) + indexShift
        _relatives[index][field] = value
      }

      return _relatives
    }, relativeIds)
    .filter(relativeHasInformation)

  const CHECKBOX_KEYS = ['hosted', 'caregiver']
  relatives = relatives.map(relative =>
    checkboxValuesToBoolean(relative, CHECKBOX_KEYS)
  )

  relatives.forEach(relative => {
    if (
      relative.id.startsWith(CREATED_HOUSEHOLD_RELATIVE_ID_PREFIX) ||
      relative.id.startsWith(CREATED_RELATIVE_ID_PREFIX)
    ) {
      delete relative.id
    }
  })

  return relatives
}

function relativeHasInformation(relative) {
  return Object.entries(relative).some(
    ([k, v]) => k !== 'id' && k !== 'hosted' && v
  )
}
