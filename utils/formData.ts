/**
 * Get the targeted form element from an event
 */
export const formFromEvent = (event: Event): HTMLFormElement => {
  if (!event.target) {
    throw new Error('Cannot get form from event without target')
  }
  return event.target as HTMLFormElement
}

/**
 * Get the data object from a submit event
 * It does not handle file fields
 */
export const dataObjectFromEvent = <T extends { [k: string]: string }>(
  event: Event
): T => {
  return dataObjectFromForm<T>(formFromEvent(event))
}

/**
 * Get the data object from a form element
 * It does not handle file fields
 */
export const dataObjectFromForm = <T extends { [k: string]: string }>(
  form: HTMLFormElement
): T => {
  return Object.fromEntries(new FormData(form)) as T
}
