export const NotificationType = {
  NewDocument: 'new_document',
  NewComment: 'new_comment',
  DueDate: 'due_date',
  HandlingDate: 'handling_date'
}

export const NOTIFICATION_TYPE_OPTIONS = new Map([
  [NotificationType.NewComment, 'Nouveau commentaire'],
  [NotificationType.NewDocument, 'Nouveau document'],
  [NotificationType.DueDate, 'Échéance'],
  [NotificationType.HandlingDate, 'Fin de prise en charge']
])

export const NOTIFICATION_TYPE_ICONS = new Map([
  [NotificationType.NewComment, 'fr-icon-message-2-line'],
  [NotificationType.NewDocument, 'fr-icon-file-text-line'],
  [NotificationType.DueDate, 'fr-icon-calendar-check-line'],
  [NotificationType.HandlingDate, 'fr-icon-timer-line']
])
