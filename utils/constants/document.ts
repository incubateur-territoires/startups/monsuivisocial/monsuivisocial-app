export enum DocumentType {
  Cerfa = 'cerfa',
  MailHistory = 'mail_history',
  Proof = 'proof',
  Report = 'report'
}

export enum DocumentTheme {
  Emploi = 'emploi',
  SanteHandicap = 'sante_handicap',
  MaintienADomicile = 'maintien_a_domicile',
  BudgetRessources = 'budget_ressources',
  Logement = 'logement',
  Impot = 'impot',
  Justice = 'justice',
  Retraite = 'retraite',
  PrestationsSociales = 'prestations_sociales'
}

export const DOCUMENT_TYPE_OPTIONS = new Map([
  [DocumentType.Cerfa, 'CERFA'],
  [DocumentType.MailHistory, 'Historique courrier'],
  [DocumentType.Proof, 'Justificatifs'],
  [DocumentType.Report, 'Rapports']
])

export const BENEFICIARY_DOCUMENT_FOLDER = 'Beneficiary documents'

export const DOCUMENT_THEME_OPTIONS = new Map([
  [DocumentTheme.Emploi, 'Emploi'],
  [DocumentTheme.SanteHandicap, 'Santé-Handicap'],
  [DocumentTheme.MaintienADomicile, 'Maintien à domicile'],
  [DocumentTheme.BudgetRessources, 'Budget-Ressources'],
  [DocumentTheme.Logement, 'Logement'],
  [DocumentTheme.Impot, 'Impôt'],
  [DocumentTheme.Justice, 'Justice'],
  [DocumentTheme.Retraite, 'Retraite'],
  [DocumentTheme.PrestationsSociales, 'Prestations sociales']
])

export const DOCUMENT_THEME_CLASSES = new Map([
  [DocumentTheme.Emploi, 'fr-badge--purple-glycine'],
  [DocumentTheme.SanteHandicap, 'fr-badge--green-emeraude'],
  [DocumentTheme.MaintienADomicile, 'fr-badge--brown-caramel'],
  [DocumentTheme.BudgetRessources, 'fr-badge--green-bourgeon'],
  [DocumentTheme.Logement, 'fr-badge--brown-cafe-creme'],
  [DocumentTheme.Impot, 'fr-badge--yellow-tournesol'],
  [DocumentTheme.Justice, 'fr-badge--blue-cumulus'],
  [DocumentTheme.Retraite, 'fr-badge--pink-macaron'],
  [DocumentTheme.PrestationsSociales, 'fr-badge--pink-tuile']
])
