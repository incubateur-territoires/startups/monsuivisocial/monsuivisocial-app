export const MINISTRE_OPTIONS = new Map([
  ['braun', 'François Braun (Santé et Prévention)'],
  ['firmin_le_bodo', 'Agnès Firmin-Le Bodo (Santé et Prévention)'],
  [
    'combe',
    'Jean-Christophe Combe (Solidarités, Autonomie et Personnes handicapées)'
  ],
  ['darrieussecq', 'Geneviève Darrieussecq (Personnes handicapées)'],
  ['dussopt', 'Olivier Dussopt (Travail, Plein emploi et Insertion)'],
  ['grandjean', 'Carole Grandjean (Enseignement, Formation professionnels)']
])
