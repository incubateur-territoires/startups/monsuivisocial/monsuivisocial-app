export enum FollowUpTypeType {
  Legal = 'legal',
  Optional = 'optional'
}

export const LEGAL_FOLLOW_UP_TYPE_VALUE = FollowUpTypeType.Legal
export const OPTIONAL_FOLLOW_UP_TYPE_VALUE = FollowUpTypeType.Optional
