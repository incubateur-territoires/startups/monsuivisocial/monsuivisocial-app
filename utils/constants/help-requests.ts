export enum HelpRequestStatus {
  WaitingAdditionalInformation = 'waiting_additional_information',
  InvestigationOngoing = 'investigation_ongoing',
  Accepted = 'accepted',
  Refused = 'refused',
  ClosedByBeneficiary = 'closed_by_beneficiary',
  Dismissed = 'dismissed',
  Ajourne = 'ajourne'
}

export const WAITING_ADDITIONAL_INFORMATION_STATUS_VALUE =
  'waiting_additional_information'
export const ACCEPTED_STATUS_VALUE = 'accepted'
export const REFUSED_STATUS_VALUE = 'refused'
export const CLOSED_BY_BENEFICIARY_STATUS_VALUE = 'closed_by_beneficiary'
export const DISMISSED_STATUS_VALUE = 'dismissed'

export const HELP_REQUEST_STATUS_OPTIONS = new Map([
  [
    HelpRequestStatus.WaitingAdditionalInformation,
    'En attente de justificatifs'
  ],
  [HelpRequestStatus.InvestigationOngoing, "En cours d'instruction"],
  [HelpRequestStatus.Accepted, 'Accepté'],
  [HelpRequestStatus.Refused, 'Refusé'],
  [HelpRequestStatus.Ajourne, 'Ajourné'],
  [HelpRequestStatus.ClosedByBeneficiary, 'Clôturé par le bénéficiaire'],
  [HelpRequestStatus.Dismissed, 'Classé sans suite']
])

export const STATUS_CLASSES = {
  [HelpRequestStatus.Accepted]: 'fr-badge--success',
  [HelpRequestStatus.InvestigationOngoing]: 'fr-badge--orange-terre-battue',
  [HelpRequestStatus.Refused]: 'fr-badge--error',
  [HelpRequestStatus.WaitingAdditionalInformation]: 'fr-badge--info',
  [HelpRequestStatus.ClosedByBeneficiary]: 'fr-badge--error',
  [HelpRequestStatus.Dismissed]: 'fr-badge--error',
  [HelpRequestStatus.Ajourne]: 'fr-badge--orange-terre-battue'
}

export const FINANCIAL_SUPPORT_OPTIONS = new Map([
  ['true', 'Oui'],
  ['false', 'Non']
])

export const EXTERNAL_ORGANISATION_OPTIONS = new Map([
  ['false', 'En interne'],
  ['true', 'Par une organisation extérieure']
])

export const PAYMENT_METHOD_OPTIONS = new Map([
  ['food_coupon', 'Bons alimentaires'],
  ['credit_card', 'Carte bancaire'],
  ['cheque', 'Chèque'],
  ['cash', 'Espèces'],
  ['bank_transfer', 'Virement']
])

export const HELP_REQUEST_REASON_OPTIONS = new Map([
  ['food', 'Alimentation'],
  ['energy', 'Énergie'],
  ['housing', 'Logement'],
  ['other', 'Autre']
])

export const HELP_REQUEST_COLOR = 'var(--color-help-request)'

export const pastStatusesFromStatus = (status: HelpRequestStatus) => {
  switch (status) {
    case HelpRequestStatus.WaitingAdditionalInformation:
      return []
    case HelpRequestStatus.InvestigationOngoing:
      return [HelpRequestStatus.WaitingAdditionalInformation]
    default:
      return [
        HelpRequestStatus.WaitingAdditionalInformation,
        HelpRequestStatus.InvestigationOngoing
      ]
  }
}
