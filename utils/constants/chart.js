import {
  LADY_GENDER_VALUE,
  MAN_GENDER_VALUE,
  OTHER_GENDER_VALUE
} from './beneficiary'
import { AgeGroup } from './age-groups'
import { HelpRequestStatus, HELP_REQUEST_COLOR } from './help-requests'
import {
  DONE_INTERVIEW_STATUS_VALUE,
  ONGOING_INTERVIEW_STATUS_VALUE,
  TO_ADDRESS_INTERVIEW_STATUS_VALUE,
  INTERVIEW_COLOR
} from './interview'

export const GENDER_COLORS = {
  [LADY_GENDER_VALUE]: '#FF8886',
  [MAN_GENDER_VALUE]: '#8D81E4',
  [OTHER_GENDER_VALUE]: '#FFCD56',
  null: '#AAA'
}

export const AGE_GROUPS_COLORS = {
  [AgeGroup.Less25]: '#BABAFF',
  [AgeGroup.Group26_34]: '#A8A8FF',
  [AgeGroup.Group35_44]: '#8585f6',
  [AgeGroup.Group45_54]: '#6a6af4',
  [AgeGroup.Group55_64]: '#313178',
  [AgeGroup.More65]: '#272747',
  null: '#AAA'
}

export const HELP_REQUEST_STATUS_COLORS = {
  [HelpRequestStatus.WaitingAdditionalInformation]: '#fcc63a',
  [HelpRequestStatus.InvestigationOngoing]: '#7AB1E8',
  [HelpRequestStatus.Accepted]: '#3bea7e',
  [HelpRequestStatus.Refused]: '#FF732C',
  [HelpRequestStatus.ClosedByBeneficiary]: '#f95c5e',
  [HelpRequestStatus.Dismissed]: '#CE70CC',
  [HelpRequestStatus.Ajourne]: '#6f45f7'
}

export const INTERVIEW_STATUS_COLORS = {
  [DONE_INTERVIEW_STATUS_VALUE]: '#6f45f7',
  [ONGOING_INTERVIEW_STATUS_VALUE]: '#7AB1E8',
  [TO_ADDRESS_INTERVIEW_STATUS_VALUE]: '#3bea7e'
}

export const COLLECTION_COLORS = {
  help_requests: HELP_REQUEST_COLOR,
  follow_ups: INTERVIEW_COLOR
}
