export const BeneficiaryTab = {
  Details: 'details',
  Documents: 'documents',
  History: 'history'
}

export const HISTORY_TAB = BeneficiaryTab.History
