export const DECEASED_STATUS_VALUE = 'deceased'

export const STATUS_OPTIONS = new Map([
  ['active', 'Actif'],
  ['inactive', 'Inactif'],
  ['archived', 'Archivé'],
  [DECEASED_STATUS_VALUE, 'Décédé·e']
])

export const TITLE_OPTIONS = new Map([
  ['lady', 'Mme.'],
  ['man', 'M.']
])

export const LADY_GENDER_VALUE = 'lady'
export const MAN_GENDER_VALUE = 'man'
export const OTHER_GENDER_VALUE = 'other'

export const GENDER_OPTIONS = new Map([
  [LADY_GENDER_VALUE, 'Femme'],
  [MAN_GENDER_VALUE, 'Homme'],
  [OTHER_GENDER_VALUE, 'Autre']
])

export const PARTNER_FAMILY_SITUATION_VALUES = [
  'concubinage',
  'couple_children',
  'married',
  'pacs'
]

export const FAMILY_SITUATION_OPTIONS = new Map([
  ['single', 'Célibataire'],
  ['divorced', 'Divorcé·e'],
  ['concubinage', 'En concubinage'],
  ['couple_children', 'En couple avec enfant(s)'],
  ['married', 'Marié·e'],
  ['pacs', 'Pacsé·e'],
  ['single_parent', 'Parent isolé·e avec enfant(s)'],
  ['separated', 'Séparé·e'],
  ['widower', 'Veuf·ve'],
  ['other', 'Autre']
])

export const PROTECTION_MEASURE_OPTIONS = new Map([
  ['simple_protection', 'Curatelle simple'],
  ['protection_intensified', 'Curatelle renforcée'],
  ['spouse_authorisation', 'Habilitation du conjoint'],
  ['family_authorisation', 'Habilitation familiale'],
  ['future_protection_mandate', 'Mandat de protection future'],
  ['support_measure', "Mesure d'accompagnement (Masp/Maj/MJAGBF)"],
  ['justice_safeguard', 'Sauvegarde de justice'],
  ['care', 'Tutelle']
])

export const MOBILITY_MODE_OPTIONS = new Map([
  ['no_mobility', 'Aucun moyen de transport à disposition'],
  ['traffic_law', 'Code obtenu'],
  ['public_transport', 'Dépendant des transports en commun'],
  ['licence_vehicle', 'Permis B avec véhicule (voiture, moto, scooter)'],
  ['licence_only', 'Permis B sans véhicule'],
  ['licence_in_progress', 'Permis et/ou code en cours'],
  ['invalid_licence', 'Permis non valide ou suspendu'],
  ['no_licence_vehicle', 'Véhicule sans permis'],
  ['bike', 'Vélo ou trottinette électrique'],
  ['other_licences', 'Autres permis (poids lourds, bus)']
])

export const ORGANISATION_ACCOMMODATION_MODE_VALUES = [
  'collective_accommodation',
  'retirement_home',
  'other_accommodation'
]

export const THIRD_PARTY_ACCOMMODATION_MODE_VALUES = [
  'third_party_hosting',
  'parental_hosting'
]

export const HOSTING_ACCOMMODATION_MODE_VALUES = [
  ...ORGANISATION_ACCOMMODATION_MODE_VALUES,
  ...THIRD_PARTY_ACCOMMODATION_MODE_VALUES
]

export const ACCOMMODATION_MODE_OPTIONS = new Map([
  ['retirement_home', 'EHPAD, résidence senior'],
  ['collective_accommodation', 'Hébergement de type CHRS, CHU, CPH, CADA...'],
  ['parental_hosting', 'Hébergé·e au domicile parental'],
  ['third_party_hosting', 'Hébergé·e chez un tiers'],
  ['private_tenant', 'Locataire parc privé'],
  ['social_tenant', 'Locataire parc social'],
  ['makshift_accommodation', 'Logement de fortune'],
  ['insalubrious_accommodation', 'Logement insalubre'],
  ['landlord', 'Propriétaire'],
  ['no_accommodation', 'Sans hébergement'],
  ['other_accommodation', 'Autre type de logement (hôtel...)']
])

export const ACCOMMODATION_ZONE_OPTIONS = new Map([
  ['france', 'France'],
  ['eu', 'UE'],
  ['outside_eu', 'Hors UE']
])

export const SOCIO_PROFESSIONAL_CATEGORY_OPTIONS = new Map([
  ['sick_leave', 'Arrêt maladie'],
  ['job_seeker', "En recherche d'emploi"],
  ['disability', 'Invalidité'],
  ['housewife', 'Mère au foyer'],
  ['retired', 'Retraité'],
  ['employed', 'Salarié'],
  ['no_activity', 'Sans activité (non inscrit à Pôle Emploi)'],
  ['other', 'Autre']
])

export const INCOME_TYPE_OPTIONS = new Map([
  ['aah', 'AAH'],
  ['apl', 'APL'],
  ['aspa', 'ASPA'],
  ['daily_payments', 'Indemnités journalières'],
  ['unemployment_benefit', 'Indemnités Pôle Emploi : ARE/ASS'],
  ['disability_allowance', "Pension d'invalidité"],
  ['family_benefits', 'Prestations familiales'],
  ['bonus', "Prime d'activité"],
  ['pension', 'Retraite'],
  ['rsa', 'RSA'],
  ['income', 'Salaire'],
  ['child_support', 'Pension alimentaire'],
  ['other', 'Autre']
])

export const ORIENTATION_TYPE_OPTIONS = new Map([
  ['non_profit', 'Orientation Association'],
  ['department', 'Orientation Département'],
  ['representative', 'Orientation Élu'],
  ['third_party_report', "Signalement d'un tiers"],
  ['spontaneous', 'Spontanée'],
  ['staff_care', 'Suivi cabinet'],
  ['other', 'Autre']
])

export enum PensionOrganisation {
  AGIRC_ARRCO = 'agirc-arrco',
  AG2R = 'ag2r',
  CNAV_CARSAT = 'cnav-carsat',
  CIPAV = 'cipav',
  CNRACL = 'cnracl',
  EDF = 'edf',
  IRCANTEC = 'ircantec',
  KLESIA = 'klesia',
  SRE = 'sre',
  SSI = 'ssi',
  MalakoffHumanis = 'malakoff_humanis',
  MSA = 'msa',
  ProBTP = 'pro_btp',
  RetraiteDesMines = 'retraite_des_mines',
  Other = 'other'
}

export const PENSION_ORGANISATION_OPTIONS = new Map([
  [PensionOrganisation.AGIRC_ARRCO, 'AGIRC ARRCO'],
  [PensionOrganisation.AG2R, 'AG2R'],
  [PensionOrganisation.CNAV_CARSAT, 'CNAV/CARSAT'],
  [PensionOrganisation.CIPAV, 'CIPAV'],
  [PensionOrganisation.CNRACL, 'CNRACL'],
  [PensionOrganisation.EDF, 'EDF'],
  [PensionOrganisation.IRCANTEC, 'IRCANTEC'],
  [PensionOrganisation.KLESIA, 'KLESIA'],
  [PensionOrganisation.SRE, 'SRE'],
  [PensionOrganisation.SSI, 'SSI (ex-RSI)'],
  [PensionOrganisation.MalakoffHumanis, 'Malakoff Humanis'],
  [PensionOrganisation.MSA, 'MSA'],
  [PensionOrganisation.ProBTP, 'ProBTP'],
  [PensionOrganisation.RetraiteDesMines, 'Retraite des mines'],
  [PensionOrganisation.Other, 'Autre(s)']
])

export const MINISTERE_CATEGORIE_OPTIONS = new Map([
  ['categorie_a', 'Catégorie A'],
  ['categorie_b', 'Catégorie B'],
  ['categorie_c', 'Catégorie C'],
  ['cadre_droit_prive', 'Cadre droit privé'],
  ['non_cadre_droit_prive', 'Non cadre droit privé']
])

export const MINISTERE_DEPARTEMENT_SERVICE_AC_OPTIONS = new Map([
  ['ain_01', '(01) Ain'],
  ['aisne_02', '(02) Aisne'],
  ['allier_03', '(03) Allier'],
  ['alpe_de_haute_provence_04', '(04) Alpes-de-Haute-Provence'],
  ['hautes_alpes_05', '(05) Hautes-Alpes'],
  ['alpes_maritimes_06', '(06) Alpes-Maritimes'],
  ['ardeche_07', '(07) Ardèche'],
  ['ardennes_08', '(08) Ardennes'],
  ['ariege_09', '(09) Ariège'],
  ['aube_10', '(10) Aube'],
  ['aude_11', '(11) Aude'],
  ['aveyron_12', '(12) Aveyron'],
  ['bouches_du_rhone_13', '(13) Bouches-du-Rhône'],
  ['calvados_14', '(14) Calvados'],
  ['cantal_15', '(15) Cantal'],
  ['charente_16', '(16) Charente'],
  ['charente_maritime_17', '(17) Charente-Maritime'],
  ['cher_18', '(18) Cher'],
  ['correze_19', '(19) Corrèze'],
  ['cote_d_or_21', "(21) Côte-d'Or"],
  ['cotes_d_armor_22', "(22) Côtes-d'Armor"],
  ['creuse_23', '(23) Creuse'],
  ['dordogne_24', '(24) Dordogne'],
  ['doubs_25', '(25) Doubs'],
  ['drome_26', '(26) Drôme'],
  ['eure_27', '(27) Eure'],
  ['eure_et_loir_28', '(28) Eure-et-Loir'],
  ['finistere_29', '(29) Finistère'],
  ['corse_du_sud_2a', '(2A) Corse-du-Sud'],
  ['haute_corse_2b', '(2B) Haute-Corse'],
  ['gard_30', '(30) Gard'],
  ['haute_garonne_31', '(31) Haute-Garonne'],
  ['gers_32', '(32) Gers'],
  ['gironde_33', '(33) Gironde'],
  ['herault_34', '(34) Hérault'],
  ['ille_et_vilaine_35', '(35) Ille-et-Vilaine'],
  ['indre_36', '(36) Indre'],
  ['indre_et_loire_37', '(37) Indre-et-Loire'],
  ['isere_38', '(38) Isère'],
  ['jura_39', '(39) Jura'],
  ['landes_40', '(40) Landes'],
  ['loir_et_cher_41', '(41) Loir-et-Cher'],
  ['loire_42', '(42) Loire'],
  ['haute_loire_43', '(43) Haute-Loire'],
  ['loire_atlantique_44', '(44) Loire-Atlantique'],
  ['loiret_45', '(45) Loiret'],
  ['lot_46', '(46) Lot'],
  ['lot_et_garonne_47', '(47) Lot-et-Garonne'],
  ['lozere_48', '(48) Lozère'],
  ['maine_et_loire_49', '(49) Maine-et-Loire'],
  ['manche_50', '(50) Manche'],
  ['marne_51', '(51) Marne'],
  ['haute_marne_52', '(52) Haute-Marne'],
  ['mayenne_53', '(53) Mayenne'],
  ['meurthe_et_moselle_54', '(54) Meurthe-et-Moselle'],
  ['meuse_55', '(55) Meuse'],
  ['morbihan_56', '(56) Morbihan'],
  ['moselle_57', '(57) Moselle'],
  ['nievre_58', '(58) Nièvre'],
  ['nord_59', '(59) Nord'],
  ['oise_60', '(60) Oise'],
  ['orne_61', '(61) Orne'],
  ['pas_de_calais_62', '(62) Pas-de-Calais'],
  ['puy_de_dome_63', '(63) Puy-de-Dôme'],
  ['pyrenees_atlantiques_64', '(64) Pyrénées-Atlantiques'],
  ['hautes_pyrenees_65', '(65) Hautes-Pyrénées'],
  ['pyrenees_orientales_66', '(66) Pyrénées-Orientales'],
  ['bas_rhin_67', '(67) Bas-Rhin'],
  ['haut_rhin_68', '(68) Haut-Rhin'],
  ['rhone_69', '(69) Rhône'],
  ['haute_saone_70', '(70) Haute-Saône'],
  ['saone_et_loire_71', '(71) Saône-et-Loire'],
  ['sarthe_72', '(72) Sarthe'],
  ['savoie_73', '(73) Savoie'],
  ['haute_savoie_74', '(74) Haute-Savoie'],
  ['paris_75', '(75) Paris'],
  ['seine_maritime_76', '(76) Seine-Maritime'],
  ['seine_et_marne_77', '(77) Seine-et-Marne'],
  ['yvelines_78', '(78) Yvelines'],
  ['deux_sevres_79', '(79) Deux-Sèvres'],
  ['somme_80', '(80) Somme'],
  ['tarn_81', '(81) Tarn'],
  ['tarn_et_garonne_82', '(82) Tarn-et-Garonne'],
  ['var_83', '(83) Var'],
  ['vaucluse_84', '(84) Vaucluse'],
  ['vendee_85', '(85) Vendée'],
  ['vienne_86', '(86) Vienne'],
  ['haute_vienne_87', '(87) Haute-Vienne'],
  ['vosges_88', '(88) Vosges'],
  ['yonne_89', '(89) Yonne'],
  ['territoire_de_belfort_90', '(90) Territoire de Belfort'],
  ['essonne_91', '(91) Essonne'],
  ['hauts_de_seine_92', '(92) Hauts-de-Seine'],
  ['seine_saint_denis_93', '(93) Seine-Saint-Denis'],
  ['val_de_marne_94', '(94) Val-de-Marne'],
  ['val_d_oise_95', "(95) Val-d'Oise"],
  ['guadeloupe_971', '(971) Guadeloupe'],
  ['martinique_972', '(972) Martinique'],
  ['guyane_973', '(973) Guyane'],
  ['la_reunion_974', '(974) La Réunion'],
  ['saint_pierre_et_miquelon_975', '(975) Saint-Pierre-et-Miquelon'],
  ['mayotte_976', '(976) Mayotte'],
  [
    'terres_australes_et_antarctiques_984',
    '(984) Terres Australes et Antarctiques'
  ],
  ['wallis_et_futuna_986', '(986) Wallis et Futuna'],
  ['polynesie_française_987', '(987) Polynésie Française'],
  ['nouvelle_caledonie_988', '(988) Nouvelle-Calédonie'],
  ['agents_en_instance_d_affectation', 'Agents en instance d’affectation'],
  ['chatefp', 'CHATEFP'],
  ['cncp', 'CNCP'],
  ['cnefop', 'CNEFOP'],
  ['cng', 'CNG'],
  ['cnit', 'CNIT'],
  ['cnml', 'CNML'],
  ['comjs', 'COMJS'],
  ['daj', 'DAJ'],
  ['dares', 'DARES'],
  ['dgefp', 'DGEFP'],
  ['dgos', 'DGOS'],
  ['dgp', 'DGP'],
  ['dgs', 'DGS'],
  ['dgt', 'DGT'],
  ['diges', 'DIGES'],
  ['djepva', 'DJEPVA'],
  ['dnum', 'DNUM'],
  ['drh', 'DRH'],
  ['ds', 'DS'],
  ['igas', 'IGAS'],
  ['igjs', 'IGJS'],
  [
    'secretariat_aux_personnes_handicapees',
    'Secrétariat aux personnes handicapées'
  ],
  ['sgmcas', 'SGMCAS']
])

export const MINISTERE_STRUCTURE_OPTIONS = new Map([
  ['administration_centrale', 'Administration centrale'],
  ['ars', 'ARS'],
  ['dreets_deets_outre_mer', 'DREETS/DEETS outre-mer'],
  ['ddets', 'DDETS'],
  ['ddetspp', 'DDETSPP'],
  ['ddpp', 'DDPP'],
  ['inja_injs', 'INJA/INJS'],
  ['ehesp', 'EHESP'],
  ['intefp', 'INTEFP'],
  ['mnc', 'MNC'],
  ['retraites', 'Retraites'],
  ['other', 'Autre']
])
