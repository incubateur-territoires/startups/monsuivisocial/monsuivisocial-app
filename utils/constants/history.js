export const CREATE_ACTION = 'create'
export const UPDATE_ACTION = 'update'

export const ACTION_LABELS = new Map([
  [CREATE_ACTION, 'Création'],
  [UPDATE_ACTION, 'Modification']
])

export const INTERVIEWS_COLLECTION = 'follow_ups'
export const HELP_REQUESTS_COLLECTION = 'help_requests'

export const TYPE_SUIVI_OPTIONS = new Map([
  [INTERVIEWS_COLLECTION, "Synthèse d'entretien"],
  [HELP_REQUESTS_COLLECTION, "Demande d'aide"]
])

export const INTERVIEWS_TAB_ID = 'interviews'
export const HELP_REQUESTS_TAB_ID = 'help-requests'
