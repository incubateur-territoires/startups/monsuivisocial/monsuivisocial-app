export const HOUSEHOLD_RELATIVE_TYPE_OPTIONS = new Map([
  ['spouse', 'Conjoint·e'],
  ['major_child', 'Enfant majeur'],
  ['minor_child', 'Enfant mineur']
])

export const RELATIVE_TYPE_OPTIONS = new Map([
  ...Array.from(HOUSEHOLD_RELATIVE_TYPE_OPTIONS.entries()),
  ['Sibling', 'Fratrie'],
  ['grand_parent', 'Grand-parent'],
  ['parent', 'Parent'],
  ['third_party', 'Tiers'],
  ['neighbour', 'Voisin·e'],
  ['other_family_member', 'Autre membre de la famille']
])

export const CREATED_RELATIVE_ID_PREFIX = 'relative-'
export const CREATED_HOUSEHOLD_RELATIVE_ID_PREFIX = 'household-relative-'
export const RELATIVE_INPUT_PREFIX = 'relative_'
export const HOUSEHOLD_RELATIVE_INPUT_PREFIX = 'household_relative_'
