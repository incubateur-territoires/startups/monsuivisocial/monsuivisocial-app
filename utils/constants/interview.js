export const OUTSIDE_INTERVIEW_TYPE_VALUE = 'outside'
export const WITH_THIRD_PERSON_INTERVIEW_TYPE_VALUE =
  'interview_with_third_person'

export const INTERVIEW_TYPE_OPTIONS = new Map([
  ['physical_spontaneous', 'Accueil physique spontané'],
  ['physical_meeting', 'Accueil physique sur rendez-vous'],
  ['mail', 'Courrier'],
  [WITH_THIRD_PERSON_INTERVIEW_TYPE_VALUE, 'Entretien avec un tiers'],
  ['phone', 'Entretien téléphonique'],
  ['email', 'E-mail'],
  ['home', 'Rendez-vous à domicile'],
  [OUTSIDE_INTERVIEW_TYPE_VALUE, 'Rendez-vous extérieur'],
  ['videoconference', 'Visioconférence']
])

export const DONE_INTERVIEW_STATUS_VALUE = 'done'
export const ONGOING_INTERVIEW_STATUS_VALUE = 'ongoing'
export const TO_ADDRESS_INTERVIEW_STATUS_VALUE = 'to_address'

export const INTERVIEW_STATUS_OPTIONS = new Map([
  [TO_ADDRESS_INTERVIEW_STATUS_VALUE, 'À traiter'],
  [ONGOING_INTERVIEW_STATUS_VALUE, 'En cours'],
  [DONE_INTERVIEW_STATUS_VALUE, 'Terminé']
])

export const STATUS_CLASSES = {
  [DONE_INTERVIEW_STATUS_VALUE]: 'fr-badge--success',
  [ONGOING_INTERVIEW_STATUS_VALUE]: 'fr-badge--orange-terre-battue',
  [TO_ADDRESS_INTERVIEW_STATUS_VALUE]: 'fr-badge--yellow-tournesol'
}

export const INTERVIEW_INTERVENTION_OPTIONS = new Map([
  ['deets_siao', 'DEETS-SIAO'],
  ['prefecture', 'Préfecture'],
  ['baileurs', 'Bailleurs'],
  ['action_logement', 'Action Logement'],
  ['secours_medecin_traitant', 'Secours/Médecin traitant']
])

export const INTERVIEW_SIGNALEMENT_OPTIONS = new Map([
  ['chef_de_cabinet', 'Chef de cabinet'],
  ['prefet', 'Préfet'],
  ['organisme_menace', 'Organisme menacé']
])

export const INTERVIEW_COLOR = 'var(--color-interview)'

export const INTERVIEWS_TABLE_FIELDS = [
  'id',
  'status',
  'type',
  'due_date',
  'follow_up_types.follow_up_types_id.id',
  'follow_up_types.follow_up_types_id.name',
  'beneficiary.id',
  'beneficiary.firstname',
  'beneficiary.usual_name',
  'beneficiary.birth_name',
  'user_created.id',
  'user_created.first_name',
  'user_created.last_name'
]
