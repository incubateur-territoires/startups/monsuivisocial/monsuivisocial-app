export const ROLES = {
  responsableDeStructure: 'responsable-de-structure',
  instructeur: 'instructeur',
  referent: 'referent',
  agentDAccueil: 'agent-d-accueil',
  travailleurSocial: 'travailleur-social'
}
