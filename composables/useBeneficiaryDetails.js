import {
  PensionOrganisation,
  PENSION_ORGANISATION_OPTIONS,
  INCOME_TYPE_OPTIONS
} from '~/utils/constants/beneficiary'
import { FollowUpTypeType } from '~/utils/constants/follow-up-type'
import { nameOrEmpty } from '~/utils/beneficiary'
import { useAuthStore } from '~/store/auth'

export function useBeneficiaryDetails(beneficiary) {
  const { $getBeneficiaryFollowUpTypes, $pinia } = useNuxtApp()
  const isMinistryAgent = useMinistryAgent()
  const authStore = useAuthStore($pinia)

  const usualName = computed(() =>
    nameOrEmpty((beneficiary.value?.usual_name || '').toUpperCase())
  )

  const minorChildrenLabel = computed(() =>
    beneficiary.value?.minor_children > 1 ? `enfants mineurs` : `enfant mineur`
  )

  const majorChildrenLabel = computed(() =>
    beneficiary.value?.major_children > 1 ? `enfants majeurs` : `enfant majeur`
  )

  const hasAddress = computed(
    () =>
      beneficiary.value?.street ||
      beneficiary.value?.zip_code ||
      beneficiary.value?.city ||
      beneficiary.value?.region
  )

  const phone = computed(() =>
    beneficiary.value?.phone_2
      ? `${beneficiary.value?.phone_1} / ${beneficiary.value?.phone_2}`
      : beneficiary.value?.phone_1
  )

  const hasHealthSection = computed(
    () =>
      beneficiary.value?.gir ||
      beneficiary.value?.doctor ||
      beneficiary.value?.health_additional_information ||
      (hasNirAccess.value && beneficiary.value?.social_security_number) ||
      beneficiary.value?.insurance
  )

  const pensionOrganisations = computed(() =>
    arrayToString(
      beneficiary.value?.pension_organisations?.filter(
        f => f !== PensionOrganisation.Other
      ),
      PENSION_ORGANISATION_OPTIONS
    )
  )

  const hasOccupationIncomeSection = computed(
    () =>
      beneficiary.value?.socio_professional_category ||
      beneficiary.value?.occupation ||
      beneficiary.value?.employer ||
      beneficiary.value?.employer_siret ||
      mainIncomeType.value ||
      beneficiary.value?.main_income_amount ||
      partnerMainIncomeType.value ||
      beneficiary.value?.partner_main_income_amount ||
      majorChildrenMainIncomeType.value ||
      beneficiary.value?.major_children_main_income_amount ||
      beneficiary.value?.pole_emploi ||
      pensionOrganisations.value ||
      beneficiary.value?.caf ||
      beneficiary.value?.bank ||
      beneficiary.value?.funeral_contract ||
      (isMinistryAgent.value &&
        (beneficiary.value?.ministere_categorie ||
          beneficiary.value?.ministere_departement_service_ac ||
          beneficiary.value?.ministere_structure))
  )

  const hasOtherInformationSection = computed(
    () =>
      beneficiary.value?.protection_measure ||
      beneficiary.value?.representative ||
      beneficiary.value?.prescribing_organisation ||
      beneficiary.value?.orientation_type ||
      beneficiary.value?.orientation_organisation ||
      beneficiary.value?.service_providers ||
      beneficiary.value?.involved_partners ||
      beneficiary.value?.additional_information
  )

  const followUpTypes = computed(() =>
    $getBeneficiaryFollowUpTypes(beneficiary.value)
  )

  const legalFollowUpTypes = computed(() =>
    followUpTypes.value.filter(f => f.type === FollowUpTypeType.Legal)
  )

  const optionalFollowUpTypes = computed(() =>
    followUpTypes.value.filter(f => f.type === FollowUpTypeType.Optional)
  )

  const mainIncomeType = computed(() =>
    arrayToString(beneficiary.value?.main_income_type, INCOME_TYPE_OPTIONS)
  )

  const partnerMainIncomeType = computed(() =>
    arrayToString(
      beneficiary.value?.partner_main_income_type,
      INCOME_TYPE_OPTIONS
    )
  )

  const majorChildrenMainIncomeType = computed(() =>
    arrayToString(
      beneficiary.value?.major_children_main_income_type,
      INCOME_TYPE_OPTIONS
    )
  )

  const hasNirAccess = computed(
    () => !isMinistryAgent.value && !authStore.isAssociationAgent
  )

  function arrayToString(arr, options) {
    return (arr || []).map(o => options.get(o) || o).join(', ')
  }

  return {
    usualName,
    minorChildrenLabel,
    majorChildrenLabel,
    hasAddress,
    phone,
    hasHealthSection,
    hasOccupationIncomeSection,
    hasOtherInformationSection,
    legalFollowUpTypes,
    optionalFollowUpTypes,
    mainIncomeType,
    partnerMainIncomeType,
    majorChildrenMainIncomeType,
    pensionOrganisations,
    hasNirAccess
  }
}
