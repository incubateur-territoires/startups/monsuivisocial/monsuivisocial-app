import { Routes } from '~/utils/routing/routes'

export function useInclusionConnect() {
  const route = useRoute()
  const config = useRuntimeConfig()

  const redirectUrl = computed(() => {
    return typeof route.query.redirectUrl === 'string'
      ? route.query.redirectUrl
      : Routes.Overview
  })

  const inclusionConnectRedirectUrl = computed(() => {
    const params = new URLSearchParams({
      continue: '',
      redirectUrl: redirectUrl.value
    })
    return `${config.public.appUrl}${Routes.SsoCallback}?${params}`
  })

  const inclusionConnectLoginUrl = computed(() => {
    const params = new URLSearchParams({
      response_type: 'code',
      client_id: config.public.inclusionConnectClientId,
      redirect: inclusionConnectRedirectUrl.value
    })

    return `${config.public.apiUrl}/auth/login/keycloak?${params}`
  })

  const inclusionConnectRegistrationUrl = computed(() => {
    const params = new URLSearchParams({
      client_id: config.public.inclusionConnectClientId,
      scope: 'openid profile email',
      response_type: 'code',
      redirect_uri: inclusionConnectRedirectUrl.value
    })

    return `${config.public.inclusionConnectUrl}protocol/openid-connect/registrations?${params}`
  })

  return {
    redirectUrl,
    inclusionConnectLoginUrl,
    inclusionConnectRegistrationUrl
  }
}
