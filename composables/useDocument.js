import { useAuthStore } from '~/store/auth'

export function useDocument() {
  const config = useRuntimeConfig()
  const { $pinia } = useNuxtApp()
  const authStore = useAuthStore($pinia)

  function documentLink(documentId) {
    return `${config.public.apiUrl}/assets/${documentId}?access_token=${authStore.accessToken}`
  }

  return { documentLink }
}
