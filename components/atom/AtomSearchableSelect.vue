<template>
  <div
    class="fr-select-group searchable-select"
    :class="{
      'fr-select-group--error': error,
      'fr-select-group--disabled': readonly,
      'multiple': multiple
    }"
  >
    <label
      class="fr-label"
      :for="displayInputId"
    >
      {{ displayLabel }}
      <span
        v-if="hint"
        class="fr-hint-text"
      >
        <slot name="hint"></slot>
      </span>
    </label>
    <input
      :id="displayInputId"
      ref="combobox"
      v-model="displayedValue"
      class="fr-select"
      :class="{ 'fr-select--error': error }"
      autocomplete="off"
      aria-autocomplete="list"
      aria-haspopup="listbox"
      aria-expanded="false"
      :aria-controls="listboxId"
      :aria-activedescendant="activeOptionValue"
      :aria-describedby="ariaDescribedby"
      role="combobox"
      :disabled="readonly"
      @input="onSearch"
      @focus="onInputFocus"
      @keydown="onInputKeyDown"
      @blur="onInputBlur"
    />
    <!-- FIXME: As input is hidden, it can not be focused on submission failure -->
    <input
      :id="name"
      ref="hiddenInput"
      :name="name"
      hidden
      :required="required"
      :value="currentValue"
      :readonly="readonly"
      @change="onValidate"
      @invalid="onValidate"
    />

    <ul
      v-show="expanded"
      :id="listboxId"
      ref="listbox"
      class="menu"
      :class="{ 'fr-tags-group': multiple }"
      role="listbox"
      :aria-multiselectable="multiple"
    >
      <li
        v-for="([optionValue, optionLabel], index) in orderedFilteredOptions"
        :ref="el => setOptionRef(el, optionValue)"
        :key="optionValue"
        :class="{
          'menu-item': !multiple,
          'active-menu-item': !multiple && index === activeIndex
        }"
        role="option"
        :aria-selected="selectedValues.includes(optionValue)"
        @mousedown="onOptionMouseDown"
        @click="onToggleOption(optionValue, index)"
      >
        <template v-if="!multiple">
          {{ optionLabel }}
        </template>
        <template v-else>
          <button
            :id="optionValue"
            type="button"
            class="fr-tag"
            :class="{ 'active-menu-item': index === activeIndex }"
            :name="optionValue"
            :aria-pressed="selectedValues.includes(optionValue)"
            tabindex="-1"
          >
            {{ optionLabel }}
          </button>
        </template>
      </li>
      <li
        v-if="createTagIsDisplayed"
        :ref="optionRefs[NEW_OPTION_ID]"
        :class="{
          'menu-item': !multiple,
          'active-menu-item': !multiple && createTagIsActive,
          'fr-icon-add-circle-line': !multiple
        }"
        role="option"
        @mousedown="onOptionMouseDown"
        @click="onCreateOption"
      >
        <template v-if="!multiple">
          {{ search }}
        </template>
        <template v-else>
          <button
            type="button"
            class="fr-tag fr-icon-add-circle-line fr-tag--icon-left"
            :class="{ 'active-menu-item': createTagIsActive }"
            tabindex="-1"
          >
            {{ search }}
          </button>
        </template>
      </li>
    </ul>
    <div
      v-if="error"
      :id="errorId"
      class="fr-messages-group"
      aria-live="assertive"
    >
      <p class="fr-message fr-message--error">
        {{ error }}
      </p>
    </div>
  </div>
</template>

<script setup>
  import { NEW_OPTION_PREFIX, NEW_OPTION_ID } from '~/utils/constants'

  const props = defineProps({
    label: { type: String, required: true },
    name: { type: String, required: true },

    /**
     * The option list in which the results are looked for
     */
    options: { type: Map, required: true },

    /**
     * The expected value is a string if the multiple prop is not set, otherwise, it should be string list
     */
    value: { type: [String, Array], default: undefined },
    required: { type: Boolean, default: false },
    hint: { type: Boolean, default: false },

    /**
     * Whether the select uses muti-selection
     */
    multiple: { type: Boolean, default: false },

    canCreateNewOption: { type: Boolean, default: false },

    readonly: { type: Boolean, default: false }
  })

  const initialValue = computed(() => {
    if (props.value) {
      if (Array.isArray(props.value)) return props.value
      return [props.value]
    }
    return []
  })

  const existingOptions = ref(props.options)
  const filteredOptions = ref(existingOptions.value)
  const displayedValue = ref('')
  const combobox = ref(null)
  const selectedValues = ref(initialValue.value)
  const hiddenInput = ref(null)

  const valueIsOption = ref(false)
  const error = ref('')
  const preventMenuReopen = ref(false)

  const optionRefs = initOptionRefs()

  const expanded = computed(
    () =>
      isFocused.value &&
      (!!filteredOptions.value.size || props.canCreateNewOption)
  )

  const listboxId = computed(() => `searchable-select-${props.name}-listbox`)

  const displayInputId = computed(
    () => `searchable-select-${props.name}-display-input`
  )

  const displayLabel = computed(() =>
    props.required ? `${props.label} *` : props.label
  )

  const errorId = computed(() => `${props.name}-error-desc-error`)

  const ariaDescribedby = computed(() =>
    error.value ? errorId.value : undefined
  )

  /**
   * The value stored in the hidden input
   */
  const currentValue = computed(() => selectedValues.value.join(','))

  const selectedValuesLabels = computed(() =>
    selectedValues.value
      .map(value => existingOptions.value.get(value))
      .join(', ')
  )

  const orderedFilteredOptions = computed(() => {
    if (!props.multiple) return filteredOptions.value
    const optionsAsArray = [...filteredOptions.value.entries()]
    const sortedOptionsAsArray = optionsAsArray.sort(([aKey], [bKey]) => {
      const aSelected = selectedValues.value.includes(aKey)
      const bSelected = selectedValues.value.includes(bKey)
      if (aSelected === bSelected) return 0
      return aSelected ? -1 : 1
    })
    return new Map(sortedOptionsAsArray)
  })

  const search = computed(() =>
    displayedValue.value.replace(selectedValuesLabels.value + ', ', '').trim()
  )

  const searchAlreadyExists = computed(() => {
    return [...orderedFilteredOptions.value.values()].includes(search.value)
  })

  const createTagIsDisplayed = computed(
    () => props.canCreateNewOption && search.value && !searchAlreadyExists.value
  )

  const optionValues = computed(() => {
    const _optionValues = [...orderedFilteredOptions.value.keys()]
    if (createTagIsDisplayed.value) _optionValues.push(NEW_OPTION_ID)
    return _optionValues
  })

  const {
    /**
     * For dropdown mode, indicates whether the listbox is expanded or not
     */
    expanded: isFocused,

    /**
     * Prevent from closing the listbox on blur
     * It is used when an option is clicked
     */
    ignoreBlur,

    /**
     * The active option index in the options list
     */
    activeIndex,
    activeOptionValue,
    listbox,

    /**
     * Flag to set focus on next render completion, when menu was blurred on click on option
     */
    callFocus
  } = useExpandMenu(
    combobox,
    optionValues,
    optionRefs,
    computed(() => props.readonly)
  )

  useWatchSelectOptions(() => props.options, selectedValues)

  watch(
    () => props.options,
    _options => {
      existingOptions.value = _options
      filteredOptions.value = existingOptions.value
      setDisplayedValueToSelected()
    }
  )

  const createTagIsActive = computed(
    () =>
      props.canCreateNewOption &&
      search.value &&
      activeIndex.value === orderedFilteredOptions.value.size
  )

  /**
   * Update the hidden input value
   */
  watch(currentValue, _currentValue => {
    if (!hiddenInput.value) return
    hiddenInput.value.value = _currentValue
    hiddenInput.value.dispatchEvent(new Event('change'))
    hiddenInput.value.closest('form')?.dispatchEvent(new Event('change'))
    setDisplayedValueToSelected()
  })

  watch(expanded, _expanded => {
    // As Vue2 removes attribute when set to a falsy value, it can not directly binded in the template
    combobox.value?.setAttribute('aria-expanded', _expanded.toString())
  })

  onMounted(() => {
    listenFormReset()
    setDisplayedValueToSelected()
  })

  onBeforeUnmount(() => {
    unlistenFormReset()
  })

  function listenFormReset() {
    hiddenInput.value?.closest('form')?.addEventListener('reset', reset)
  }

  function unlistenFormReset() {
    hiddenInput.value?.closest('form')?.removeEventListener('reset', reset)
  }

  function onSearch(event) {
    valueIsOption.value = false
    const input = event.target

    // When the input is empty, selected values are deselected
    if (!input.value) reset()

    if (!search.value) {
      filteredOptions.value = existingOptions.value
      return
    }

    const filteredOptionsArray = [...existingOptions.value.entries()].filter(
      ([_, label]) =>
        label.toLocaleLowerCase().includes(search.value.toLocaleLowerCase())
    )
    filteredOptions.value = new Map(filteredOptionsArray)
  }

  function onOptionMouseDown() {
    ignoreBlur.value = true
    callFocus.value = true
  }

  function onInputKeyDown(e) {
    if (!expanded.value) {
      if (e.key !== 'Escape') isFocused.value = true
      if (props.multiple && e.key === ' ') {
        e.preventDefault()
        isFocused.value = true
      }
      return
    }

    // Keyboard interactions on the expanded listbox
    if (e.key === 'Enter') {
      if (props.multiple) {
        onToggleOption(activeOptionValue.value)
      } else onSelectActiveOption()
    }
  }

  function onSelectActiveOption() {
    if (!activeOptionValue.value) return

    if (activeOptionValue.value === NEW_OPTION_ID) return onCreateOption()

    if (!props.multiple) selectedValues.value = [activeOptionValue.value]
    else {
      if (selectedValues.value.includes(activeOptionValue.value)) return
      selectedValues.value.push(activeOptionValue.value)
    }
  }

  /**
   * If the searchable select is not in multiple selection mode, set an option value as the selected value
   * Else add a value to the selected values if it is not present, otherwise remove it from
   * @param value The option value to select or toggle
   * @param optionIndex Optional - The option index to set as active when selecting from mouse click
   */
  function onToggleOption(value, optionIndex) {
    if (optionIndex) activeIndex.value = optionIndex

    if (!props.multiple) {
      selectedValues.value = [value]

      /**
       * When toggling an option in a single-option searchable select by clicking on it,
       * we want the menu to close. As the mouse down event on an option has been triggered,
       * the searchable select will force open state using ignoreBlur and callFocus
       * variables. At this stage in the code, ignoreBlur has already been consumed
       * by the callback of the blur event. The focus back to the input has to prevent
       * openning the menu again.
       */
      forceMenuClose()
    } else if (selectedValues.value.includes(value)) {
      selectedValues.value = selectedValues.value.filter(o => o !== value)
    } else selectedValues.value.push(value)
  }

  function setDisplayedValueToSelected() {
    displayedValue.value = selectedValuesLabels.value

    // Add trailing comma and space for a better user experience
    if (props.multiple && displayedValue.value.length) {
      displayedValue.value += ', '
    }

    valueIsOption.value = true
    filteredOptions.value = existingOptions.value
  }

  function onInputBlur() {
    /**
     * Reinitialize the displayed value when the input is blurred, only if it is
     * not blurred while clicking an option to select it
     */
    if (!ignoreBlur.value && !valueIsOption.value) setDisplayedValueToSelected()
  }

  function onValidate(event) {
    if (!event.target) return
    error.value = event.target.validationMessage
  }

  function onInputFocus() {
    if (!props.multiple) {
      if (preventMenuReopen.value) preventMenuReopen.value = false
      else isFocused.value = true
    }
  }

  function forceMenuClose() {
    isFocused.value = false
    preventMenuReopen.value = true
  }

  function reset() {
    selectedValues.value = []
  }

  function onCreateOption() {
    const newOptionValue = `${NEW_OPTION_PREFIX}${search.value}`.replace(
      ' ',
      '_'
    )
    existingOptions.value.set(newOptionValue, search.value)
    optionRefs[newOptionValue] = ref(null)
    if (props.multiple) selectedValues.value.push(newOptionValue)
    else selectedValues.value = [newOptionValue]
  }

  function initOptionRefs() {
    const optionValues = [...existingOptions.value.keys()]
    const existingOptionsRefs = optionValues.reduce(
      (optionRefs, optionValue) => ({
        ...optionRefs,
        [optionValue]: ref(null)
      }),
      {}
    )
    if (!props.canCreateNewOption) return existingOptionsRefs
    return { ...existingOptionsRefs, [NEW_OPTION_ID]: ref(null) }
  }

  function setOptionRef(el, optionValue) {
    if (optionRefs[optionValue]) {
      optionRefs[optionValue].value = el
    } else optionRefs[optionValue] = ref(el)
  }
</script>

<style>
  .searchable-select.multiple .menu {
    padding: 1rem;
  }
</style>
