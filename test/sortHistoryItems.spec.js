import { sortHistoryItems } from '~/utils/sortHistoryItems'

describe('sortHistoryItems', () => {
  it('sort an history made of interviews and help requests by date', () => {
    expect(
      [
        { date: '2022-09-10' },
        { date: '2022-09-12' },
        { date: '2022-09-08' },
        { opening_date: '2022-08-20' },
        { date: '2022-09-01' },
        { opening_date: '2022-09-12' },
        { date: '2022-09-03' },
        { opening_date: '2022-08-28' },
        { date: '2022-09-14' },
        { date: '2022-09-15' }
      ].sort(sortHistoryItems)
    ).toEqual([
      { date: '2022-09-15' },
      { date: '2022-09-14' },
      { opening_date: '2022-09-12' },
      { date: '2022-09-12' },
      { date: '2022-09-10' },
      { date: '2022-09-08' },
      { date: '2022-09-03' },
      { date: '2022-09-01' },
      { opening_date: '2022-08-28' },
      { opening_date: '2022-08-20' }
    ])
  })
})
