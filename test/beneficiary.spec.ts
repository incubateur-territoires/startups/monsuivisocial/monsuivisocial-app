import { beneficiaryName, beneficiaryToSearchResult } from '~/utils/beneficiary'

describe('Beneficiary utils', () => {
  describe('beneficiaryName', () => {
    it('displays firstname and usual name', () => {
      expect(
        beneficiaryName({
          firstname: 'John',
          usual_name: 'Doe'
        })
      ).toBe('John DOE')
    })

    it('displays firstname and birth name', () => {
      expect(
        beneficiaryName({
          firstname: 'John',
          usual_name: '',
          birth_name: 'Doe'
        })
      ).toBe('John (DOE)')
    })

    it('displays firstname and usual name even if the beneficiary has a birth name', () => {
      expect(
        beneficiaryName({
          firstname: 'John',
          usual_name: 'Doe',
          birth_name: 'Baz'
        })
      ).toBe('John DOE')
    })

    it('displays unprovided names', () => {
      expect(
        beneficiaryName({
          firstname: 'John',
          usual_name: ''
        })
      ).toBe('John (non renseigné)')
      expect(
        beneficiaryName({
          firstname: '',
          usual_name: 'Doe'
        })
      ).toBe('(non renseigné) DOE')
      expect(
        beneficiaryName({
          firstname: '',
          usual_name: ''
        })
      ).toBe('(non renseigné) (non renseigné)')
    })
  })

  describe('beneficiaryToSearchResult', () => {
    it('displays the beneficiary name with the birth date if it has one', () => {
      expect(
        beneficiaryToSearchResult({
          firstname: 'John',
          usual_name: 'Doe',
          birthdate: '01-01-1970'
        })
      ).toBe('John DOE (01/01/1970)')
    })

    it('displays the beneficiary name without the birth date if it does not have any', () => {
      expect(
        beneficiaryToSearchResult({
          firstname: 'John',
          usual_name: 'Doe'
        })
      ).toBe('John DOE')
    })
  })
})
