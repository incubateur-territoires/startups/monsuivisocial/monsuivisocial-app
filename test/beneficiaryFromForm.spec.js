import { beneficiaryFromForm } from '~/utils/beneficiaryFromForm'

function createInputForForm(name, value) {
  const el = document.createElement('input')
  el.name = name
  el.value = value
  return el
}

describe('beneficiaryFromForm', () => {
  it('prepares beneficiary from the form', () => {
    const form = document.createElement('form')
    form.appendChild(createInputForForm('referents', 'agent-id-1,agent-id-2'))
    form.appendChild(createInputForForm('status', 'active'))
    form.appendChild(createInputForForm('title', 'lady'))
    form.appendChild(createInputForForm('usual_name', 'bénéfiCiaire'))
    form.appendChild(createInputForForm('birth_name', 'De son Nom'))
    form.appendChild(createInputForForm('firstname', 'jEan'))
    form.appendChild(createInputForForm('birthdate', '01-01-1980'))
    form.appendChild(createInputForForm('birth_place', 'Ville'))
    form.appendChild(createInputForForm('gender', 'other'))
    form.appendChild(createInputForForm('nationality', ''))
    form.appendChild(createInputForForm('accommodation_mode', ''))
    form.appendChild(createInputForForm('accommodation_name', ''))
    form.appendChild(
      createInputForForm('accommodation_additional_information', '')
    )
    form.appendChild(createInputForForm('street_number', '10'))
    form.appendChild(createInputForForm('street', 'rue du Test'))
    form.appendChild(createInputForForm('address_complement', 'Étage 2'))
    form.appendChild(createInputForForm('zip_code', '00000'))
    form.appendChild(createInputForForm('city', 'Ville'))
    form.appendChild(createInputForForm('no_phone', ''))
    form.appendChild(createInputForForm('phone_1', '0600000000'))
    form.appendChild(createInputForForm('phone_2', ''))
    form.appendChild(createInputForForm('email', 'test@example.com'))
    form.appendChild(createInputForForm('family_situation', 'single'))
    form.appendChild(createInputForForm('caregiver', 'on'))
    form.appendChild(createInputForForm('minor_children', '1'))
    form.appendChild(createInputForForm('major_children', ''))
    form.appendChild(createInputForForm('mobility', ''))
    form.appendChild(createInputForForm('relative_lastname0', 'Test'))
    form.appendChild(createInputForForm('relative_firstname0', 'Entourage'))
    form.appendChild(createInputForForm('relative_type0', 'grand_parent'))
    form.appendChild(createInputForForm('relative_city0', 'Ville'))
    form.appendChild(createInputForForm('relative_email0', ''))
    form.appendChild(createInputForForm('relative_phone0', '0400000000'))
    form.appendChild(createInputForForm('relative_hosted0', 'on'))
    form.appendChild(createInputForForm('relative_additional_information0', ''))
    form.appendChild(createInputForForm('relative_lastname1', ''))
    form.appendChild(createInputForForm('relative_firstname1', ''))
    form.appendChild(createInputForForm('relative_type1', ''))
    form.appendChild(createInputForForm('relative_city1', ''))
    form.appendChild(createInputForForm('relative_email1', ''))
    form.appendChild(createInputForForm('relative_phone1', ''))
    form.appendChild(createInputForForm('relative_additional_information1', ''))
    form.appendChild(createInputForForm('relative_lastname2', 'Test2'))
    form.appendChild(createInputForForm('relative_firstname2', 'Entourage'))
    form.appendChild(createInputForForm('relative_type2', 'spouse'))
    form.appendChild(createInputForForm('relative_city2', ''))
    form.appendChild(createInputForForm('relative_email2', ''))
    form.appendChild(createInputForForm('relative_phone2', '0600000000'))
    form.appendChild(
      createInputForForm(
        'relative_additional_information2',
        'Additional information'
      )
    )
    form.appendChild(createInputForForm('gir', 'gir_2'))
    form.appendChild(createInputForForm('doctor', ''))
    form.appendChild(
      createInputForForm('health_additional_information', 'Asthme')
    )
    form.appendChild(createInputForForm('social_security_number', ''))
    form.appendChild(createInputForForm('insurance', ''))
    form.appendChild(createInputForForm('occupation', ''))
    form.appendChild(createInputForForm('main_income_type', ''))
    form.appendChild(createInputForForm('main_income_amount', ''))
    form.appendChild(createInputForForm('main_income_type_partner', ''))
    form.appendChild(createInputForForm('main_income_amount_partner', '24000'))
    form.appendChild(createInputForForm('pole_emploi', '00000'))
    form.appendChild(createInputForForm('pension_organisation', ''))
    form.appendChild(createInputForForm('caf', ''))
    form.appendChild(createInputForForm('bank', 'Banque'))
    form.appendChild(
      createInputForForm('protection_measure', 'simple_protection')
    )
    form.appendChild(createInputForForm('representative', ''))
    form.appendChild(createInputForForm('prescribing_organisation', ''))
    form.appendChild(createInputForForm('orientation_organisation', ''))
    form.appendChild(createInputForForm('service_providers', ''))
    form.appendChild(createInputForForm('involved_partners', ''))
    form.appendChild(createInputForForm('additional_information', ''))

    expect(
      beneficiaryFromForm(
        form,
        undefined,
        [],
        [
          { id: '0c66f94a-29f3-4ee6-b7b7-804c4c464019' },
          { id: '3d7f77e3-b52a-43ba-b6cf-f6deed5e5b0c' },
          { id: 'relative-created1' }
        ]
      )
    ).toEqual({
      referents: [{ referent: 'agent-id-1' }, { referent: 'agent-id-2' }],
      aidant_connect_authorisation: false,
      status: 'active',
      title: 'lady',
      usual_name: 'BÉNÉFICIAIRE',
      birth_name: 'DE SON NOM',
      firstname: 'JEan',
      birthdate: '01-01-1980',
      birth_place: 'Ville',
      gender: 'other',
      nationality: '',
      accommodation_mode: '',
      accommodation_name: '',
      accommodation_additional_information: '',
      street_number: '10',
      street: 'rue du Test',
      address_complement: 'Étage 2',
      zip_code: '00000',
      city: 'Ville',
      no_phone: false,
      phone_1: '0600000000',
      phone_2: '',
      email: 'test@example.com',
      family_situation: 'single',
      caregiver: true,
      minor_children: '1',
      major_children: '',
      mobility: '',
      relatives: [
        {
          id: '0c66f94a-29f3-4ee6-b7b7-804c4c464019',
          lastname: 'Test',
          firstname: 'Entourage',
          type: 'grand_parent',
          city: 'Ville',
          phone: '0400000000',
          email: '',
          additional_information: '',
          hosted: true,
          caregiver: false
        },
        {
          lastname: 'Test2',
          firstname: 'Entourage',
          type: 'spouse',
          city: '',
          phone: '0600000000',
          email: '',
          additional_information: 'Additional information',
          hosted: false,
          caregiver: false
        }
      ],
      gir: 'gir_2',
      doctor: '',
      health_additional_information: 'Asthme',
      social_security_number: '',
      insurance: '',
      occupation: '',
      main_income_type: '',
      main_income_amount: '',
      main_income_type_partner: '',
      main_income_amount_partner: '24000',
      pole_emploi: '00000',
      pension_organisation: '',
      caf: '',
      bank: 'Banque',
      protection_measure: 'simple_protection',
      representative: '',
      prescribing_organisation: '',
      orientation_organisation: '',
      service_providers: '',
      involved_partners: '',
      additional_information: ''
    })
  })
})
