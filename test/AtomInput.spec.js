import { shallowMount } from '@vue/test-utils'
import AtomInput from '~/components/atom/AtomInput.vue'

async function inputCharacter(inputWrapper, char) {
  inputWrapper.element.value += char
  await inputWrapper.trigger('input', { data: char })
}

async function deleteLastCharacter(inputWrapper) {
  inputWrapper.element.value = inputWrapper.element.value.slice(
    0,
    inputWrapper.element.value.length - 1
  )
  await inputWrapper.trigger('input', { data: null })
}

describe('AtomInput', () => {
  let atomInputWrapper
  let inputWrapper

  afterEach(() => {
    atomInputWrapper.unmount()
  })

  it('has an error', async () => {
    atomInputWrapper = shallowMount(AtomInput, {
      props: { label: 'Test input', name: 'test-input', required: true }
    })
    inputWrapper = atomInputWrapper.get('input')

    await inputWrapper.setValue('')

    expect(atomInputWrapper.classes()).toContain('fr-input-group--error')
    expect(inputWrapper.classes()).toContain('fr-input--error')

    const errorWrapper = atomInputWrapper.find('#test-input-error-desc-error')

    expect(errorWrapper.exists()).toBe(true)

    const errorMessageWrapper = errorWrapper.get('p')

    expect(errorMessageWrapper.text()).toBe('Constraints not satisfied')
  })

  it('has a custom error', async () => {
    atomInputWrapper = shallowMount(AtomInput, {
      props: { label: 'Test input', name: 'test-input', required: true }
    })
    inputWrapper = atomInputWrapper.get('input')

    await inputWrapper.setValue('Non-empty value')

    expect(atomInputWrapper.find('#test-input-error-desc-error').exists()).toBe(
      false
    )

    atomInputWrapper.setProps({ customError: 'Custom error' })
    await atomInputWrapper.vm.$nextTick()

    const errorWrapper = atomInputWrapper.find('#test-input-error-desc-error')

    expect(errorWrapper.exists()).toBe(true)

    const errorMessageWrapper = errorWrapper.get('p')

    expect(errorMessageWrapper.text()).toBe('Custom error')

    await inputWrapper.setValue('')

    expect(errorMessageWrapper.text()).toBe('Custom error')

    atomInputWrapper.setProps({ customError: '' })
    await atomInputWrapper.vm.$nextTick()

    expect(errorMessageWrapper.text()).toBe('Constraints not satisfied')
  })

  it('formats phone numbers', async () => {
    atomInputWrapper = shallowMount(AtomInput, {
      props: { label: 'Test input', name: 'test-input', type: 'tel' }
    })
    inputWrapper = atomInputWrapper.get('input')

    await inputCharacter(inputWrapper, '0')

    expect(inputWrapper.element.value).toBe('0')

    await inputCharacter(inputWrapper, '4')

    expect(inputWrapper.element.value).toBe('04')

    await inputCharacter(inputWrapper, '1')

    expect(inputWrapper.element.value).toBe('04 1')

    await deleteLastCharacter(inputWrapper)
    expect(inputWrapper.element.value).toBe('04 ')

    await inputCharacter(inputWrapper, '1')

    expect(inputWrapper.element.value).toBe('04 1')

    await deleteLastCharacter(inputWrapper)
    await deleteLastCharacter(inputWrapper)

    expect(inputWrapper.element.value).toBe('04')

    await inputCharacter(inputWrapper, '1')

    expect(inputWrapper.element.value).toBe('04 1')
  })

  it('formats social security numbers', async () => {
    atomInputWrapper = shallowMount(AtomInput, {
      props: {
        label: 'Test input',
        name: 'test-input',
        type: 'social-security-number'
      }
    })
    inputWrapper = atomInputWrapper.get('input')

    await inputCharacter(inputWrapper, '1')

    expect(inputWrapper.element.value).toBe('1')

    await inputCharacter(inputWrapper, '2')

    expect(inputWrapper.element.value).toBe('1 2')

    await inputCharacter(inputWrapper, '3')

    expect(inputWrapper.element.value).toBe('1 23')

    await inputCharacter(inputWrapper, '4')

    expect(inputWrapper.element.value).toBe('1 23 4')

    await deleteLastCharacter(inputWrapper)

    expect(inputWrapper.element.value).toBe('1 23 ')

    await inputCharacter(inputWrapper, '4')
    await deleteLastCharacter(inputWrapper)
    await deleteLastCharacter(inputWrapper)

    expect(inputWrapper.element.value).toBe('1 23')

    await inputCharacter(inputWrapper, '4')

    expect(inputWrapper.element.value).toBe('1 23 4')
  })
})
