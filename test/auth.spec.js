// FIXME: Make tests evolution to new Pinia store
import { setActivePinia, createPinia } from 'pinia'
import { useAuthStore } from '../store/auth'

const VALID_ACCESS_TOKEN = 'valid-access-token'
const VALID_EXPIRES = 1000
const VALID_REFRESH_TOKEN = 'valid-refresh-token'
const EMPTY_EXPIRES = -1
const VALID_ACCESS_TOKEN_REFRESHED = 'valid-access-token-refreshed'
const USER = 'valid-user'

const mockApiRequest = vi.fn()

vi.mock('#app', () => {
  return {
    useRuntimeConfig: () => ({ public: { authRefreshDelay: 0.75 } }),
    useNuxtApp: () => ({
      $apiRequest: mockApiRequest
    })
  }
})

describe.skip('Authentication', () => {
  beforeEach(() => {
    setActivePinia(createPinia())
  })

  describe('logout', () => {
    it('logs out', async () => {
      // Temporary mock apiRequest action to check that the correct refresh token is sent
      mockApiRequest.mockReturnValueOnce({ status: 204, ok: true })

      const authStore = useAuthStore()
      authStore.$state = {
        accessToken: VALID_ACCESS_TOKEN,
        expires: VALID_EXPIRES,
        refreshToken: VALID_REFRESH_TOKEN
      }

      await authStore.logout()

      expect(mockApiRequest).toHaveBeenCalledWith('/auth/logout', {
        method: 'post',
        credentials: 'include'
      })
      expect(authStore.accessToken).toBeNull()
      expect(authStore.expires).toBe(EMPTY_EXPIRES)
    })
  })

  describe('refresh', () => {
    beforeEach(() => {
      // Temporary mock loginSuccessful action to make sure only the refresh action is tested
      // store.actions._loginSuccessful = vi.fn(() => {})
    })

    it('refreshes with valid refresh token', async () => {
      // Temporary mock apiRequest action to check that the correct refresh token is sent
      mockApiRequest.mockReturnValueOnce({
        status: 200,
        data: {
          data: {
            access_token: VALID_ACCESS_TOKEN_REFRESHED,
            expires: VALID_EXPIRES
          }
        }
      })

      const authStore = useAuthStore()

      await authStore.refresh()

      expect(authStore.accessToken).toBe(VALID_ACCESS_TOKEN_REFRESHED)
      expect(authStore.expires).toBe(VALID_EXPIRES)
    })

    it('does not refresh with invalid refresh token', async () => {
      // Temporary mock apiRequest action to check that the correct refresh token is sent
      mockApiRequest.mockReturnValueOnce({
        status: 401
      })

      const authStore = useAuthStore()

      await authStore.refresh()

      expect(authStore.accessToken).toBeNull()
      expect(authStore.expires).toBe(EMPTY_EXPIRES)
    })
  })

  describe('_autoRefresh', () => {
    it('does not refresh if expires is empty', async () => {
      global.setTimeout = vi.fn(() => {})

      store.state.expires = EMPTY_EXPIRES

      await store.dispatch('_autoRefresh')

      expect(global.setTimeout.mock.calls.length).toBe(0)
    })

    it('refreshes and restart autoRefresh', async () => {
      vi.useFakeTimers()

      // Temporary refresh action to make sure only the autoRefresh action is tested
      store.actions.refresh = vi.fn(() => true)

      store.actions.$config = { public: { authRefreshDelay: 0.75 } }

      store.state.expires = VALID_EXPIRES
      const dispatchSpy = vi.spyOn(store, 'dispatch')
      const setTimeOutSpy = vi.spyOn(global, 'setTimeout')

      await store.dispatch('_autoRefresh')

      expect(setTimeOutSpy).toBeCalledTimes(1)
      expect(dispatchSpy).toBeCalledTimes(1)
      expect(dispatchSpy).nthCalledWith(1, '_autoRefresh')

      vi.runAllTimers()

      expect(dispatchSpy).nthCalledWith(2, 'refresh')

      // Wait for asynchronous refresh action in timeout callback to end
      const { setImmediate } = await vi.importActual('timers')
      await setImmediate

      expect(dispatchSpy).toBeCalledTimes(3)
      expect(dispatchSpy).nthCalledWith(3, '_autoRefresh')
      expect(setTimeOutSpy).toBeCalledTimes(2)

      vi.useRealTimers()
    })
  })

  describe('_loginSuccessful', () => {
    beforeEach(() => {
      // Temporary autoRefresh action to make sure only the loginSuccessful action is tested
      store.actions._autoRefresh = vi.fn(() => {})
    })

    it('gets the user and update state with new tokens', async () => {
      store.actions._getUser = vi.fn(() => USER)

      await store.dispatch('_loginSuccessful', {
        accessToken: VALID_ACCESS_TOKEN,
        expires: VALID_EXPIRES
      })

      expect(store.state.accessToken).toBe(VALID_ACCESS_TOKEN)
      expect(store.state.expires).toBe(VALID_EXPIRES)
      expect(store.state.user).toBe(USER)
    })

    it('does not update state if user is not retrieved', async () => {
      store.actions._getUser = vi.fn(() => null)

      await expect(
        store.dispatch('_loginSuccessful', {
          accessToken: VALID_ACCESS_TOKEN,
          expires: VALID_EXPIRES
        })
      ).rejects.toThrow('Invalid user')
      expect(store.state.accessToken).toBeNull()
      expect(store.state.expires).toBe(EMPTY_EXPIRES)
      expect(store.state.user).toBeNull()
    })
  })
})
