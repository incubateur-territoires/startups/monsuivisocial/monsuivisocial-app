import { prepareFollowUpTypes } from '~/utils/organisationForm'

describe('organisationForm', () => {
  describe('prepareFollowUpTypes', () => {
    it('computes no changes in the follow up type list of an organisation if it is unchanged', () => {
      expect(
        prepareFollowUpTypes(
          [
            {
              id: '1',
              follow_up_types_id: '0696cce3-ab9c-4023-9113-f76e8f006919'
            },
            {
              id: '2',
              follow_up_types_id: '17b1cde5-db4c-4a53-afeb-7a45baea3739'
            },
            {
              id: '3',
              follow_up_types_id: '12435394-a86e-4fd5-a7fe-a2ca729c9f61'
            },
            {
              id: '4',
              follow_up_types_id: '18365fef-de5c-4b67-a8da-d7590165be9c'
            }
          ],
          [
            '0696cce3-ab9c-4023-9113-f76e8f006919',
            '17b1cde5-db4c-4a53-afeb-7a45baea3739',
            '12435394-a86e-4fd5-a7fe-a2ca729c9f61',
            '18365fef-de5c-4b67-a8da-d7590165be9c'
          ]
        )
      ).toEqual([
        { id: '1', follow_up_types_id: '0696cce3-ab9c-4023-9113-f76e8f006919' },
        { id: '2', follow_up_types_id: '17b1cde5-db4c-4a53-afeb-7a45baea3739' },
        { id: '3', follow_up_types_id: '12435394-a86e-4fd5-a7fe-a2ca729c9f61' },
        { id: '4', follow_up_types_id: '18365fef-de5c-4b67-a8da-d7590165be9c' }
      ])
    })

    it('computes changes in the follow up type list of an organisation if it has changed', () => {
      expect(
        prepareFollowUpTypes(
          [
            {
              id: '1',
              follow_up_types_id: '0696cce3-ab9c-4023-9113-f76e8f006919'
            },
            {
              id: '2',
              follow_up_types_id: '17b1cde5-db4c-4a53-afeb-7a45baea3739'
            },
            {
              id: '3',
              follow_up_types_id: '12435394-a86e-4fd5-a7fe-a2ca729c9f61'
            },
            {
              id: '4',
              follow_up_types_id: '18365fef-de5c-4b67-a8da-d7590165be9c'
            }
          ],
          [
            '020b7a36-7d50-447f-a1aa-06845e5b79e0',
            '12435394-a86e-4fd5-a7fe-a2ca729c9f61',
            '17b1cde5-db4c-4a53-afeb-7a45baea3739',
            '181a198b-61a4-445a-96ae-f756b0b9a1a0',
            '18365fef-de5c-4b67-a8da-d7590165be9c',
            'follow-up-type-custom-7'
          ],
          [
            {
              id: 'follow-up-type-custom-7',
              name: 'Custom follow up type 7',
              type: 'optional'
            }
          ]
        )
      ).toEqual([
        { follow_up_types_id: '020b7a36-7d50-447f-a1aa-06845e5b79e0' },
        {
          id: '3',
          follow_up_types_id: '12435394-a86e-4fd5-a7fe-a2ca729c9f61'
        },
        {
          id: '2',
          follow_up_types_id: '17b1cde5-db4c-4a53-afeb-7a45baea3739'
        },
        { follow_up_types_id: '181a198b-61a4-445a-96ae-f756b0b9a1a0' },
        {
          id: '4',
          follow_up_types_id: '18365fef-de5c-4b67-a8da-d7590165be9c'
        },
        {
          follow_up_types_id: {
            name: 'Custom follow up type 7',
            type: 'optional'
          }
        }
      ])
    })
  })
})
