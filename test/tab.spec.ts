import { focusTab } from '~/utils/tab'

const tabsGroupMock = document.createElement('div')

describe('Tab utils', () => {
  it('focuses a tab in an existing tabs group', () => {
    const dsfrTabsGroup = {
      members: [
        { disclose: vi.fn() },
        { disclose: vi.fn() },
        { disclose: vi.fn() }
      ]
    }
    window.dsfr = vi.fn((_: HTMLElement) => ({
      tabsGroup: dsfrTabsGroup
    }))

    focusTab('second', tabsGroupMock, ['first', 'second', 'third'])

    expect(dsfrTabsGroup.members[0].disclose).not.toHaveBeenCalled()
    expect(dsfrTabsGroup.members[1].disclose).toHaveBeenCalledOnce()
    expect(dsfrTabsGroup.members[2].disclose).not.toHaveBeenCalled()
  })

  it('focuses the first tab when the targeted tab does not exist in an existing tabs group', () => {
    const dsfrTabsGroup = {
      members: [
        { disclose: vi.fn() },
        { disclose: vi.fn() },
        { disclose: vi.fn() }
      ]
    }
    window.dsfr = vi.fn((_: HTMLElement) => ({
      tabsGroup: dsfrTabsGroup
    }))

    focusTab('fourth', tabsGroupMock, ['first', 'second', 'third'])

    expect(dsfrTabsGroup.members[0].disclose).toHaveBeenCalledOnce()
    expect(dsfrTabsGroup.members[1].disclose).not.toHaveBeenCalled()
    expect(dsfrTabsGroup.members[2].disclose).not.toHaveBeenCalled()
  })

  it('does not focus anything if the tabs group does not exist', () => {
    const dsfrTabsGroup = {
      members: [
        { disclose: vi.fn() },
        { disclose: vi.fn() },
        { disclose: vi.fn() }
      ]
    }
    window.dsfr = vi.fn((_: HTMLElement) => undefined)

    focusTab('second', tabsGroupMock, ['first', 'second', 'third'])

    expect(dsfrTabsGroup.members[0].disclose).not.toHaveBeenCalled()
    expect(dsfrTabsGroup.members[1].disclose).not.toHaveBeenCalled()
    expect(dsfrTabsGroup.members[2].disclose).not.toHaveBeenCalled()
  })
})
