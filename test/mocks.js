export function initPluginContext(nuxtApp) {
  vi.mock('#app', () => {
    return {
      defineNuxtPlugin: pluginFactory => {
        const plugin = pluginFactory(nuxtApp)
        Object.entries(plugin.provide).forEach(([key, value]) => {
          nuxtApp[`$${key}`] = value
        })
      }
    }
  })
}
