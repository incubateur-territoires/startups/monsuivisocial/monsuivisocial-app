import { initPluginContext } from '~/test/mocks'

describe('Beneficiaries plugin', () => {
  window.nuxtApp = {}

  beforeAll(async () => {
    initPluginContext(window.nuxtApp)

    await import('~/plugins/beneficiaries.client')
  })

  describe('getBeneficiaryFollowUpTypes', () => {
    it('gets an empty list of follow up types when the beneficiary does not have any interviews nor help requests', () => {
      expect(
        window.nuxtApp.$getBeneficiaryFollowUpTypes({
          follow_ups: [],
          help_requests: []
        })
      ).toEqual([])
    })

    it('gets a list of unique follow up types sorted by name when the beneficiary has interviews but does not have any help requests', () => {
      expect(
        window.nuxtApp.$getBeneficiaryFollowUpTypes({
          follow_ups: [
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-1',
                    name: 'Follow-up type 1'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-4',
                    name: 'Follow-up type 4'
                  }
                },
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-1',
                    name: 'Follow-up type 1'
                  }
                },
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                },
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-4',
                    name: 'Follow-up type 4'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                }
              ]
            }
          ],
          help_requests: []
        })
      ).toEqual([
        {
          id: 'follow-up-type-1',
          name: 'Follow-up type 1'
        },
        {
          id: 'follow-up-type-3',
          name: 'Follow-up type 3'
        },
        {
          id: 'follow-up-type-4',
          name: 'Follow-up type 4'
        }
      ])
    })

    it('gets a list of unique follow up types sorted by name when the beneficiary has help requests but does not have any interviews', () => {
      expect(
        window.nuxtApp.$getBeneficiaryFollowUpTypes({
          follow_ups: [],
          help_requests: [
            {
              follow_up_type: {
                id: 'follow-up-type-1',
                name: 'Follow-up type 1'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-4',
                name: 'Follow-up type 4'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-1',
                name: 'Follow-up type 1'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-3',
                name: 'Follow-up type 3'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-3',
                name: 'Follow-up type 3'
              }
            }
          ]
        })
      ).toEqual([
        {
          id: 'follow-up-type-1',
          name: 'Follow-up type 1'
        },
        {
          id: 'follow-up-type-3',
          name: 'Follow-up type 3'
        },
        {
          id: 'follow-up-type-4',
          name: 'Follow-up type 4'
        }
      ])
    })

    it('gets a list of unique follow up types sorted by name when the beneficiary has help requests and interviews', () => {
      expect(
        window.nuxtApp.$getBeneficiaryFollowUpTypes({
          follow_ups: [
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-2',
                    name: 'Follow-up type 2'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-5',
                    name: 'Follow-up type 5'
                  }
                },
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-2',
                    name: 'Follow-up type 2'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-1',
                    name: 'Follow-up type 1'
                  }
                },
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                },
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-5',
                    name: 'Follow-up type 5'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                }
              ]
            },
            {
              follow_up_types: [
                {
                  follow_up_types_id: {
                    id: 'follow-up-type-3',
                    name: 'Follow-up type 3'
                  }
                }
              ]
            }
          ],
          help_requests: [
            {
              follow_up_type: {
                id: 'follow-up-type-1',
                name: 'Follow-up type 1'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-4',
                name: 'Follow-up type 4'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-1',
                name: 'Follow-up type 1'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-3',
                name: 'Follow-up type 3'
              }
            },
            {
              follow_up_type: {
                id: 'follow-up-type-3',
                name: 'Follow-up type 3'
              }
            }
          ]
        })
      ).toEqual([
        {
          id: 'follow-up-type-1',
          name: 'Follow-up type 1'
        },
        {
          id: 'follow-up-type-2',
          name: 'Follow-up type 2'
        },
        {
          id: 'follow-up-type-3',
          name: 'Follow-up type 3'
        },
        {
          id: 'follow-up-type-4',
          name: 'Follow-up type 4'
        },
        {
          id: 'follow-up-type-5',
          name: 'Follow-up type 5'
        }
      ])
    })
  })
})
