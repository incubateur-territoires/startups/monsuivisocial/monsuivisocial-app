import { setActivePinia, createPinia } from 'pinia'
import { shallowMount, VueWrapper } from '@vue/test-utils'
import BeneficiaryDocument from '~/components/beneficiary/document/BeneficiaryDocument.vue'
import type { DirectusFile } from '~/types/document'

const mockApiRequest = vi.fn()
const mockSnackbarError = vi.fn()
const mockGetItems = vi.fn()

vi.mock('#app', () => {
  return {
    useRuntimeConfig: () => ({ public: { apiUrl: 'http://mock.api.url' } }),
    useNuxtApp: () => ({
      $apiRequest: mockApiRequest,
      $snackbar: { error: mockSnackbarError },
      $getItems: mockGetItems
    })
  }
})

let mockPermissionsCanDeleteDocuments = true

vi.mock('~/store/permissions', () => ({
  usePermissionsStore: () => ({
    canDeleteDocuments: mockPermissionsCanDeleteDocuments
  })
}))

vi.mock('~/store/auth', () => ({
  useAuthStore: () => ({
    accessToken: 'mock-access-token'
  })
}))

describe('BeneficiaryDocument', () => {
  let beneficiaryDocumentWrapper: VueWrapper

  beforeEach(() => {
    setActivePinia(createPinia())
  })

  afterEach(() => {
    beneficiaryDocumentWrapper.unmount()
  })

  it('displays the filename as a title', () => {
    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })
    const titleWrapper = beneficiaryDocumentWrapper.get('h3 > span')

    expect(titleWrapper.text()).toBe('Test CERFA')
  })

  it('displays the file extension and the file size in the card end', () => {
    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })
    const cardEndDetailWrapper = beneficiaryDocumentWrapper.get(
      '.fr-card__end > .fr-card__detail'
    )

    expect(cardEndDetailWrapper.text()).toBe('PDF – 123,46 Mo')
  })

  it('has a preview link', () => {
    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    const cardFooterLinksWrapper = beneficiaryDocumentWrapper.get(
      '.fr-card__footer > .fr-links-group'
    )
    const previewLink = cardFooterLinksWrapper.get('nuxt-link-stub')

    expect(previewLink.attributes('to')).toBe(
      'http://mock.api.url/assets/test-document?access_token=mock-access-token'
    )
    expect(previewLink.attributes('title')).toBe(
      'Nouvel onglet - Voir Test CERFA'
    )
    expect(previewLink.attributes('download')).toBeUndefined()
  })

  it('has a download link', () => {
    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    const cardFooterLinksWrapper = beneficiaryDocumentWrapper.get(
      '.fr-card__footer > .fr-links-group'
    )
    const downloadLink = cardFooterLinksWrapper.findAll('nuxt-link-stub')[1]

    expect(downloadLink.attributes('to')).toBe(
      'http://mock.api.url/assets/test-document?access_token=mock-access-token&download'
    )
    expect(downloadLink.attributes('title')).toBe('Télécharger Test CERFA')
    expect(downloadLink.attributes('download')).toBe('Test CERFA')
  })

  it('has a delete button', () => {
    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    expect(
      beneficiaryDocumentWrapper.find('.btn-delete-action').exists()
    ).toBeTruthy()
  })

  it('has not a delete button if the user can not delete documents', () => {
    mockPermissionsCanDeleteDocuments = false

    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    expect(
      beneficiaryDocumentWrapper.find('.btn-delete-action').exists()
    ).toBeFalsy()

    mockPermissionsCanDeleteDocuments = true
  })

  it('can cancel deletion', async () => {
    // Mock cancelation in confirm alert
    window.confirm = vi.fn().mockReturnValueOnce(false)

    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    const deleteButtonWrapper =
      beneficiaryDocumentWrapper.get('.btn-delete-action')

    await deleteButtonWrapper.trigger('click')

    expect(beneficiaryDocumentWrapper.emitted('deleted')).toBeFalsy()
    expect(mockApiRequest).not.toHaveBeenCalled()
  })

  it('can accept deletion', async () => {
    // Mock acceptation in confirm alert
    window.confirm = vi.fn().mockReturnValueOnce(true)

    // Mock successful API call
    mockApiRequest.mockReturnValueOnce({ status: 204, ok: true })

    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    const deleteButtonWrapper =
      beneficiaryDocumentWrapper.get('.btn-delete-action')

    await deleteButtonWrapper.trigger('click')

    expect(beneficiaryDocumentWrapper.emitted('deleted')).toBeTruthy()
    // [NUXT3] Don't know if it is a lack of vitest or @vue/test-utils but beneficiaryDocumentWrapper.emitted('deleted') is still considered as possibly undefined after the above expect statement
    expect((beneficiaryDocumentWrapper.emitted('deleted') as any[])[0]).toEqual(
      [
        {
          documentId: 'test-document',
          documentType: 'cerfa'
        }
      ]
    )
    expect(mockApiRequest).toHaveBeenCalledWith('/files/test-document', {
      method: 'delete'
    })
  })

  it('opens a snackbar if document deletion fails', async () => {
    // Mock acceptation in confirm alert
    window.confirm = vi.fn().mockReturnValueOnce(true)

    // Mock failing API call
    mockApiRequest.mockReturnValueOnce({
      status: 400,
      data: { errors: [{ message: 'Test error' }] }
    })

    beneficiaryDocumentWrapper = shallowMount(BeneficiaryDocument, {
      props: {
        document: {
          id: 'test-document',
          document_type: 'cerfa',
          type: 'application/pdf',
          filesize: 123456789,
          filename_download: 'Test CERFA',
          confidential: false,
          labels: []
        } as DirectusFile,
        unreadNotifications: []
      }
    })

    const deleteButtonWrapper =
      beneficiaryDocumentWrapper.get('.btn-delete-action')

    await deleteButtonWrapper.trigger('click')

    expect(beneficiaryDocumentWrapper.emitted('deleted')).toBeFalsy()
    expect(mockApiRequest).toHaveBeenCalledWith('/files/test-document', {
      method: 'delete'
    })
    expect(mockSnackbarError).toHaveBeenCalled()
  })
})
