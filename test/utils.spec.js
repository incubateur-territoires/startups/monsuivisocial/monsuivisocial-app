import {
  getNestedValue,
  sort,
  directusItemDiff,
  setEmptyFieldsToNull,
  prepareM2MRelation,
  age,
  readableFileSize,
  readableFileType,
  euros,
  relativeGeneralInformation,
  relativeContact
} from '~/utils'

describe('sort', () => {
  it('sorts a array of objects', () => {
    expect(
      sort(
        [
          { firstname: 'Rosario', lastname: 'Booth' },
          { firstname: 'Franklin', lastname: 'Ballard' },
          { firstname: 'Graciela', lastname: 'Schneider' },
          { firstname: 'graciela', lastname: 'Moreno' },
          { firstname: 'Keven', lastname: 'Moreno' },
          { firstname: 'Tia', lastname: 'Parks' }
        ],
        [{ field: 'firstname', sort: '' }]
      )
    ).toEqual([
      { firstname: 'Franklin', lastname: 'Ballard' },
      { firstname: 'Graciela', lastname: 'Schneider' },
      { firstname: 'graciela', lastname: 'Moreno' },
      { firstname: 'Keven', lastname: 'Moreno' },
      { firstname: 'Rosario', lastname: 'Booth' },
      { firstname: 'Tia', lastname: 'Parks' }
    ])

    expect(
      sort(
        [
          { firstname: 'Rosario', lastname: 'Booth' },
          { firstname: 'Franklin', lastname: 'Ballard' },
          { firstname: 'Graciela', lastname: 'Schneider' },
          { firstname: 'graciela', lastname: 'Moreno' },
          { firstname: 'Keven', lastname: 'Moreno' },
          { firstname: 'Tia', lastname: 'Parks' }
        ],
        [{ field: 'firstname', sort: '-' }]
      )
    ).toEqual([
      { firstname: 'Tia', lastname: 'Parks' },
      { firstname: 'Rosario', lastname: 'Booth' },
      { firstname: 'Keven', lastname: 'Moreno' },
      { firstname: 'Graciela', lastname: 'Schneider' },
      { firstname: 'graciela', lastname: 'Moreno' },
      { firstname: 'Franklin', lastname: 'Ballard' }
    ])

    expect(
      sort(
        [
          { firstname: 'Rosario', lastname: 'Booth' },
          { firstname: 'Franklin', lastname: 'Ballard' },
          { firstname: 'Graciela', lastname: 'Schneider' },
          { firstname: 'graciela', lastname: 'Moreno' },
          { firstname: 'Keven', lastname: 'Moreno' },
          { firstname: 'Tia', lastname: 'Parks' }
        ],
        [
          { field: 'firstname', sort: '-' },
          { field: 'lastname', sort: '' }
        ]
      )
    ).toEqual([
      { firstname: 'Tia', lastname: 'Parks' },
      { firstname: 'Rosario', lastname: 'Booth' },
      { firstname: 'Keven', lastname: 'Moreno' },
      { firstname: 'graciela', lastname: 'Moreno' },
      { firstname: 'Graciela', lastname: 'Schneider' },
      { firstname: 'Franklin', lastname: 'Ballard' }
    ])

    expect(
      sort(
        [
          { firstname: 'Rosario', lastname: 'Booth' },
          { firstname: 'Franklin', lastname: 'Ballard' },
          { firstname: 'Graciela', lastname: 'Schneider' },
          { firstname: 'graciela', lastname: 'Moreno' },
          { firstname: 'Keven', lastname: 'Moreno' },
          { firstname: 'Tia', lastname: 'Parks' }
        ],
        [
          { field: 'lastname', sort: '' },
          { field: 'firstname', sort: '-' }
        ]
      )
    ).toEqual([
      { firstname: 'Franklin', lastname: 'Ballard' },
      { firstname: 'Rosario', lastname: 'Booth' },
      { firstname: 'Keven', lastname: 'Moreno' },
      { firstname: 'graciela', lastname: 'Moreno' },
      { firstname: 'Tia', lastname: 'Parks' },
      { firstname: 'Graciela', lastname: 'Schneider' }
    ])

    expect(
      sort(
        [
          { identity: { firstname: 'Rosario', lastname: 'Booth' } },
          { identity: { firstname: 'Franklin', lastname: 'Ballard' } },
          { identity: { firstname: 'Graciela', lastname: 'Schneider' } },
          { identity: { firstname: 'graciela', lastname: 'Moreno' } },
          { identity: { firstname: 'Keven', lastname: 'Moreno' } },
          { identity: { firstname: 'Tia', lastname: 'Parks' } }
        ],
        [{ field: 'identity.firstname', sort: '-' }]
      )
    ).toEqual([
      { identity: { firstname: 'Tia', lastname: 'Parks' } },
      { identity: { firstname: 'Rosario', lastname: 'Booth' } },
      { identity: { firstname: 'Keven', lastname: 'Moreno' } },
      { identity: { firstname: 'Graciela', lastname: 'Schneider' } },
      { identity: { firstname: 'graciela', lastname: 'Moreno' } },
      { identity: { firstname: 'Franklin', lastname: 'Ballard' } }
    ])
  })
})

describe('getNestedValue', () => {
  it('gets nested values', () => {
    expect(getNestedValue({ a: 'correct', b: 'incorrect' }, 'a')).toBe(
      'correct'
    )

    expect(
      getNestedValue({ a: 'correct', b: 'incorrect' }, 'c')
    ).toBeUndefined()

    expect(
      getNestedValue(
        { a: { c: 'correct', d: 'incorrect' }, b: 'incorrect' },
        'a'
      )
    ).toEqual({ c: 'correct', d: 'incorrect' })

    expect(
      getNestedValue(
        { a: { c: 'correct', d: 'incorrect' }, b: 'incorrect' },
        'a.c'
      )
    ).toBe('correct')

    expect(
      getNestedValue(
        { a: { c: 'correct', d: 'incorrect' }, b: 'incorrect' },
        'a.e'
      )
    ).toBeUndefined()

    expect(
      getNestedValue(
        { a: { c: { e: 'correct' }, d: 'incorrect' }, b: 'incorrect' },
        'a.c.e'
      )
    ).toBe('correct')
  })
})

describe('directusItemDiff', () => {
  it('does not return changes for an empty object', () => {
    expect(directusItemDiff({}, {})).toBeNull()
  })

  it('does not return changes for an unchanged object with only one property', () => {
    expect(directusItemDiff({ foo: 'bar' }, { foo: 'bar' })).toBeNull()
  })

  it('does not return changes for an unchanged object with multiple properties', () => {
    expect(
      directusItemDiff(
        { foo: { bar: 'bar1', baz: 'baz1' } },
        { foo: { bar: 'bar1', baz: 'baz1' } }
      )
    ).toBeNull()
  })

  it('does not return changes for an unchanged array', () => {
    expect(
      directusItemDiff({ foo: ['bar1', 'baz1'] }, { foo: ['bar1', 'baz1'] })
    ).toBeNull()
  })

  it('does not return changes for an array with changed order but unchanged content', () => {
    expect(
      directusItemDiff({ foo: ['bar1', 'baz1'] }, { foo: ['baz1', 'bar1'] })
    ).toBeNull()
  })

  it('does not return changes for an unchanged array with nested objects', () => {
    expect(
      directusItemDiff(
        {
          foo: [
            { id: 'id1', bar: 'baz1' },
            { id: 'id2', bar: 'baz2' }
          ]
        },
        {
          foo: [
            { id: 'id1', bar: 'baz1' },
            { id: 'id2', bar: 'baz2' }
          ]
        }
      )
    ).toBeNull()
  })

  it('does not return changes on an unchanged complex object', () => {
    expect(
      directusItemDiff(
        {
          id: 'fff8cdf8-fb86-4123-825e-f990aedcb530',
          user_created: 'e20c6461-d8b4-40e4-bff9-3c2e361b3e18',
          date_created: '2022-09-19T09:35:24.434Z',
          user_updated: null,
          date_updated: null,
          title: 'man',
          usual_name: 'Bénéficiaire',
          birth_name: null,
          firstname: 'Jean',
          birthdate: '1970-01-01',
          birth_place: 'Lieu de naissance',
          gender: 'man',
          nationality: 'FRA',
          zip_code: '30200',
          city: 'Bagnols-sur-Cèze',
          street: 'Rue du Général Teste',
          street_number: '1',
          address_complement: null,
          phone_1: '06 00 00 00 00',
          phone_2: null,
          email: 'test@example.com',
          family_situation: 'concubinage',
          minor_children: 1,
          major_children: null,
          gir: 'gir_1',
          doctor: 'Médecin traitant',
          health_additional_information: null,
          social_security_number: '1 01 01 01 000 000',
          occupation: 'Profession',
          pension_organisation: null,
          caf: null,
          bank: null,
          protection_measure: 'simple_protection',
          representative: 'Mandataire',
          prescribing_organisation: null,
          orientation_organisation: "Organisme d'orientation",
          service_providers: null,
          involved_partners: null,
          insurance: null,
          status: 'active',
          pole_emploi: '0000000',
          mobility: 'licence_only',
          accommodation_mode: 'social_tenant',
          accommodation_name: null,
          aidant_connect_authorisation: false,
          deathdate: null,
          caregiver: false,
          socio_professional_category: 'employed',
          employer: null,
          employer_siret: null,
          main_income_type: ['family_benefits', 'unemployment_benefit'],
          main_income_amount: 950,
          funeral_contract: null,
          orientation_type: 'spontaneous',
          partner_main_income_type: ['family_benefits', 'income'],
          partner_main_income_amount: 1700,
          major_children_main_income_amount: null,
          major_children_main_income_type: null,
          minister: null,
          administration: null,
          additional_information: null,
          accommodation_additional_information: null,
          relatives: [
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
              firstname: 'Jeannette',
              lastname: 'Bénéficiaire',
              type: 'spouse',
              city: 'Bagnols-sur-Cèze',
              email: 'test-entourage@example.com',
              phone: '06 10 00 00 00',
              additional_information: null,
              hosted: true,
              caregiver: false
            },
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba4',
              firstname: 'Enfant',
              lastname: 'Bénéficiaire',
              type: 'minor_child',
              city: 'Bagnols-sur-Cèze',
              email: null,
              phone: null,
              additional_information: null,
              hosted: true,
              caregiver: false
            }
          ],
          agent: {
            id: 'a1cf221e-efd7-42b3-9841-24bd9269e23a',
            first_name: 'Jean',
            last_name: 'Agent de Montpellier'
          }
        },
        {
          id: 'fff8cdf8-fb86-4123-825e-f990aedcb530',
          user_created: 'e20c6461-d8b4-40e4-bff9-3c2e361b3e18',
          date_created: '2022-09-19T09:35:24.434Z',
          user_updated: null,
          date_updated: null,
          title: 'man',
          usual_name: 'Bénéficiaire',
          birth_name: null,
          firstname: 'Jean',
          birthdate: '1970-01-01',
          birth_place: 'Lieu de naissance',
          gender: 'man',
          nationality: 'FRA',
          zip_code: '30200',
          city: 'Bagnols-sur-Cèze',
          street: 'Rue du Général Teste',
          street_number: '1',
          address_complement: null,
          phone_1: '06 00 00 00 00',
          phone_2: null,
          email: 'test@example.com',
          family_situation: 'concubinage',
          minor_children: '1',
          major_children: null,
          gir: 'gir_1',
          doctor: 'Médecin traitant',
          health_additional_information: null,
          social_security_number: '1 01 01 01 000 000',
          occupation: 'Profession',
          pension_organisation: null,
          caf: null,
          bank: null,
          protection_measure: 'simple_protection',
          representative: 'Mandataire',
          prescribing_organisation: null,
          orientation_organisation: "Organisme d'orientation",
          service_providers: null,
          involved_partners: null,
          insurance: null,
          status: 'active',
          pole_emploi: '0000000',
          mobility: 'licence_only',
          accommodation_mode: 'social_tenant',
          accommodation_name: null,
          aidant_connect_authorisation: false,
          deathdate: null,
          caregiver: false,
          socio_professional_category: 'employed',
          employer: null,
          employer_siret: null,
          main_income_type: ['family_benefits', 'unemployment_benefit'],
          main_income_amount: '950',
          funeral_contract: null,
          orientation_type: 'spontaneous',
          partner_main_income_type: ['family_benefits', 'income'],
          partner_main_income_amount: '1700',
          major_children_main_income_amount: null,
          major_children_main_income_type: null,
          minister: null,
          administration: null,
          additional_information: null,
          accommodation_additional_information: null,
          relatives: [
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
              firstname: 'Jeannette',
              lastname: 'Bénéficiaire',
              type: 'spouse',
              city: 'Bagnols-sur-Cèze',
              email: 'test-entourage@example.com',
              phone: '06 10 00 00 00',
              additional_information: null,
              hosted: true,
              caregiver: false
            },
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba4',
              firstname: 'Enfant',
              lastname: 'Bénéficiaire',
              type: 'minor_child',
              city: 'Bagnols-sur-Cèze',
              email: null,
              phone: null,
              additional_information: null,
              hosted: true,
              caregiver: false
            }
          ],
          agent: {
            id: 'a1cf221e-efd7-42b3-9841-24bd9269e23a'
          }
        }
      )
    ).toBeNull()
  })

  it('returns changes for an object with a different value', () => {
    expect(directusItemDiff({ foo: 'baz' }, { foo: 'bar' })).toEqual({
      foo: 'bar'
    })
  })

  it('returns changes for an object with a different value in a nested object', () => {
    expect(
      directusItemDiff(
        { foo1: { bar1: 'baz1', bar2: 'baz3' }, foo2: { bar1: 'baz2' } },
        { foo1: { bar1: 'baz2', bar2: 'baz3' }, foo2: { bar1: 'baz2' } }
      )
    ).toEqual({ foo1: { bar1: 'baz2' } })
  })

  it('returns changes for an object with a different value that is an array', () => {
    expect(
      directusItemDiff(
        { foo: ['bar1', 'baz1', 'bar2', 'baz2'] },
        { foo: ['baz2', 'bar1', 'bar2'] }
      )
    ).toEqual({
      foo: ['baz2', 'bar1', 'bar2']
    })
  })

  it('returns changes for a changed array with nested objects', () => {
    expect(
      directusItemDiff(
        {
          foo: [
            { id: 'id1', bar: 'baz1' },
            { id: 'id2', bar: 'baz2' },
            { id: 'id3', bar: 'baz3' }
          ]
        },
        {
          foo: [
            { id: 'id1', bar: 'baz1_1' },
            { id: 'id2', bar: 'baz2' },
            { bar: 'baz4' }
          ]
        }
      )
    ).toEqual({
      foo: [{ id: 'id1', bar: 'baz1_1' }, 'id2', { bar: 'baz4' }]
    })
  })

  it('returns changes for an array with removed nested objects', () => {
    expect(
      directusItemDiff(
        {
          foo: [
            { id: 'id1', bar: 'baz1' },
            { id: 'id2', bar: 'baz2' },
            { id: 'id3', bar: 'baz3' }
          ]
        },
        {
          foo: [
            { id: 'id1', bar: 'baz1' },
            { id: 'id2', bar: 'baz2' }
          ]
        }
      )
    ).toEqual({
      foo: ['id1', 'id2']
    })
  })

  it('returns changes on a complex object', () => {
    expect(
      directusItemDiff(
        {
          id: 'fff8cdf8-fb86-4123-825e-f990aedcb530',
          user_created: 'e20c6461-d8b4-40e4-bff9-3c2e361b3e18',
          date_created: '2022-09-19T09:35:24.434Z',
          user_updated: null,
          date_updated: null,
          title: 'man',
          usual_name: 'Bénéficiaire',
          birth_name: null,
          firstname: 'Jean',
          birthdate: '1970-01-01',
          birth_place: 'Lieu de naissance',
          gender: 'man',
          nationality: 'FRA',
          zip_code: '30200',
          city: 'Bagnols-sur-Cèze',
          street: 'Rue du Général Teste',
          street_number: '1',
          address_complement: null,
          phone_1: '06 00 00 00 00',
          phone_2: null,
          email: 'test@example.com',
          family_situation: 'concubinage',
          minor_children: 1,
          major_children: 1,
          gir: 'gir_1',
          doctor: 'Médecin traitant',
          health_additional_information: null,
          social_security_number: '1 01 01 01 000 000',
          occupation: 'Profession',
          pension_organisation: null,
          caf: null,
          bank: null,
          protection_measure: 'simple_protection',
          representative: 'Mandataire',
          prescribing_organisation: null,
          orientation_organisation: "Organisme d'orientation",
          service_providers: null,
          involved_partners: null,
          insurance: null,
          status: 'active',
          pole_emploi: '0000000',
          mobility: 'licence_only',
          accommodation_mode: 'social_tenant',
          accommodation_name: null,
          aidant_connect_authorisation: false,
          deathdate: null,
          caregiver: false,
          socio_professional_category: 'employed',
          employer: null,
          employer_siret: null,
          main_income_type: ['family_benefits', 'unemployment_benefit'],
          main_income_amount: 950,
          funeral_contract: null,
          orientation_type: 'spontaneous',
          partner_main_income_type: ['family_benefits', 'income'],
          partner_main_income_amount: 1700,
          major_children_main_income_amount: null,
          major_children_main_income_type: null,
          minister: null,
          administration: null,
          additional_information: null,
          accommodation_additional_information: null,
          relatives: [
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
              firstname: 'Jeannette',
              lastname: 'Bénéficiaire',
              type: 'spouse',
              city: 'Bagnols-sur-Cèze',
              email: 'test-entourage@example.com',
              phone: '06 10 00 00 00',
              additional_information: null,
              hosted: true,
              caregiver: false
            },
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba4',
              firstname: 'Enfant',
              lastname: 'Bénéficiaire',
              type: 'minor_child',
              city: 'Bagnols-sur-Cèze',
              email: null,
              phone: null,
              additional_information: null,
              hosted: true,
              caregiver: false
            }
          ],
          agent: {
            id: 'a1cf221e-efd7-42b3-9841-24bd9269e23a',
            first_name: 'Jean',
            last_name: 'Agent de Montpellier'
          }
        },
        {
          id: 'fff8cdf8-fb86-4123-825e-f990aedcb530',
          user_created: 'e20c6461-d8b4-40e4-bff9-3c2e361b3e18',
          date_created: '2022-09-19T09:35:24.434Z',
          user_updated: 'e20c6461-d8b4-40e4-bff9-3c2e361b3e18',
          date_updated: '2022-09-19T09:43:05.049Z',
          title: 'man',
          usual_name: 'Bénéficiaire',
          birth_name: null,
          firstname: 'Jean',
          birthdate: '1970-01-01',
          birth_place: 'Lieu de naissance',
          gender: 'man',
          nationality: 'FRA',
          zip_code: '49250',
          city: 'Beaufort-en-Anjou',
          street: 'Test',
          street_number: '1',
          address_complement: null,
          phone_1: '06 00 00 00 00',
          phone_2: null,
          email: 'test@example.com',
          family_situation: 'concubinage',
          minor_children: '2',
          major_children: null,
          gir: 'gir_2',
          doctor: 'Médecin traitant 2',
          health_additional_information: null,
          social_security_number: '1 01 01 01 000 000',
          occupation: 'Profession',
          pension_organisation: null,
          caf: null,
          bank: null,
          protection_measure: 'care',
          representative: 'Mandataire',
          prescribing_organisation: null,
          orientation_organisation: "Organisme d'orientation",
          service_providers: null,
          involved_partners: null,
          insurance: null,
          status: 'active',
          pole_emploi: null,
          mobility: 'licence_vehicle',
          accommodation_mode: 'collective_accommodation',
          accommodation_name: "Structure d'hébergement",
          aidant_connect_authorisation: true,
          deathdate: null,
          caregiver: false,
          socio_professional_category: 'employed',
          employer: null,
          employer_siret: null,
          main_income_type: ['bonus', 'family_benefits', 'income'],
          main_income_amount: '1350',
          funeral_contract: null,
          orientation_type: 'spontaneous',
          partner_main_income_type: ['family_benefits', 'income'],
          partner_main_income_amount: '1700',
          major_children_main_income_amount: null,
          major_children_main_income_type: ['family_benefits'],
          minister: null,
          administration: null,
          additional_information: null,
          accommodation_additional_information: 'Précisions hébergement',
          relatives: [
            {
              id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
              firstname: 'Jeannette',
              lastname: 'Bénéficiaire',
              type: 'spouse',
              city: 'Beaufort-en-Anjou',
              email: 'test-entourage@example.com',
              phone: '06 10 00 00 00',
              additional_information: 'Informations complémentaires',
              hosted: false,
              caregiver: true
            },
            {
              firstname: 'Enfant',
              lastname: 'Bénéficiaire',
              type: 'minor_child',
              city: 'Bagnols-sur-Cèze',
              email: null,
              phone: null,
              additional_information: null,
              hosted: true,
              caregiver: false
            }
          ],
          agent: {
            id: 'a1cf221e-efd7-42b3-9841-24bd9269e23a'
          }
        }
      )
    ).toEqual({
      zip_code: '49250',
      city: 'Beaufort-en-Anjou',
      street: 'Test',
      minor_children: '2',
      major_children: null,
      gir: 'gir_2',
      doctor: 'Médecin traitant 2',
      protection_measure: 'care',
      pole_emploi: null,
      mobility: 'licence_vehicle',
      accommodation_mode: 'collective_accommodation',
      accommodation_name: "Structure d'hébergement",
      aidant_connect_authorisation: true,
      main_income_type: ['bonus', 'family_benefits', 'income'],
      main_income_amount: '1350',
      major_children_main_income_type: ['family_benefits'],
      accommodation_additional_information: 'Précisions hébergement',
      relatives: [
        {
          id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
          city: 'Beaufort-en-Anjou',
          additional_information: 'Informations complémentaires',
          hosted: false,
          caregiver: true
        },
        {
          firstname: 'Enfant',
          lastname: 'Bénéficiaire',
          type: 'minor_child',
          city: 'Bagnols-sur-Cèze',
          email: null,
          phone: null,
          additional_information: null,
          hosted: true,
          caregiver: false
        }
      ]
    })
  })

  it('returns changes when a relation array is emptied', () => {
    expect(
      directusItemDiff({ foo: [{ id: '1', bar: 'bar1' }] }, { foo: [] })
    ).toEqual({
      foo: []
    })
  })
})

describe('setEmptyFieldsToNull', () => {
  it('sets empty fields to null', () => {
    expect(
      setEmptyFieldsToNull({
        title: 'man',
        usual_name: 'Bénéficiaire',
        birth_name: '',
        firstname: 'Jean',
        birthdate: '1970-01-01',
        birth_place: '',
        gender: '',
        main_income_type: ['bonus', 'family_benefits', 'income'],
        relatives: [
          {
            id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
            firstname: 'Jeannette',
            lastname: 'Bénéficiaire',
            type: 'spouse',
            city: 'Beaufort-en-Anjou',
            email: 'test-entourage@example.com',
            phone: '06 10 00 00 00',
            additional_information: '',
            hosted: false,
            caregiver: true
          },
          {
            firstname: 'Enfant',
            lastname: 'Bénéficiaire',
            type: 'minor_child',
            city: 'Bagnols-sur-Cèze',
            email: '',
            phone: '',
            additional_information: 'Informations complémentaires',
            hosted: true,
            caregiver: false
          }
        ],
        agent: {
          id: 'a1cf221e-efd7-42b3-9841-24bd9269e23a',
          first_name: '',
          last_name: 'Agent de Montpellier'
        }
      })
    ).toEqual({
      title: 'man',
      usual_name: 'Bénéficiaire',
      birth_name: null,
      firstname: 'Jean',
      birthdate: '1970-01-01',
      birth_place: null,
      gender: null,
      main_income_type: ['bonus', 'family_benefits', 'income'],
      relatives: [
        {
          id: '4d96bc56-d4a9-4b59-964b-a7fd4ccd8ba3',
          firstname: 'Jeannette',
          lastname: 'Bénéficiaire',
          type: 'spouse',
          city: 'Beaufort-en-Anjou',
          email: 'test-entourage@example.com',
          phone: '06 10 00 00 00',
          additional_information: null,
          hosted: false,
          caregiver: true
        },
        {
          firstname: 'Enfant',
          lastname: 'Bénéficiaire',
          type: 'minor_child',
          city: 'Bagnols-sur-Cèze',
          email: null,
          phone: null,
          additional_information: 'Informations complémentaires',
          hosted: true,
          caregiver: false
        }
      ],
      agent: {
        id: 'a1cf221e-efd7-42b3-9841-24bd9269e23a',
        first_name: null,
        last_name: 'Agent de Montpellier'
      }
    })
  })
})

describe('prepareM2MRelation', () => {
  it('prepare an empty list of M2M relations from an initial empty list and an empty list of selected foreign ids', () => {
    expect(prepareM2MRelation([], [], 'follow_up_types_id')).toEqual([])
  })

  it('prepare a list of M2M relations from an initial empty list and a list of selected foreign ids', () => {
    expect(
      prepareM2MRelation(
        [],
        ['follow-up-type-1', 'follow-up-type-2'],
        'follow_up_types_id'
      )
    ).toEqual([
      { follow_up_types_id: 'follow-up-type-1' },
      { follow_up_types_id: 'follow-up-type-2' }
    ])
  })

  it('prepare a list of M2M relations from an initial list and an identical list of selected foreign ids', () => {
    expect(
      prepareM2MRelation(
        [
          { id: '1', follow_up_types_id: 'follow-up-type-1' },
          { id: '2', follow_up_types_id: 'follow-up-type-2' }
        ],
        ['follow-up-type-1', 'follow-up-type-2'],
        'follow_up_types_id'
      )
    ).toEqual([
      { id: '1', follow_up_types_id: 'follow-up-type-1' },
      { id: '2', follow_up_types_id: 'follow-up-type-2' }
    ])
  })

  it('prepare a updated list of M2M relations from an initial list and a different list of selected foreign ids', () => {
    expect(
      prepareM2MRelation(
        [
          { id: '1', follow_up_types_id: 'follow-up-type-1' },
          { id: '2', follow_up_types_id: 'follow-up-type-2' }
        ],
        ['follow-up-type-1', 'follow-up-type-3'],
        'follow_up_types_id'
      )
    ).toEqual([
      { id: '1', follow_up_types_id: 'follow-up-type-1' },
      { follow_up_types_id: 'follow-up-type-3' }
    ])
  })

  it('prepare a list of M2M relations from an initial list with nested objects and an identical list of selected foreign ids', () => {
    expect(
      prepareM2MRelation(
        [
          {
            id: 52,
            referent: {
              id: 'referent-1',
              first_name: 'Jean',
              last_name: 'Référent 1'
            }
          },
          {
            id: 53,
            referent: {
              id: 'referent-2',
              first_name: 'Jean',
              last_name: 'Référent 2'
            }
          }
        ],
        ['referent-1', 'referent-3'],
        'referent'
      )
    ).toEqual([
      {
        id: 52,
        referent: {
          id: 'referent-1',
          first_name: 'Jean',
          last_name: 'Référent 1'
        }
      },
      { referent: 'referent-3' }
    ])
  })
})

describe('age', () => {
  it('calculates an  age from a birth date', () => {
    expect(age('09-16-1997')).toBe(25)
    expect(age('08-02-1965')).toBe(57)
  })
})

describe('readableFileSize', () => {
  it('converts a file size in byte to a human-readable file size', () => {
    expect(readableFileSize(845)).toBe('845 o')
    expect(readableFileSize(12_845)).toBe('12,85 ko')
    expect(readableFileSize(87_912_845)).toBe('87,91 Mo')
    expect(readableFileSize(1_287_912_845)).toBe('1,29 Go')
  })
})

describe('readableFileType', () => {
  it('converts a MIME type to a human-readable file type', () => {
    expect(readableFileType('application/pdf')).toBe('PDF')
    expect(
      readableFileType(
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      )
    ).toBe('XLSX')
    expect(readableFileType('application/vnd.ms-excel')).toBe('XLS')
    expect(readableFileType('image/svg+xml')).toBe('SVG')
    expect(readableFileType('image/jpeg')).toBe('JPEG')
    expect(readableFileType('unknown')).toBe('unknown')
  })
})

describe('euros', () => {
  it('parses a number without digit to a 2-digit number as a string', () => {
    expect(euros(2)).toBe('2,00 €')
  })
  it('parses a number with 1 digit to a 2-digit number as a string', () => {
    expect(euros(2.1)).toBe('2,10 €')
  })
  it('parses a number with 2 digit to a 2-digit number as a string', () => {
    expect(euros(2.12)).toBe('2,12 €')
  })
  it('parses a number with more than 2 digit to a 2-digit number as a string', () => {
    expect(euros(2.12345)).toBe('2,12 €')
  })
})

describe('relativeGeneralInformation', () => {
  it('gets relative general information', () => {
    expect(
      relativeGeneralInformation({
        firstname: 'Relative',
        lastname: 'Test',
        type: 'grand_parent',
        city: 'City',
        phone: '0600000000',
        email: 'test@example.com'
      })
    ).toBe('Relative TEST - Grand-parent')
    expect(
      relativeGeneralInformation({
        firstname: 'Relative',
        lastname: 'Test',
        type: 'invalid',
        phone: '0600000000'
      })
    ).toBe('Relative TEST - invalid')
    expect(relativeGeneralInformation({ lastname: 'Test' })).toBe(
      '(non renseigné) TEST'
    )
  })
})

describe('relativeContact', () => {
  it('gets relative contact', () => {
    expect(
      relativeContact({
        firstname: 'Relative',
        lastname: 'Test',
        type: 'grand_parent',
        city: 'City',
        phone: '0600000000',
        email: 'test@example.com'
      })
    ).toBe('City / 0600000000 / test@example.com')
    expect(
      relativeContact({
        firstname: 'Relative',
        lastname: 'Test',
        type: 'invalid',
        phone: '0600000000'
      })
    ).toBe('0600000000')
    expect(relativeContact({ lastname: 'Test' })).toBe('')
  })
})
