import { defineStore } from 'pinia'
import {
  MINISTRY_ORGANISATION_TYPE_VALUE,
  ASSOCIATION_ORGANISATION_TYPE_VALUE
} from '~/utils/constants/organisation'
import { errorResponseMessage } from '~/utils/errorResponse'
import { ROLES } from '~/utils/constants/roles'

export const useAuthStore = defineStore('auth', () => {
  const { $apiRequest } = useNuxtApp()
  const config = useRuntimeConfig()

  const EMPTY_EXPIRES = -1
  let timer

  const user = ref(null)
  const accessToken = ref(null)
  const expires = ref(EMPTY_EXPIRES)

  const isAuthenticated = computed(() => accessToken.value && user.value)
  const isResponsableDeStructure = computed(
    () => user.value?.role.slug === ROLES.responsableDeStructure
  )
  const isTravailleurSocial = computed(
    () => user.value?.role.slug === ROLES.travailleurSocial
  )
  const isInstructeur = computed(
    () => user.value?.role.slug === ROLES.instructeur
  )
  const isReferent = computed(() => user.value?.role.slug === ROLES.referent)
  const isAgentDAccueil = computed(
    () => user.value?.role.slug === ROLES.agentDAccueil
  )
  const userId = computed(() => user.value?.id)
  const userName = computed(() =>
    user.value ? `${user.value.first_name} ${user.value.last_name}` : ''
  )
  const userOrganisationId = computed(() => user.value?.organisation.id)
  const isMinistryAgent = computed(
    () => user.value?.organisation.type === MINISTRY_ORGANISATION_TYPE_VALUE
  )
  const isAssociationAgent = computed(
    () => user.value?.organisation.type === ASSOCIATION_ORGANISATION_TYPE_VALUE
  )

  async function logout() {
    try {
      const res = await $apiRequest('/auth/logout', {
        method: 'post',
        credentials: 'include'
      })

      if (res.status !== 204) throw new Error(errorResponseMessage(res))

      _logout()
    } catch {
      _logout()
    }
  }

  async function refresh() {
    try {
      const res = await $apiRequest('/auth/refresh', {
        method: 'post',
        credentials: 'include'
      })

      if (res.status !== 200) throw new Error(errorResponseMessage(res))

      const data = res.data.data

      await _loginSuccessful(data.access_token, data.expires)
    } catch {
      _logout()
    }
  }

  function _login(_accessToken, _expires, _user) {
    user.value = _user
    accessToken.value = _accessToken
    expires.value = _expires
  }

  function _logout() {
    user.value = null
    accessToken.value = null
    expires.value = EMPTY_EXPIRES
  }

  function _autoRefresh() {
    if (expires.value === EMPTY_EXPIRES) return

    if (timer) clearTimeout(timer)

    timer = setTimeout(async () => {
      if (await refresh()) _autoRefresh()
    }, expires.value * config.public.authRefreshDelay)
  }

  async function _loginSuccessful(accessToken, expires) {
    const _user = user.value || (await _getUser(accessToken))

    if (_user) {
      _login(accessToken, expires, _user)
      _autoRefresh()
    } else throw new Error('Invalid user')
  }

  async function _getUser(accessToken) {
    const res = await $apiRequest(
      '/users/me',
      { headers: new Headers({ Authorization: `Bearer ${accessToken}` }) },
      {
        fields: [
          'id',
          'first_name',
          'last_name',
          'email',
          'role.slug',
          'role.name',
          'organisation.id',
          'organisation.type'
        ]
      }
    )

    if (res.status !== 200) throw new Error(errorResponseMessage(res))
    return { ...res.data.data }
  }

  return {
    user,
    accessToken,
    isAuthenticated,
    isResponsableDeStructure,
    isTravailleurSocial,
    isInstructeur,
    isReferent,
    isAgentDAccueil,
    userId,
    userName,
    userOrganisationId,
    isMinistryAgent,
    isAssociationAgent,
    logout,
    refresh
  }
})
