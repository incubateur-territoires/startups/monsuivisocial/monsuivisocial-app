import { defineStore } from 'pinia'

export const useFiltersStore = defineStore('filters', () => {
  const interviewHistory = ref({})
  const helpRequestHistory = ref({})
  const beneficiaryHistory = ref({})
  const beneficiary = ref({})

  return {
    interviewHistory,
    helpRequestHistory,
    beneficiaryHistory,
    beneficiary
  }
})
