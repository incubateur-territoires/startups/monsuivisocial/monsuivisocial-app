import { defineStore } from 'pinia'
import { BENEFICIARY_DOCUMENT_FOLDER } from '~/utils/constants/document'

export const useDocumentsStore = defineStore('documents', () => {
  const { $apiRequest } = useNuxtApp()

  const beneficiaryDocumentsFolderId = ref(null)

  function setBeneficiaryDocumentsFolderId(_beneficiaryDocumentsFolderId) {
    if (!beneficiaryDocumentsFolderId.value) {
      beneficiaryDocumentsFolderId.value = _beneficiaryDocumentsFolderId
    }
    return beneficiaryDocumentsFolderId.value
  }

  async function loadBeneficiaryDocumentsFolderId() {
    if (beneficiaryDocumentsFolderId.value) {
      return beneficiaryDocumentsFolderId.value
    }

    try {
      const res = await $apiRequest(
        '/folders',
        {},
        {
          fields: ['id'],
          filter: { name: { _eq: BENEFICIARY_DOCUMENT_FOLDER } }
        }
      )

      if (res.status === 200) {
        const beneficiaryDocumentsFolderId = res.data.data[0].id
        return setBeneficiaryDocumentsFolderId(beneficiaryDocumentsFolderId)
      }
      return null
    } catch {
      return null
    }
  }

  return { loadBeneficiaryDocumentsFolderId }
})
