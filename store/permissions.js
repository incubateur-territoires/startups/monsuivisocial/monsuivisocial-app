import { defineStore } from 'pinia'
import { useAuthStore } from './auth'

export const usePermissionsStore = defineStore('permissions', () => {
  const authStore = useAuthStore()

  const ownsItem = computed(
    () => item =>
      !item || (item.user_created?.id || item.user_created) === authStore.userId
  )

  const canEditAnyHistoryItem = computed(
    () => authStore.isResponsableDeStructure
  )

  const canEditHistoryItem = computed(
    () => history => canEditAnyHistoryItem.value || ownsItem.value(history)
  )

  const canDeleteHistoryItem = computed(
    () => history =>
      authStore.isResponsableDeStructure ||
      (ownsItem.value(history) && !authStore.isAgentDAccueil)
  )

  const canEditOrganisation = computed(() => authStore.isResponsableDeStructure)

  const canAddUsers = computed(() => authStore.isResponsableDeStructure)

  const canDeleteDocuments = computed(
    () => !authStore.isInstructeur && !authStore.isAgentDAccueil
  )

  const canDisplayDetailedHistory = computed(
    () => history =>
      ownsItem.value(history) ||
      (!authStore.isAgentDAccueil &&
        (!authStore.isInstructeur || history.isInterview))
  )

  const canSetReferents = computed(
    () => beneficiary => !beneficiary || !authStore.isAgentDAccueil
  )

  const canSetHealth = computed(
    () => !authStore.isAgentDAccueil && !authStore.isMinistryAgent
  )

  const canSetOccupationIncome = computed(() => !authStore.isAgentDAccueil)

  const canSetExternalOrganisations = computed(() => !authStore.isAgentDAccueil)

  const canSetGeneralInformation = computed(() => !authStore.isAgentDAccueil)

  const canSetRelatives = computed(() => !authStore.isAgentDAccueil)

  const canAddHelpRequests = computed(() => !authStore.isAgentDAccueil)

  const canAccessRestrictedFieldsInHelpRequest = computed(
    () => !authStore.isAgentDAccueil
  )

  const accessibleHelpRequestFields = computed(() => {
    const fields = [
      'id',
      'status',
      'follow_up_type.name',
      'beneficiary.id',
      'beneficiary.firstname',
      'beneficiary.usual_name',
      'beneficiary.birth_name',
      'user_created.id',
      'user_created.first_name',
      'user_created.last_name'
    ]

    if (canAccessRestrictedFieldsInHelpRequest.value) {
      fields.push(
        'financial_support',
        'asked_amount',
        'allocated_amount',
        'payment_method',
        'payment_date',
        'external_organisation',
        'organisme_instructeur.name',
        'examination_date',
        'handling_date',
        'due_date'
      )
    }

    return fields
  })

  const canSetPrivateSynthesis = computed(
    () => interview => !authStore.isAgentDAccueil && ownsItem.value(interview)
  )

  const canAccessStats = computed(() => !authStore.isAgentDAccueil)

  const canCreateOrganisme = computed(() => !authStore.isAgentDAccueil)

  const canSetDocumentAsConfidential = computed(
    () => document =>
      !document ||
      (document.uploaded_by?.id || document.uploaded_by) === authStore.userId ||
      !authStore.isInstructeur
  )

  const canAccessRelatives = computed(() => !authStore.isAgentDAccueil)

  const canDeleteBeneficiaries = computed(
    () => authStore.isResponsableDeStructure
  )

  return {
    ownsItem,
    canEditAnyHistoryItem,
    canEditHistoryItem,
    canDeleteHistoryItem,
    canEditOrganisation,
    canAddUsers,
    canDeleteDocuments,
    canDisplayDetailedHistory,
    canSetReferents,
    canSetHealth,
    canSetOccupationIncome,
    canSetExternalOrganisations,
    canSetGeneralInformation,
    canSetRelatives,
    canAddHelpRequests,
    canAccessRestrictedFieldsInHelpRequest,
    accessibleHelpRequestFields,
    canSetPrivateSynthesis,
    canAccessStats,
    canCreateOrganisme,
    canSetDocumentAsConfidential,
    canAccessRelatives,
    canDeleteBeneficiaries
  }
})
