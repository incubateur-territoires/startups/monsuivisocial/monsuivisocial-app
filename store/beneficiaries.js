import { defineStore } from 'pinia'

export const useBeneficiariesStore = defineStore('beneficiaries', () => {
  const originPage = ref(null)
  const originTab = ref(null)

  return { originPage, originTab }
})
