import type { Relative } from '~/types/relative'
import { PensionOrganisation } from '~/utils/constants/beneficiary'

export type Beneficiary = {
  usual_name: string
  firstname: string
  id: string

  accommodation_additional_information?: string
  accommodation_mode?: string
  accommodation_name?: string
  accommodation_zone?: string
  additional_information?: string
  address_complement?: string
  administration?: string
  bank?: string
  birth_name?: string
  birth_place?: string
  birthdate?: string
  caf?: string
  caregiver: boolean
  city?: string
  deathdate?: string
  doctor?: string
  email?: string
  employer_siret?: string
  employer?: string
  family_situation?: string
  funeral_contract?: string
  gender?: string
  gir?: string
  health_additional_information?: string
  insurance?: string
  involved_partners?: string
  main_income_amount?: number
  main_income_type?: string[]
  major_children_main_income_amount?: number
  major_children_main_income_type?: string[]
  major_children: number
  minister?: string
  ministere_categorie?: string
  ministere_departement_service_ac?: string
  ministere_structure?: string
  minor_children: number
  mobility?: string
  nationality?: string
  no_phone: boolean
  numero_pegase?: string
  occupation?: string
  orientation_organisation?: string
  orientation_type?: string
  partner_main_income_amount?: number
  partner_main_income_type?: string[]
  pension_organisations?: PensionOrganisation[]
  phone_1?: string
  phone_2?: string
  pole_emploi?: string
  prescribing_organisation?: string
  protection_measure?: string
  region?: string
  relatives: Relative[]
  representative?: string
  service_providers?: string
  social_security_number?: string
  socio_professional_category?: string
  street_number?: string
  street?: string
  title?: string
  zip_code?: string
}
