import { DocumentType, DocumentTheme } from '~/utils/constants/document'

export type DirectusFile = {
  id: string
  filename_download: string
  type: string
  filesize: number
  document_type: DocumentType
  confidential: boolean
  labels: DocumentTheme[]
}
