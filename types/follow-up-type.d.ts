import { FollowUpTypeType } from '~/utils/constants/follow-up-type'

export type FollowUpType = {
  id: string
  name: string
  type: FollowUpTypeType
}
