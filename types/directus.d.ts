export type DirectusSuccessResponse<T> = { data: T; errors?: undefined }
export type DirectusErrorResponse = {
  data?: undefined
  errors: [{ message: string }]
}
export type DirectusResponse<T> =
  | DirectusSuccessResponse<T>
  | DirectusErrorResponse

export type DirectusQuery = {
  fields?: string[]
  sort?: { field: string; sort?: '' | '-' }[]
  filter?: any
  aggregate?: any
  groupBy?: string
  search?: string
  limit?: number
  page?: number
  meta?: string
  deep?: any
}

export type DirectusItem = { id: string }
