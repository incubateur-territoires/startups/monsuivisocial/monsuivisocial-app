import { Beneficiary } from './beneficiary'

export type Relative = {
  id: string
  firstname?: string
  lastname?: string
  type?: string
  city?: string
  phone?: string
  email?: string
  hosted: boolean
  caregiver: boolean
  additional_information?: string
  beneficiary: Beneficiary
}
