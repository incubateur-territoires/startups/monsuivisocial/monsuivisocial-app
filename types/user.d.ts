export type User = {
  id: string
  email: string
  first_name: string
  last_name: string
  organisation: string
  provider: Provider
  external_identifier: string
  role: Role
  status?: string
}
