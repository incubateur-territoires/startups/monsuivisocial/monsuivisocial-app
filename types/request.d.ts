import type {
  DirectusErrorResponse,
  DirectusSuccessResponse
} from '~/types/directus'

export type Options = RequestInit & { data?: any; headers?: Headers }

type OkResponse = Response & { ok: true }
type ErrorResponse = Response & { ok: false }

export type ExtendedResponse<T = unknown> =
  | (OkResponse & { data: T })
  | (ErrorResponse & { data?: DirectusErrorResponse | any })

export type DirectusExtendedResponse<T = unknown> =
  | (OkResponse & { data: DirectusSuccessResponse<T> })
  | (ErrorResponse & { data?: DirectusErrorResponse })

export type Query = {
  fields?: string
  sort?: string
  filter?: string
  aggregate?: string
  groupBy?: string
  search?: string
  limit?: string
  page?: string
  meta?: string
  deep?: string
}
